$(document).on('click', '.js-toggle-mark', function() {
	var mark = $(this).parent();


	$('.photo-map__mark').each(function(index, element){
		if(element.classList.contains('show-mark-content') && mark[0] != element){
			element.classList.remove('show-mark-content');
		}
	});

	mark.toggleClass('show-mark-content');
});


if(document.getElementById('js-photo-map')){
var photo_map = new Vue({
	el: '#js-photo-map',
	data: {
		'mark' : {
			"title" : '',
			"content" : '',
			"link" : '',
			"left" : '20',
			"top" : '20'
		},
		'marks' : [
		{
			'id' : 0,
			"title" : 'АЛЕВТИНА НЕМЦЕВА #1',
			"content" : 'Блокирующая',
			"link" : '#',
			"left" : '150',
			"top" : '90'
		},
		{
			"title" : 'АЛЕВТИНА НЕМЦЕВА #2',
			"content" : 'Блокирующая',
			"link" : '#',
			"left" : '300',
			"top" : '200'
		}
		]
	},
	methods: {
		addMark : function(){
			this.marks.push(this.mark);
			this.mark =  {
				"title" : '',
				"content" : '',
				"link" : '',
				"left" : '20',
				"top" : '20'
			}
		},
		deleteMark : function(key){
			dd(key)
			this.marks = this.marks.splice(key, 1);
		}
	},

	watch: {
	},

	updated : function() {
		// Передаем глобальный контекст, внутри этой функции this = window
		var dragDrop = Dragger(window);
		var divs = document.getElementsByClassName("js-drag-and-drop");
		dragDrop.makeDragDrop(divs);

	},

	created: function () {
	},
	mounted : function() {
		// Передаем глобальный контекст, внутри этой функции this = window
		var dragDrop = Dragger(window);
		var divs = document.getElementsByClassName("js-drag-and-drop");
		dragDrop.makeDragDrop(divs);
	}
});

}
function Dragger(GLOBAL) {
    
	"use strict";
	var DOC = GLOBAL.document;
    // Действия когда завершили перетаскивание:
    function stopDrag() {
    	DOC.onmousemove = null;
    	DOC.onselectstart = null;
    }
    // Служебная функция, позволяет кроссбраузерно получить прокрутку страницы:
    function pageOffset() {
    	return {
    		x : GLOBAL.pageXOffset || DOC.documentElement.scrollLeft || DOC.body.scrollLeft,
    		y : GLOBAL.pageYOffset || DOC.documentElement.scrollTop  || DOC.body.scrollTop
    	};
    }
    // Действия во время перетаскивания:
    function process(element, dx, dy) {

        // Корректный запрет выделения в Chrom Safari:
        DOC.onselectstart = function () {
        	return false;
        };
        // Обработчик нужно ставить именно на объект document:
        DOC.onmousemove = function (event) {
            // Кроссбраузерно получаем объект события:
            var e = GLOBAL.event || event;

            // Запрет выделения в Opera IE
            if (GLOBAL.getSelection) {
            	GLOBAL.getSelection().removeAllRanges();
            } else if (DOC.selection && DOC.selection.clear) {
            	DOC.selection.clear();
            }
            // Высчитываем новое положение перетаскиваемого элемента, с
            // учётом дельты:
            var left = e.clientX + pageOffset().x - dx,
            top = e.clientY + pageOffset().y - dy,
            index = element.getElementsByClassName('js-index')[0].value;

            element.style.left = left + "px";
            element.style.top  = top + "px";

            photo_map.marks[index].left = left;
            photo_map.marks[index].top = top;
        };
    }
    // Основная функция-метод, вызываемая из вне:
    // param elements:NodeList - содержит элементы,
    // которые нужно сделать перетаскиваемыми.
    function dragDrop(elements) {

    	var length, deltaX, deltaY, i;
    	length = elements.length;

        // Действия когда нажали кнопку мыши:
        function startDrag(event) {
            // Кроссбраузерно получаем объект события и текущую цель:
            var e = GLOBAL.event || event,
            target = e.srcElement || e.target;

            // Высчитываем дельту, обязательно только в момент нажатия кнопки мыши:
            deltaX = e.clientX + pageOffset().x - target.offsetLeft;
            deltaY = e.clientY + pageOffset().y - target.offsetTop;
            // Запускаем процесс перетаскивания:
            process(target, deltaX, deltaY);
        }
        // В цикле проходим по всему списку элементов:
        for (i = 0; i < length; i += 1) {
            // Здесь всё начинается. Задаем позиционирование и раздаём обработчики:
            elements[i].style.position = "absolute";
            elements[i].onmousedown = startDrag;
        }
    }
    // Отпустили кнопку - конец перетаскивания:
    DOC.onmouseup = stopDrag;
    // Возвращаем объект:
    return {
    	makeDragDrop : dragDrop
    };
}
