$('.js-block--text-toggle').each(function () {
	var btn = $(this).find('.js-text-toggle--btn'),
		typeText = btn.attr('data-togglle-text'),
		replaceBtnText = btn.find('.js-replace-text');


});

$(document).on('click', '.js-text-toggle--btn', function(){
	var typeText = $(this).attr('data-togglle-text'),
		replaceBtnText = $(this).find('.js-replace-text'),
		that = this;

	$('[data-text="'+typeText+'"]').slideToggle("slow");
	$(this).toggleClass('active');
	replaceText();
	function replaceText(){
		if($(that).hasClass('active')){
			replaceBtnText.text(replaceBtnText.attr('data-replace-text'))
		} else {
			replaceBtnText.text(replaceBtnText.attr('data-text-default'))
		}
	}

})