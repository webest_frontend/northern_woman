
if($(window).width() > 770){

	youtube_slider = $('.js-slider--youtube').lightSlider({
		item:1,
		loop:false,
		slideMargin: 30,
		slideMove:1,
		easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
		speed:600,
		pager: true,
		responsive : [
		{
			breakpoint:800,
			settings: {
				item:1,
				slideMove:1,
			}
		},
		{
			breakpoint:480,
			settings: {
				item:1,
				slideMove:1
			}
		}
		]
	});
}



partners_slider = $('.js-slider--partners').lightSlider({
	item:6,
	loop: true,
	slideMargin: 30,
	auto:true,
	pauseOnHover: true,
	slideMove:1,
	easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
	speed:900,
	pager: false,
	responsive : [
	{
		breakpoint: 1200,
		settings: {
			item:5,
			slideMove:1,
		}
	},
	{
		breakpoint: 991,
		settings: {
			item:4,
			slideMove:1,
		}
	},
	{
		breakpoint:800,
		settings: {
			item:3,
			slideMove:1
		}
	},
	{
		breakpoint:600,
		settings: {
			item:2,
			slideMove:1
		}
	},
	{
		breakpoint:480,
		settings: {
			item:1.5,
			slideMove:1
		}
	}
	]
});

players_slider = $('.js-players-slider').lightSlider({
	item:4,
	loop: true,
	slideMargin: 30,
	auto:true,
	pauseOnHover: true,
	slideMove:1,
	easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
	speed:900,
	pager: false,
	responsive : [
	{
		breakpoint: 991,
		settings: {
			item:3,
			slideMove:1,
		}
	},
	{
		breakpoint:768,
		settings: {
			item:2,
			slideMove:1
		}
	},
	{
		breakpoint:480,
		settings: {
			item:1,
			slideMove:1
		}
	}
	]
});


// document.querySelectorAll('.js-b-slider-content_realizovannye-proekty').forEach(function(element, index){
// 	window['realizovannye_proekty_'+index ]= $(element).lightSlider({
// 		item:5,
// 		loop:false,
// 		slideMargin: 6,
// 		slideMove:1,
// 		easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
// 		speed:600,
// 		pager: false,
// 		responsive : [
// 		{
// 			breakpoint:800,
// 			settings: {
// 				item:3,
// 				slideMove:1,
// 			}
// 		},
// 		{
// 			breakpoint:480,
// 			settings: {
// 				item:2,
// 				slideMove:1
// 			}
// 		}
// 		]
// 	});
// })



progress_achievements = $('.js-progress__slider--achievements').lightSlider({
	item:5,
	loop: false,
	slideMargin: 0,
	auto:false,
	pauseOnHover: true,
	slideMove:1,
	easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
	speed:900,
	pager: false,
	onBeforeStart : function() {
		toggleShowBtnSlider();
	},
	responsive : [
	{
		breakpoint: 991,
		settings: {
			item:3,
			slideMove:1,
		}
	},
	{
		breakpoint:768,
		settings: {
			item:2,
			slideMove:1
		}
	},
	{
		breakpoint:480,
		settings: {
			item:1.5,
			slideMove:1
		}
	}
	]
});

progress_career = $('.js-progress__slider--career').lightSlider({
	item:5,
	loop: false,
	slideMargin: 0,
	auto:false,
	pauseOnHover: true,
	slideMove:1,
	easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
	speed:900,
	pager: false,
	onBeforeStart : function() {
		toggleShowBtnSlider();
	},
	responsive : [
	{
		breakpoint: 991,
		settings: {
			item:3,
			slideMove:1,
		}
	},
	{
		breakpoint:768,
		settings: {
			item:2,
			slideMove:1
		}
	},
	{
		breakpoint:480,
		settings: {
			item:1.5,
			slideMove:1
		}
	}
	]
});

home_slider = $('.js-slider--home-media').lightSlider({
	item:1,
	loop: true,
	slideMargin: 0,
	auto: true,
	pauseOnHover: true,
	slideMove:1,
	easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
	speed:900,
	pager: true,
	pause: 3000
});


$(window).resize(function(){
	toggleShowBtnSlider()
});

function toggleShowBtnSlider(){
	var width = $(window).width();

	$('.progress__slider').each(function(){
		var count = $(this).children('.progress__item').length,
		controlSlider = $(this).attr('data-slider');
		dd($(this).attr('data-slider'))

		if(width > 991 && count < 5){
			$('button[data-slider="'+controlSlider+'"]').css('display', 'none');
		} else {
			$('button[data-slider="'+controlSlider+'"]').css('display', 'flex');
		}

		// if(width < 991 && count < 3){
		// 	$('[data-slider="'+controlSlider+'"]').css('display', 'none');
		// } else {
		// 	$('[data-slider="'+controlSlider+'"]').css('display', 'flex');
		// }

		// if(width < 768 && count < 2){
		// 	$('[data-slider="'+controlSlider+'"]').css('display', 'none');
		// } else {
		// 	$('[data-slider="'+controlSlider+'"]').css('display', 'flex');
		// }


	})
	// if(width ){

	// }

}

$(document).on('click', '.js-prev-banner', function(){
	var slider = $(this).attr('data-slider');
	window[slider].goToPrevSlide();
});

$(document).on('click', '.js-next-banner', function(){
	var slider = $(this).attr('data-slider');
	window[slider].goToNextSlide();
});