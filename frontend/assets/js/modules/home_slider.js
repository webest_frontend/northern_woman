$(document).on('click', '.js-home-slider-controll--next', function(){
	homeSlider.nextSlide();
});

$(document).on('click', '.js-home-slider-controll--prev', function(){
	homeSlider.prewSlide();
});




var homeSlider = (function(){
	var activeSlide = 0,
		itemWidth = window.innerWidth * 0.2,
		transform = Modernizr.prefixed('transform')
		time__transition = 350,
		countSlide = 0;


	var init = function(){
		countSlide = $('.home-slider__item').length -1;
		var widthActiveEl = window.innerWidth * 0.6;
		$('.js-home-slider__container').width(countSlide * itemWidth + widthActiveEl);
	}

	init();

	var calcItemWidth = function(){
		itemWidth = window.innerWidth * 0.2;
	}

	var nextSlide = function(){
		calcItemWidth();
		activeSlide ++; 

		$('.home-slider__item').removeClass('active');

		$('.home-slider__item').eq(activeSlide).addClass('active').addClass('leaves--slider');
		$('.home-slider__item').eq(activeSlide-1).addClass('leaves--slider');

		setTimeout(function(){
			$('.home-slider__item').removeClass('leaves--slider');
		},time__transition)
		


		is__translate('next');

		showControlBtn();
		
	};

	var prewSlide = function(){
		calcItemWidth();
		activeSlide --; 
		dd(activeSlide)
		
		$('.home-slider__item').removeClass('active')
		$('.home-slider__item').eq(activeSlide).addClass('active').addClass('leaves--slider');
		$('.home-slider__item').eq(activeSlide+1).addClass('leaves--slider');
		setTimeout(function(){
			$('.home-slider__item').removeClass('leaves--slider');
		},time__transition)


		is__translate('prew');

		showControlBtn();
	};


	var is__translate = function(direction){

		if(direction == 'prew'){
			if(activeSlide  == 0){
				$('.js-home-slider__container').css('transform' , 'translate3d(0px,0,0)')
			} else {
				return translateSlider();
			}
			
		}

		if(direction == 'next'){
			if(activeSlide  != countSlide && activeSlide > 1){
				return translateSlider();
			}
		}



	}

	var translateSlider = function(){
		$('.js-home-slider__container').css('transform' , 'translate3d(' + (-itemWidth * (activeSlide -1)) + 'px,0,0)')
	};

	var showControlBtn = function(){

		if(activeSlide > 0) {
			$('.js-home-slider__controls').addClass('show-controls')
		} else {
			$('.js-home-slider__controls').removeClass('show-controls')
		}

		if(activeSlide == countSlide && activeSlide > 0){
			$('.js-home-slider__controls').find('.js-home-slider-controll--next').css('transform', 'scale(0)')
		} else {
			$('.js-home-slider__controls').find('.js-home-slider-controll--next').css('transform', 'scale(1)')
		}
		
	}

	return {
		nextSlide,
		prewSlide
	}
}());