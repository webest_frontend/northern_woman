
$(document).on('click', '.js-select-tab', function () {
	var selectTab = $(this).attr('data-id-tab'),
		toggleTab = $(this).attr('data-targer-tab');

		$(toggleTab).css('display', 'none');
		$('#'+selectTab).css('display', 'block');

		$('.js-select-tab').removeClass('active');
		$(this).addClass('active');

});