
slideKof = (function(){
    var contetnWidth = $('.wrap-time-history').outerWidth(),
        controllWidth = $('.js-time-history__line').outerWidth(),
        wrapContent = $('.js-wrap-time-history-content').outerWidth();
        kof = (contetnWidth - wrapContent) / controllWidth;

    return kof;
}());

$(window).resize(function(){
    slideKof = (function(){
        var contetnWidth = $('.wrap-time-history').outerWidth(),
            controllWidth = $('.js-time-history__line').outerWidth(),
            wrapContent = $('.js-wrap-time-history-content').outerWidth();
            kof = (contetnWidth - wrapContent) / controllWidth;

        return kof;
    }());
});

slideHistoryContent = function (){
    var controllLeft = this.left - $('.js-time-history__line').offset().left,
        slidePosition = controllLeft * slideKof;

    $('.wrap-time-history').css("transform" , "translateX(-"+slidePosition+"px)");
}


$(document).on('click', '.js-toggle-media-history', function(){
    $('.js-hide-media-history').toggleClass('hide-media-history');
    $(this).toggleClass('active');

    if($(this).hasClass('active')){
        $(this).find('span').text('Свернуть');
    } else {
        $(this).find('span').text('Показать еще');
    }
});

var dragDrop = Dragger(
{
    'context' : window,
    'callbackMovement' : slideHistoryContent, // передаются элемент и позиция
    'wrapper' : '.js-time-history__line'
}
);

var historyBtn = document.getElementsByClassName("js-history-control");
dragDrop.makeDragDrop(historyBtn);


function Dragger(setting) {

	"use strict";

	var DOC = setting.context.document,
        GLOBAL = setting.context,
        wrapper = setting.wrapper ? setting.wrapper : false,
        callbackMovement = setting.callbackMovement ? setting.callbackMovement : false;


    // Действия когда завершили перетаскивание:
    function stopDrag() {
    	DOC.onmousemove = null;
    	DOC.onselectstart = null;
    }

    // Служебная функция, позволяет кроссбраузерно получить прокрутку страницы:
    function pageOffset() {
    	return {
    		x : GLOBAL.pageXOffset || DOC.documentElement.scrollLeft || DOC.body.scrollLeft,
    		y : GLOBAL.pageYOffset || DOC.documentElement.scrollTop  || DOC.body.scrollTop
    	};
    }

    // служебная функция позволяет расчитать размеры обёртки для область DragAndDrop 
    // и задать максимальные координаты для перемещения элементов
    function wrapperOffset(){
        var position = $(wrapper).offset(),
            width = $(wrapper).outerWidth();

        return {
            'left' : position.left,
            'right' : position.left + width
        };
    }

    // Действия во время перетаскивания:
    function process(element, dx, dy) {

        // Корректный запрет выделения в Chrom Safari:
        DOC.onselectstart = function () {
        	return false;
        };

        // Обработчик нужно ставить именно на объект document:
        DOC.onmousemove = function (event) {
            // Кроссбраузерно получаем объект события:
            var e = GLOBAL.event || event;

            // Запрет выделения в Opera IE
            if (GLOBAL.getSelection) {
            	GLOBAL.getSelection().removeAllRanges();
            } else if (DOC.selection && DOC.selection.clear) {
            	DOC.selection.clear();
            }

            // Высчитываем новое положение перетаскиваемого элемента, с
            // учётом дельты:
            var left = e.clientX + pageOffset().x - dx,
                top = e.clientY + pageOffset().y - dy;

            if(wrapper){

                var maxPosition = wrapperOffset();

                if(maxPosition.left && left > maxPosition.right){
                    left = maxPosition.right;
                }

                if(maxPosition.left && left < maxPosition.left){
                    left = maxPosition.left;
                }

            }

            element.style.left = left + "px";
            element.style.top  = top + "px";
            
        
            if(callbackMovement){
                var position = {'left': left, 'top' : top, 'element': element};
                callbackMovement.apply(position);
            }

        };

        

       
    }

    // Основная функция-метод, вызываемая из вне:
    // param elements:NodeList - содержит элементы,
    // которые нужно сделать перетаскиваемыми.
    function dragDrop(elements) {

    	var length, deltaX, deltaY, i;
    	length = elements.length;

        // Действия когда нажали кнопку мыши:
        function startDrag(event) {
            // Кроссбраузерно получаем объект события и текущую цель:
            var e = GLOBAL.event || event,
            target = e.srcElement || e.target;

            // Высчитываем дельту, обязательно только в момент нажатия кнопки мыши:
            deltaX = e.clientX + pageOffset().x - target.offsetLeft;
            deltaY = e.clientY + pageOffset().y - target.offsetTop;
            // Запускаем процесс перетаскивания:
            process(target, deltaX, deltaY);
        }
        // В цикле проходим по всему списку элементов:
        for (i = 0; i < length; i += 1) {
            // Здесь всё начинается. Задаем позиционирование и раздаём обработчики:
            elements[i].style.position = "absolute";
            elements[i].onmousedown = startDrag;
        }
    }

    // Отпустили кнопку - конец перетаскивания:
    DOC.onmouseup = stopDrag;
    // Возвращаем объект:
    return {
    	makeDragDrop : dragDrop
    };
}