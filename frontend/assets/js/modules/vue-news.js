$(document).on('click', '.js-show-filter', function () {
	var type = $(this).attr('data-filter-type');
	$('[data-list-type="'+type+'"]').toggle();
	$(this).toggleClass('active')
});


$(document).on('click', '.js-filter-value', function () {
	var type = $(this).attr('data-filter'),
		filterBtn  = $('[data-filter-type="'+type+'"]'),
		value =  $(this).text();

	$('[data-list-type="'+type+'"]').toggle();
	filterBtn.toggleClass('active')
});

var filterSelect = Vue.extend({
    name : 'filter-select',
    template: ' <div class="filter-select">'+
                  '<div class="filter-select__header">'+
                      '<div class="filter-select__name">{{name}}:</div>'+
                      '<button v-bind:data-filter-type="type" class="filter-select__current js-show-filter">'+
                          '<div class="filter-select__current-value js-current-value">{{selectValue}}</div>'+
                          '<svg class="icon icon-14 filter-select__current-icon"> <use xlink:href="#14"></use></svg>'+
                      '</button>'+
                  '</div>'+
                  '<div v-bind:data-list-type="type" class="filter-select__list">'+
                       '<button v-bind:data-filter="type"'+
                               'v-bind:data-filter-value="index"'+
                               'v-on:click="setFilter({name:type, value:index})"'+
                               'class="filter-select__item js-filter-value"'+
                               'v-bind:class="selectUrl == index ? \'filter-select__item--active\' : \'\' "'+
                               'v-for="(value, index) in filterData">'+
                            '{{value.name ? value.name : value}}'+
                       '</button>'+
                  '</div>'+
              '</div>',

    props : {
        "name" : '',
        'selectValue': '',
        'filterData':{},
        'selectUrl': '',
        'type':''

    },
    mounted: function () {
            dd(this.selectUrl)
    },
    methods: {
        setFilter : function (params){
            this.$emit('setting-filter', params)
        }
    },

    data: function(){
        return {

        }
    }
});

Vue.component('filter-select', filterSelect)

// window.addEventListener('popstate', function(e){
//   location.reload();
// }, false);


    // setTimeout(function() {
    //     window.onpopstate = function() {
    //         location.reload();
    //     }
    // }, 0);


if(document.getElementById('vue-news')) {
    var arhiveNews = new Vue({
        el: '#vue-news',
        data: function () {
            return {
                newsList: __NewsList__.data,
                PageInfo: __NewsList__,
                filter: __Filter__,
                pageUrl: 'archive',
                month:  null ,
                year:  null,
                lazy_loading : false,
                page_load : false
            }
        },
        mounted: function () {
            this.checkLoadImage();
            this.month = __Filter__ ? __Filter__.month.setUrl : null;
            this.year =   __Filter__ ? __Filter__.year.setUrl : null;
        },
        computed: {
            listPagination: function () {
                pagination = [];
                for (var i = 1; i <= this.PageInfo.last_page; i++) {
                    pagination[i] = this.PageInfo.path + '?page=' + i
                }

                return pagination;
            },
            filterUrl: function () {
                var filterUrl = '/' + this.pageUrl;

                if (this.year && this.month) {
                    return filterUrl = '/' + this.pageUrl + '/year-' + this.year + '/month-' + this.month;
                }

                if (this.year) {
                    return filterUrl = '/' + this.pageUrl + '/year-' + this.year;
                }

                if (this.month) {
                    return filterUrl = '/' + this.pageUrl + '/month-' + this.month;
                }

                return filterUrl;
            }
        },
        methods: {

            lazyLoad: function () {
                this.PageInfo.current_page++;
                this.lazy_loading = true;

                var that = this,
                    url = '?page=' + that.PageInfo.current_page;

                $.ajax({
                    url: url,
                    dataType: 'json'
                }).done(function (response) {
                    response.news.data.forEach(function (element) {
                        that.newsList.push(element);
                    });
                    that.filter = response.filter;
                    that.PageInfo = response.news;
                    history.pushState('', '', url);
                    dd(url)
                    that.lazy_loading = false;
                }).fail(function () {
                    alert('Данные не удалось загрузить');
                });
            },

            loadLink: function (link) {
                var that = this;
                that.page_load = false;

                $('html, body').scrollTop(0);

                $.ajax({
                    url: link,
                    dataType: 'json'
                }).done(function (response) {
                    that.newsList = [];
                    response.news.data.forEach(function (element) {
                        that.newsList.push(element);
                    });
                    that.PageInfo = response.news;
                    that.filter = response.filter;
                    history.pushState('', '', link);
                    dd(link)
                    that.checkLoadImage();
                }).fail(function () {
                    alert('Данные не удалось загрузить');
                });
            },

            setFilterGlobal: function (params) {
                this[params.name] == params.value ? this[params.name] = '' : this[params.name] = params.value;
                this.loadLink(this.filterUrl)
            },

            checkLoadImage : function () {
                var that = this,
                    count_file = 0,
                    count_load_file = null;

                $(".js-loading-img").remove();
                $('body').append('<div class="js-loading-img"></div>');
                $('.js-loading-img').css("display", 'none');

                that.newsList.forEach(function (element) {
                    if(element.preview_photo){
                        count_file++;
                        var img = document.createElement('img');
                        img.setAttribute('src', element.preview_photo);
                        $('.js-loading-img').append(img);
                    }
                });

                if(count_file > 0){
                    $('.js-loading-img img').on('load', function () {
                        count_load_file++;
                        if(count_file === count_load_file){
                            that.page_load = true;
                        }
                    })
                } else {
                    that.page_load = true;
                }
            }

        }
    });

}





