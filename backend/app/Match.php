<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $table = "matches";

    protected static function boot()
    {
        parent::boot();

        Match::updating(function ($model_request) {
            $url = '';
            if ($model_request->type_location == 'away') {
                $url = str_slug($model_request->rival->name) . '-' . str_slug($model_request->team->name) . '-' . $model_request->date;
            }
            if ($model_request->type_location == 'home') {
                $url = str_slug($model_request->team->name) . '-' . str_slug($model_request->rival->name) . '-' . $model_request->date;
            }

            if($model_request->team_load_file){
                $team = \Maatwebsite\Excel\Facades\Excel::load($model_request->team_load_file, function ($reader) {
                })->get();

                $players = [];
                foreach ($team as $key => $player) {
                    if($player['igrok']){
                        $players[$key]['name'] = $player['igrok'];
                        $players[$key]['nomer'] = (int)$player['nomer'];
                        $players[$key]['amplua'] = $player['amplua'];
                        $players[$key]['zabitykh_myachey'] = $player['zabitykh_myachey'] ? (int)$player['zabitykh_myachey'] : 0;
                    }
                }
                $model_request->severjanka_composition = json_encode($players);
            } else {
                $model_request->severjanka_composition = '';
            }

            if($model_request->rival_load_file){
                $rival = \Maatwebsite\Excel\Facades\Excel::load($model_request->rival_load_file, function ($reader) {
                })->get();

                $rivals = [];
                foreach ($rival as $key => $player) {
                    if($player['igrok']){
                        $rivals[$key]['name'] = $player['igrok'];
                        $rivals[$key]['nomer'] = (int)$player['nomer'];
                        $rivals[$key]['amplua'] = $player['amplua'];
                        $rivals[$key]['zabitykh_myachey'] = $player['zabitykh_myachey'] ? (int)$player['zabitykh_myachey'] : 0;
                    }

                }

                $model_request->rival_composition = json_encode($rivals);
            } else {
                $model_request->rival_composition = '';
            }

            $model_request->url = $url;
            return $model_request;
        });

        Match::creating(function ($model_request) {
            $url = '';
            if ($model_request->type_location == 'away') {
                $url = str_slug($model_request->rival->name) . '-' . str_slug($model_request->team->name) . '-' . $model_request->date;
            }
            if ($model_request->type_location == 'home') {
                $url = str_slug($model_request->team->name) . '-' . str_slug($model_request->rival->name) . '-' . $model_request->date;
            }
            return $model_request->url = $url;
        });
    }

    public function team()
    {
        return $this->belongsTo('App\Team');
    }

    public function rival()
    {
        return $this->belongsTo('App\Rival');
    }
}
