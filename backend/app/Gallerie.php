<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallerie extends Model
{
    protected $table = 'galleries';
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function items()
    {
        return $this->hasMany('App\GallerieItem');
    }

    public function scopeGalleries($query, $flag)
    {
        return $query->where('flag', $flag);
    }
}
