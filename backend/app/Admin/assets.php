<?php

/**
 * @var KodiCMS\Assets\Contracts\MetaInterface $meta
 * @var KodiCMS\Assets\Contracts\PackageManagerInterface $packages
 *
 * @see http://sleepingowladmin.ru/docs/assets
 */


//$meta
//    ->css('custom', asset('custom.css'))
//    ->js('custom', asset('custom.js'), 'admin-default');

//$packages->add('jquery')
//    ->js(null, asset('libs/jquery.js'));

Meta::addJs('custom', asset('js/admin/_player-awards.js'),'admin-default');
Meta::addJs('team', asset('js/admin/_team.js'),'admin-default');
Meta::addJs('links', asset('js/admin/_links.js'),'admin-default');


Meta::addCss('custom', asset('css/admin.css'),'admin-default');
//PackageManager::add('stopRefresh')
//    ->js('tree',         asset('customjs/stopPageRefresh.js'), ['admin-default'], true);
