<?php

Route::get('', ['as' => 'admin.dashboard', function () {
	$content = '';
	return AdminSection::view($content, '');
}]);

Route::get('links', ['as' => 'admin.link', function () {
    $content = view('admin.link', ['name' => 'James']);;
    return AdminSection::view($content, 'Ссылки');
}]);

Route::post('links', ['as' => 'admin.link', function () {
    $content = view('admin.link', ['name' => 'James']);;
    return AdminSection::view($content, 'Ссылки');
}]);


Route::get('information', ['as' => 'admin.information', function () {
	$content = 'Define your information here.';
	return AdminSection::view($content, 'Information');
}]);