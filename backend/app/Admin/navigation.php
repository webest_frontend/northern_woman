<?php

use SleepingOwl\Admin\Navigation\Page;

return [

    [
        'title' => "Северянка",
        'icon' => 'fa  fa-id-card-o',
        'priority' =>'200',
        'pages' => [
            (new Page(\App\Team::class))
                ->setIcon('fa fa-handshake-o')
                ->setPriority(0),
            (new Page(\App\Trainer::class))
                ->setIcon('fa fa-user-circle')
                ->setPriority(100),
            (new Page(\App\Player::class))
                ->setIcon('fa fa-user-circle')
                ->setPriority(200)
        ]
    ],
    [
        'title' => "Доп. информация",
        'icon' => 'fa  fa-id-card-o',
        'priority' =>'300',
        'pages' => [
            (new Page(\App\History::class))
                ->setIcon('fa fa-book')
                ->setPriority(100),
            (new Page(\App\Partner::class))
                ->setIcon('fa fa-building')
                ->setPriority(200),
            (new Page(\App\File::class))
                ->setIcon('fa fa-file')
                ->setPriority(300),
            [
                'title' => 'Ссылки',
                'icon'  => 'fa fa-link',
                'url'   => route('admin.link'),
            ],
        ]
    ],
    [
        'title' => "Галлереи",
        'icon' => 'fa fa-picture-o',
        'priority' =>'300',
        'pages' => [
            (new Page(\App\Gallerie::class))
                ->setIcon('fa fa-picture-o')
                ->setPriority(0),
            (new Page(\App\GallerieItem::class))
                ->setIcon('fa fa-file-image-o')
                ->setPriority(100),
            ]
    ],


    // Examples
    // [
    //    'title' => 'Content',
    //    'pages' => [
    //
    //        \App\User::class,
    //
    //        // or
    //
    //        (new Page(\App\User::class))
    //            ->setPriority(100)
    //            ->setIcon('fa fa-user')
    //            ->setUrl('users')
    //            ->setAccessLogic(function (Page $page) {
    //                return auth()->user()->isSuperAdmin();
    //            }),
    //
    //        // or
    //
    //        new Page([
    //            'title'    => 'News',
    //            'priority' => 200,
    //            'model'    => \App\News::class
    //        ]),
    //
    //        // or
    //        (new Page(/* ... */))->setPages(function (Page $page) {
    //            $page->addPage([
    //                'title'    => 'Blog',
    //                'priority' => 100,
    //                'model'    => \App\Blog::class
	//		      ));
    //
	//		      $page->addPage(\App\Blog::class);
    //	      }),
    //
    //        // or
    //
    //        [
    //            'title'       => 'News',
    //            'priority'    => 300,
    //            'accessLogic' => function ($page) {
    //                return $page->isActive();
    //		      },
    //            'pages'       => [
    //
    //                // ...
    //
    //            ]
    //        ]
    //    ]
    // ]
];