<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $table = 'players';

    protected static function boot()
    {
        parent::boot();

        Player::updating(function ($model_request) {
            $paramsName = $_POST['params']['name'];
            $paramsValue = $_POST['params']['value'];
            $resultParams = [];

            foreach ($paramsName as $key => $paramName) {
                if ($paramName != null && $paramsValue[$key] != null) {
                    $resultParams[] = array(
                        'name' => $paramName,
                        'value' => $paramsValue[$key],
                    );
                }
            }

            $model_request->career = $_POST['json-career'];
            $model_request->awards = $_POST['json-awards'];
            $model_request->achievements = $_POST['json-achievements'];
            $model_request->parameters = serialize($resultParams);
            return $model_request->url = str_slug($model_request->name);
        });

        Player::creating(function ($model_request) {
            $paramsName = $_POST['params']['name'];
            $paramsValue = $_POST['params']['value'];
            $resultParams = [];

            foreach ($paramsName as $key => $paramName) {
                if ($paramName != null && $paramsValue[$key] != null) {
                    $resultParams[] = array(
                        'name' => $paramName,
                        'value' => $paramsValue[$key],
                    );
                }
            }

            $model_request->career = $_POST['json-career'];
            $model_request->awards = $_POST['json-awards'];
            $model_request->achievements = $_POST['json-achievements'];
            $model_request->parameters = serialize($resultParams);
            return $model_request->url = str_slug($model_request->name);
        });
    }

    public function scopeGetPlayer($query, $url)
    {
        return $query->active()->where('url', '=', $url)->first();
    }


    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    static function GetAge($year_birth)
    {
        $birthday_timestamp = strtotime($year_birth);
        $age = date('Y') - date('Y', $birthday_timestamp);
        if (date('md', $birthday_timestamp) > date('md')) {
            $age--;
        }
        return $age;
    }


    public function getUrlAttribute($value)
    {
        return route('player', ['url_player' => $value]);
    }

    public function team()
    {
        return $this->belongsTo('App\Team');
    }

    public function scopeGetIds($query, $ids)
    {
        return $query->whereIn('id', $ids);
    }
}
