<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}
