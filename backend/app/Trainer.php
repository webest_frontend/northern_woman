<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    protected $table = 'trainers';

    public function scopeGetTrainer($query, $url){
        return $query->active()->where('url', '=', $url)->first();
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    static function GetAge($year_birth){
        $birthday_timestamp = strtotime($year_birth);
        $age = date('Y') - date('Y', $birthday_timestamp);
        if (date('md', $birthday_timestamp) > date('md')) {
            $age--;
        }
        return $age;
    }

    public function getUrlAttribute($value)
    {
        return route('trainer', ['url_trainer' => $value]);
    }

    protected static function boot()
    {
        parent::boot();

        Trainer::updating(function ($model_request) {
            $model_request->parameters = $_POST['json-parameters'];
            $model_request->career = $_POST['json-career'];
            $model_request->awards = $_POST['json-awards'];
            $model_request->achievements = $_POST['json-achievements'];
            return $model_request->url = str_slug($model_request->name);
        });

        Trainer::creating(function ($model_request) {
            $model_request->parameters = $_POST['json-parameters'];
            $model_request->career = $_POST['json-career'];
            $model_request->awards = $_POST['json-awards'];
            $model_request->achievements = $_POST['json-achievements'];

            return $model_request->url = str_slug($model_request->name);
        });
    }
}
