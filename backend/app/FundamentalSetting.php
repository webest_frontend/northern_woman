<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundamentalSetting extends Model
{
    protected $table = 'fundamental_settings';

    protected $fillable = [
        'name', 'var', 'value', 'description'
    ];
}
