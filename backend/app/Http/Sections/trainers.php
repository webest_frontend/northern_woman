<?php

namespace App\Http\Sections;

use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Section;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;

/**
 * Class players
 *
 * @property \App\Player $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class trainers extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $model = '\App\Trainer';
    protected $checkAccess = false;

    public function initialize()
    {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {

        });
    }
    /**
     * @var string
     */
    protected $title = "Тренеры";

    /**
     * @var string
     */
    protected $alias = "trainers";

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatablesAsync()
            ->setDisplaySearch(true)
            ->setOrder([0, 'desc'])
            ->paginate(20)
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::datetime('updated_at', 'Дата')->setFormat('d F,Y')->setWidth('150px'),
                AdminColumn::text(function ($query){
                    $arType = ['Нет', 'Да', ];
                    return $arType[$query['active']];
                }, 'Активность')->setWidth('50px'),
                AdminColumn::image('photo', 'Фото')->setWidth('200px'),
                AdminColumn::link('name', 'Имя')->setWidth('200px')
            );
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::panel();

        $tabs = AdminDisplay::tabbed([
            'Информация о тренере' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::checkbox('active', 'Активность'),
                AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::image('photo','Фотография' )->required()
                    ], 2)
                    ->addColumn([
                        AdminFormElement::text('name', 'Имя')->required(),
                        AdminFormElement::date('year_birth', 'Дата рождения')->setFormat('Y-m-d')->required(),
                    ], 4)
                    ->addColumn([
                        AdminFormElement::text('hometown', 'Родной город'),
                        AdminFormElement::text('сitizenship', 'Гражданство'),
                    ], 4),
                AdminFormElement::view('admin.params_vue'),
                AdminFormElement::wysiwyg('description', 'Описание для карточки тренера')->setHeight('300')

            ]),
            'Спортивные показатели' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::number('height', 'Рост'),
                AdminFormElement::text('role_in_team', 'Роль в команде'),
                AdminFormElement::text('sporting_title', 'Спортивное звание'),
            ]),
            'Карьера, достижения' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::view('admin.trainers-awards'),
            ]),
            'SEO' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::text('url', 'URL')->setReadonly(1),
                AdminFormElement::text('seo_title', 'SEO заголовок'),
                AdminFormElement::text('seo_description', 'SEO описание'),
                AdminFormElement::text('seo_keywords', 'SEO ключевые слова'),
                AdminFormElement::text('id', 'ID')->setReadonly(1),
                AdminFormElement::text('created_at')->setLabel('Создано')->setReadonly(1),
            ])
        ]);
        $form->addElement($tabs);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    public function getIcon()
    {
        return 'fa fa-id-card-o';
    }
}
