<?php
namespace App\Http\Sections;

use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Section;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;

class Rivals extends Section implements Initializable
{
    /**
     * @var \App\fundamentalSetting
     */
    protected $model = '\App\Rival';

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation($priority = 500);
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {

        });
    }

    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * Заголовок раздела и название пункта в меню
     * @var string
     */
    protected $title = 'Соперники';

    /**
     * URL по которому будет доступен раздел
     * @var string
     */
    protected $alias = 'rivals';

    /**
     * @return Первичная отображаемая таблица
     */
    public function onDisplay()
    {
        return AdminDisplay::datatablesAsync()
            ->setOrder([0, 'desc'])
            ->paginate(20)
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::image('logo', 'Логотип')->setWidth('150px'),
                AdminColumn::link('name', 'Название')
            );
    }

    /**
     * @param int $id
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::columns()
                ->addColumn([
                    AdminFormElement::image('logo', 'Логотип' )->required()
                ], 2)
                ->addColumn([
                    AdminFormElement::checkbox('active', 'Активность'),
                    AdminFormElement::text('name', 'Название команды')->required(),
                    AdminFormElement::text('location', "Местоположение")->required(),
                ], 6),

        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    public function getIcon()
    {
        return 'fa fa-bolt';
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

}