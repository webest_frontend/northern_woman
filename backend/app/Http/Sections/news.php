<?php

namespace App\Http\Sections;

use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Section;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;

/**
 * Class news
 *
 * @property \App\FundamentalSetting $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class news extends Section implements Initializable
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */

    protected $model = '\App\News';

    protected $checkAccess = false;

    public function initialize()
    {
        // Добавление пункта меню и счетчика кол-ва записей в разделе
        $this->addToNavigation($priority = 500);

        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {

        });
    }
    /**
     * @var string
     */
    protected $title = 'Новости';

    /**
     * @var string
     */
    protected $alias = 'news';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatablesAsync()
            ->setDisplaySearch(true)
            ->setOrder([0, 'desc'])
            ->paginate(20)
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::datetime('date', 'Дата')->setFormat('d F,Y')->setWidth('150px'),
                AdminColumn::text(function ($query){
                    $arType = ['Нет', 'Да', ];
                    return $arType[$query['published_slider']];
                }, 'В слайдере')->setWidth('200px'),
                AdminColumn::image('preview_photo', 'Фото'),
                AdminColumn::link('title', 'Заголовок')->setWidth('200px'),
                AdminColumn::text('preview_text', 'Текст предпросмотра')
            );
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::panel();

        $tabs = AdminDisplay::tabbed([
            'Информация о новости' => new \SleepingOwl\Admin\Form\FormElements([

                AdminFormElement::text('title', 'Заголовок')->required(),
                AdminFormElement::checkbox('active', 'Активность'),
                AdminFormElement::checkbox('published_slider', 'Отображать в слайдере'),
                AdminFormElement::text('city', 'Город'),
                AdminFormElement::datetime('date', 'Дата')->setFormat('Y-m-d H:i:s')->required(),
                AdminFormElement::image('preview_photo','Изображение для предпросмотра' )->required(),
                AdminFormElement::textarea('preview_text', 'Текс для предпросмотра'),
                AdminFormElement::number('sort', 'Значение сортировки'),
                AdminFormElement::wysiwyg('content', 'Тело новости')->setHeight('700')->required(),

            ]),
            'SEO' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::text('url', 'URL')->setReadonly(1),
                AdminFormElement::text('seo_title', 'SEO заголовок'),
                AdminFormElement::text('seo_description', 'SEO описание'),
                AdminFormElement::text('seo_keywords', 'SEO ключевые слова'),
            ]),
            'Не для редактирования' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::number('share__vk', 'Колличество публикаций в vk')->setReadonly(1),
                AdminFormElement::number('share__facebook', 'Колличество публикаций в facebook')->setReadonly(1),
                AdminFormElement::number('share__twetter', 'Колличество публикаций в twetter')->setReadonly(1),
                AdminFormElement::text('id', 'ID')->setReadonly(1),
                AdminFormElement::text('created_at')->setLabel('Создано')->setReadonly(1),
            ]),
        ]);
        $form->addElement($tabs);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }

    //заголовок для создания записи
    public function getCreateTitle()
    {
        return 'Создание базовой настройки';
    }

    // иконка для пункта меню - шестеренка
    public function getIcon()
    {
        return 'fa fa-newspaper-o';
    }
}
