<?php
namespace App\Http\Sections;

use App\Gallerie;
use AdminColumn;
use AdminDisplay;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use AdminSection;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;

class gallerieItems extends Section implements Initializable
{
    /**
     * @var \App\fundamentalSetting
     */
    protected $model = '\App\GallerieItem';

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            //...
        });
    }

    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * Заголовок раздела и название пункта в меню
     * @var string
     */
    protected $title = 'Элементы галлереи';

    /**
     * URL по которому будет доступен раздел
     * @var string
     */
    protected $alias = 'gallerie-item';

    /**
     * @return Первичная отображаемая таблица
     */
    public function onDisplay()
    {
//        return AdminDisplay::table()
//        ->setHtmlAttribute('class', 'table-primary')
//            ->setColumns(
//                AdminColumn::link('name', 'Название')->setWidth('500px'),
//                AdminColumn::text('gallerie.name', 'Галлерея')
//            )->paginate(20);

        $display = AdminDisplay::datatablesAsync()
            ->setOrder([0, 'desc'])
            ->paginate(20);

        $display->setHtmlAttribute('class', 'table-info table-hover');

        //$display->with('category', 'id');
        $display->setFilters(
            AdminDisplayFilter::related('gallerie_id')->setModel(Gallerie::class)->setTitle(function($value) {
                $gallery = Gallerie::find($value);
                return $gallery->name;
            })
        );

        $display->setColumns([
            AdminColumn::datetime('date', 'Дата публикации')->setFormat('d.m.Y')->setWidth('100px'),
            AdminColumn::link('name', 'Название')->setWidth('300px'),

            AdminColumn::text('gallerie.name', 'Галлерея')
                ->setWidth('150px')
                ->setSearchCallback(function (){
                    return 1;
                })
                ->append(
                    AdminColumn::filter('gallerie_id')
                ),
        ]);

        return $display;
    }


    /**
     * @param int $id
     * @return FormInterface
     */
    public function onEdit($id)
    {
        // поле var - нельзя редактировать, ибо нефиг системообразующий код редактировать
        return AdminForm::panel()->addBody([
            AdminFormElement::checkbox('active', 'Активность'),
            AdminFormElement::text('name', 'Название'),
            AdminFormElement::select('gallerie_id', 'Принадлежность к галеерее')
                ->setModelForOptions('App\Gallerie')
                ->setDisplay('name'),
            AdminFormElement::text('src', 'Ссылка на ресурс')->required(),
            AdminFormElement::image('image', 'Фото для предпросмотра'),
            AdminFormElement::date('date', 'Дата публикации')->setFormat('Y-m-d')->required(),
            //AdminFormElement::number('sort', 'Значение сортировки'),

        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

    // иконка для пункта меню - шестеренка
    public function getIcon()
    {
        return 'fa fa-file-image-o';
    }
}