<?php

namespace App\Http\Sections;

use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Section;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;

/**
 * Class Partners
 *
 * @property \App\Partner $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Partners extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    protected $model = '\App\Partner';
    /**
     * @var string
     */
    protected $title = "Партнёры";

    /**
     * @var string
     */
    protected $alias = "partners";

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::table();

        $display->setHtmlAttribute('class', ' table-hover');

        $display->setApply(function ($query) {
            $query->orderBy('sort', 'asc');
        });

        $display->setColumns([
            AdminColumn::image('logo', 'Логотип'),
            AdminColumn::link('name', 'Название организации'),
            AdminColumn::order()
                ->setLabel('Сортировка')
                ->setHtmlAttribute('class', 'text-center')
                ->setWidth('100px'),
        ]);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::checkbox('active', 'Активность'),
            AdminFormElement::number('sort', 'Значение сортировки')->setHelpText('Чем меньше значение тем раньше показывается'),
            AdminFormElement::image('logo', 'Описание матча'),
            AdminFormElement::text('name', 'Название организации'),
            AdminFormElement::text('link', 'Ссылка на организацию'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
