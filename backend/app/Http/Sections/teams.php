<?php
namespace App\Http\Sections;

use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Section;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;

class teams extends Section implements Initializable
{
    /**
     * @var \App\fundamentalSetting
     */
    protected $model = '\App\teams';

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {

        });
    }

    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * Заголовок раздела и название пункта в меню
     * @var string
     */
    protected $title = 'Команды';

    /**
     * URL по которому будет доступен раздел
     * @var string
     */
    protected $alias = 'teams';

    /**
     * @return Первичная отображаемая таблица
     */
    public function onDisplay()
    {
        return AdminDisplay::datatablesAsync()
            ->setOrder([0, 'desc'])
            ->paginate(20)
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::image('logo', 'Логотип')->setWidth('150px'),
                AdminColumn::link('name', 'Название')->setWidth('150px'),
                AdminColumn::text('date_season', 'Сезон')
            );
    }

    /**
     * @param int $id
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::panel()->setHtmlAttribute('class', 'js-send-form');;

        $tabs = AdminDisplay::tabbed([
            'Информация о игроке' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::checkbox('active', 'Активность'),
                AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::image('logo', 'Логотип' )->required()
                    ], 2)
                    ->addColumn([
                        AdminFormElement::image('menu_photo', 'Фотография для меню' )->required()
                    ], 2)
                    ->addColumn([
                        AdminFormElement::image('team_photo', 'Фотография команды' )->required()
                    ], 2)
                    ->addColumn([
                        AdminFormElement::number('date_season', 'Сезон ')->required(),
                        AdminFormElement::text('name', 'Название команды')->required(),
                        AdminFormElement::text('location', "Местоположение"),
                    ], 6),
                AdminFormElement::view('admin.team')
            ]),
            'SEO' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::text('url', 'URL')->setReadonly(1),
                AdminFormElement::text('seo_title', 'SEO заголовок'),
                AdminFormElement::text('seo_description', 'SEO описание'),
                AdminFormElement::text('seo_keywords', 'SEO ключевые слова'),
                AdminFormElement::text('id', 'ID')->setReadonly(1),
                AdminFormElement::text('created_at')->setLabel('Создано')->setReadonly(1),
            ])
        ]);
        $form->addElement($tabs);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

}