<?php
namespace App\Http\Sections;

use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Section;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;

class Matches extends Section implements Initializable
{
    /**
     * @var \App\fundamentalSetting
     */
    protected $model = '\App\Match';

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation($priority = 500);
        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
        });
    }

    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * Заголовок раздела и название пункта в меню
     * @var string
     */
    protected $title = 'Матчи';

    /**
     * URL по которому будет доступен раздел
     * @var string
     */
    protected $alias = 'matches';

    /**
     * @return Первичная отображаемая таблица
     */
    public function onDisplay()
    {
        return AdminDisplay::datatablesAsync()
            ->setOrder([0, 'desc'])
            ->paginate(20)
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                AdminColumn::datetime('date', 'Дата матча')->setFormat('d F,Y')->setWidth('150px'),
                AdminColumn::text('team.name', 'Команда северянки')
                    ->setSearchCallback(function (){
                        return 1;
                    })->setWidth('200px'),
                AdminColumn::text('rival.name', 'Команда противника')
                    ->setSearchCallback(function (){
                        return 1;
                    })
            ]);

    }

    /**
     * @param int $id
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $form = AdminForm::panel();

        $tabs = AdminDisplay::tabbed([
            'Информация о матче' => new \SleepingOwl\Admin\Form\FormElements([

                AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::checkbox('active', 'Активность'),
                        AdminFormElement::checkbox('is_more_info', 'Показывать ссылку на информацию о матче'),
                    ], 6)
                    ->addColumn([
                        AdminFormElement::select('type_location', 'Выездной или домашний матч', ['home'=>'Дома','away'=>'Выезд']),
                        AdminFormElement::select('type_match', 'Тип матча', ['championship_russia'=>'Чемпионат России','cup_russia'=>'Кубок России']),
                    ], 6),

                AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::date('date', 'Дата матча')->required(),
                    ], 6)
                    ->addColumn([
                        AdminFormElement::text('time', 'Время начала матча')->setHelpText('Формат времени 20:00')
                    ], 6),

                AdminFormElement::text('city', 'Город проведения матча'),
                AdminFormElement::wysiwyg('description', 'Описание матча')->setHeight('350'),
                AdminFormElement::text('video', 'Ссылка на видео'),

            ]),
            'Участники матча' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::select('team_id', 'Команда северянка')
                            ->setModelForOptions('App\Team')
                            ->setDisplay('name')
                            ->required(),
                        AdminFormElement::file('team_load_file', 'Данные о команде'),
                        AdminFormElement::view('admin.match_severjanka')
                    ], 6)
                    ->addColumn([
                        AdminFormElement::select('rival_id', 'Команда противника')
                            ->setModelForOptions('App\Rival')
                            ->setDisplay('name')
                            ->required(),
                        AdminFormElement::file('rival_load_file', 'Данные о сопернике'),
                        AdminFormElement::view('admin.match_rival')

                    ], 6),
            ]),
            'Результаты встречи' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::text('scope', 'Счёт')->setHelpText('3:0'),
                AdminFormElement::text('result', 'Результат матча')->setHelpText('25:18 25:22 15:25 25:15 25:18 25:22'),
                AdminFormElement::select('leader_team_first', 'Первый лидер команды')
                    ->setModelForOptions('App\Player')
                    ->setDisplay('name')
                    ->required(),
                AdminFormElement::select('leader_team_two', 'Второй лидер команды')
                    ->setModelForOptions('App\Player')
                    ->setDisplay('name')
                    ->required(),
            ]),
            'SEO' => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::text('url', 'URL')->setReadonly(1),
                AdminFormElement::text('seo_title', 'SEO заголовок'),
                AdminFormElement::text('seo_description', 'SEO описание'),
                AdminFormElement::text('seo_keywords', 'SEO ключевые слова'),
            ]),
        ]);

        $form->addElement($tabs);
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    public function getIcon()
    {
        return 'fa fa-star';
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }

}