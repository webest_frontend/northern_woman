<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImportController extends Controller
{
    public function championship_import()
    {
        $html_source = file_get_contents('http://www.volley.ru/competitions.php?tid=80&gender=2&league=7&year=2018&x=32&y=11');
        //$html_source = mb_convert_encoding($html_source, "UTF-8", "WINDOWS-1251");
        $html_source = preg_replace('|\t|', '', $html_source);
        $html_source = preg_replace('|\n|', '', $html_source);
        //dd($html_source);
        //$html_source = mb_convert_encoding( $html_source, "HTML-ENTITIES", "UTF-8");
        //dd($html_source);
        //$content = str_replace('banner_search', $this::generatePassword(), $content);

        $doc = new \DOMDocument();
        @$doc->loadHTML('<table cellpadding="0" cellspacing="0" class="calend_table" border="0">
<tbody><tr>
	<td width="72" class="width_td"><img src="/img/0.gif" width="72" height="1"></td>
	<td width="2" class="width_td"></td>
	<td width="126" class="width_td"><img src="/img/0.gif" width="126" height="1"></td>
	<td width="2" class="width_td"></td>
	<td width="245" class="width_td"><img src="/img/0.gif" width="240" height="1"></td>
	<td width="5" class="width_td"></td>
	<td width="245" class="width_td"><img src="/img/0.gif" width="240" height="1"></td>
	<td width="2" class="width_td"></td>
	<td width="20" class="width_td"><img src="/img/0.gif" width="20" height="1"></td>
	<td width="2" class="width_td"></td>
	<td width="248" class="width_td"><img src="/img/0.gif" width="240" height="1"></td>
</tr>
<tr>
	<td class="calend_table_headtd">Дата</td>
	<td></td>
	<td class="calend_table_headtd">Тур</td>
	<td></td>
	<td class="calend_table_headtd" colspan="3" align="center">Команды</td>
	<td></td>
	<td class="calend_table_headtd">Статистика</td>
	<td></td>
	<td class="calend_table_headtd">Результаты</td>
</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">07.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">1-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Приморочка </b> (Владивосток)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Луч</b> (Москва)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:1</b> (25:16, 29:27, 24:26, 25:12)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">08.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:19, 25:15, 28:26)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">07.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">1-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тулица</b> (Тула)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:17, 28:26, 25:13)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">08.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:22, 25:22, 25:19)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">07.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">1-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:1</b> (25:18, 23:25, 25:18, 25:16)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">08.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:2</b> (21:25, 11:25, 26:24, 26:24, 15:13)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">07.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">1-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Воронеж</b> (Воронеж)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Олимп</b> (Новосибирская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>0:3</b> (11:25, 16:25, 19:25)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">08.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>0:3</b> (18:25, 19:25, 12:25)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">07.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">1-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Северянка</b> (Череповец)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Импульс</b> (Волгодонск)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:16, 25:16, 25:21)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">08.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:22, 25:17, 25:19)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">08.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">1-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Спарта</b> (Нижний Новгород)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>1:3</b> (25:18, 21:25, 29:31, 21:25)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">09.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>1:3</b> (25:23, 20:25, 19:25, 10:25)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">14.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">2-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Луч</b> (Москва)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>2:3</b> (25:23, 25:20, 23:25, 21:25, 9:15)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">15.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>1:3</b> (21:25, 25:15, 21:25, 15:25)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">14.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">2-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Импульс</b> (Волгодонск)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Спарта</b> (Нижний Новгород)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>1:3</b> (25:21, 22:25, 23:25, 10:25)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">15.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>0:3</b> (22:25, 24:26, 21:25)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">14.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">2-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Северянка</b> (Череповец)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Приморочка </b> (Владивосток)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:19, 25:20, 25:15)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">15.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:22, 25:15, 25:19)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">14.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">2-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Олимп</b> (Новосибирская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>2:3</b> (18:25, 25:20, 16:25, 25:21, 8:15)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">15.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:2</b> (28:26, 25:17, 19:25, 25:27, 15:11)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">14.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">2-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Воронеж</b> (Воронеж)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:8, 25:19, 26:24)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">15.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:20, 25:19, 25:22)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">14.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">2-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тулица</b> (Тула)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>0:3</b> (24:26, 21:25, 16:25)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">15.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>2:3</b> (25:15, 22:25, 21:25, 25:16, 6:15)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">21.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">3-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Луч</b> (Москва)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:1</b> (26:24, 22:25, 25:23, 25:16)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">22.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:23, 25:17, 25:17)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">21.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">3-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Воронеж</b> (Воронеж)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тулица</b> (Тула)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>0:3</b> (15:25, 11:25, 8:25)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">22.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>0:3</b> (18:25, 18:25, 12:25)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">21.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">3-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Спарта</b> (Нижний Новгород)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Олимп</b> (Новосибирская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:18, 25:19, 25:19)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">22.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:22, 25:23, 25:17)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">21.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">3-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Приморочка </b> (Владивосток)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Импульс</b> (Волгодонск)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:12, 25:16, 27:25)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">22.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:1</b> (28:26, 23:25, 25:17, 25:21)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">21.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">3-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Северянка</b> (Череповец)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>0:3</b> (13:25, 19:25, 21:25)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">22.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>1:3</b> (23:25, 13:25, 26:24, 24:26)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">23.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">3-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:2</b> (23:25, 23:25, 25:13, 26:24, 15:7)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">24.10.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>0:3</b> (17:25, 20:25, 22:25)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">11.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">4-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Спарта</b> (Нижний Новгород)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>1:3</b> (13:25, 25:23, 21:25, 20:25)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">12.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:1</b> (25:18, 16:25, 26:24, 25:16)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">11.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">4-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тулица</b> (Тула)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (26:24, 25:13, 25:15)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">12.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (27:25, 25:19, 26:24)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">11.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">4-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Луч</b> (Москва)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Воронеж</b> (Воронеж)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:16, 25:11, 25:15)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">12.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:15, 25:19, 25:14)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">11.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">4-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Северянка</b> (Череповец)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:1</b> (25:13, 25:17, 25:27, 25:18)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">12.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:14, 25:14, 27:25)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">11.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">4-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Импульс</b> (Волгодонск)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Олимп</b> (Новосибирская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:1</b> (25:23, 24:26, 25:21, 25:22)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">12.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>2:3</b> (22:25, 25:21, 25:18, 17:25, 14:16)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">11.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">4-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Приморочка </b> (Владивосток)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>1:3</b> (25:15, 20:25, 21:25, 24:26)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">12.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:20, 25:18, 25:20)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">18.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">5-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Спарта</b> (Нижний Новгород)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тулица</b> (Тула)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>2:3</b> (20:25, 25:12, 14:25, 25:21, 6:15)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">19.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>2:3</b> (24:26, 22:25, 25:21, 26:24, 8:15)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">18.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">5-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Импульс</b> (Волгодонск)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:16, 25:16, 25:23)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">19.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:17, 25:16, 25:22)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">18.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">5-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Олимп</b> (Новосибирская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>1:3</b> (24:26, 19:25, 25:19, 21:25)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">19.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:20, 25:23, 25:22)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">18.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">5-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Приморочка </b> (Владивосток)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>0:3</b> (18:25, 18:25, 23:25)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">19.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:2</b> (22:25, 25:20, 25:23, 21:25, 15:11)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">18.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">5-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Воронеж</b> (Воронеж)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Северянка</b> (Череповец)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>1:3</b> (18:25, 23:25, 25:22, 19:25)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">19.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>0:3</b> (11:25, 10:25, 13:25)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">18.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">5-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Луч</b> (Москва)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:17, 25:17, 25:16)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">19.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:19, 25:16, 25:21)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">25.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">6-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Северянка</b> (Череповец)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:1</b> (25:16, 25:20, 23:25, 25:14)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">26.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:14, 25:22, 25:19)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">25.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">6-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Приморочка </b> (Владивосток)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Воронеж</b> (Воронеж)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:0, 25:0, 25:0)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">26.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:0, 25:0, 25:0)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">25.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">6-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Импульс</b> (Волгодонск)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>0:3</b> (22:25, 18:25, 21:25)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">26.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>1:3</b> (22:25, 26:24, 20:25, 17:25)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">25.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">6-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:2</b> (25:22, 22:25, 18:25, 27:25, 15:8)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">26.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:2</b> (21:25, 25:20, 25:22, 22:25, 15:13)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">25.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">6-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тулица</b> (Тула)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Олимп</b> (Новосибирская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (25:17, 25:16, 25:23)</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">26.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>1:3</b> (25:17, 20:25, 22:25, 23:25)</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">25.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">6-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Луч</b> (Москва)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Спарта</b> (Нижний Новгород)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>2:3</b> (25:13, 23:25, 25:20, 21:25, 8:15)</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">26.11.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text"><b>3:0</b> (26:24, 25:21, 25:11)</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">09.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">7-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Воронеж</b> (Воронеж)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">10.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">09.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">7-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Спарта</b> (Нижний Новгород)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Северянка</b> (Череповец)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">10.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">09.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">7-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Олимп</b> (Новосибирская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Луч</b> (Москва)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">10.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">09.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">7-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тулица</b> (Тула)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">10.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">09.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">7-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Импульс</b> (Волгодонск)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">10.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">10.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">7-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Приморочка </b> (Владивосток)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">11.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">16.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">8-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">17.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">16.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">8-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Импульс</b> (Волгодонск)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тулица</b> (Тула)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">17.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">16.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">8-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Воронеж</b> (Воронеж)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">17.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">16.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">8-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Луч</b> (Москва)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">17.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">16.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">8-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Северянка</b> (Череповец)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Олимп</b> (Новосибирская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">17.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">16.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">8-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Приморочка </b> (Владивосток)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Спарта</b> (Нижний Новгород)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">17.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">23.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">9-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Олимп</b> (Новосибирская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Приморочка </b> (Владивосток)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">24.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">23.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">9-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Северянка</b> (Череповец)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">24.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">23.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">9-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Воронеж</b> (Воронеж)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Импульс</b> (Волгодонск)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">24.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">23.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">9-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тулица</b> (Тула)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Луч</b> (Москва)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">24.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">23.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">9-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">24.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">23.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">9-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Спарта</b> (Нижний Новгород)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">24.12.17</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">13.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">10-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Приморочка </b> (Владивосток)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">14.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">13.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">10-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Олимп</b> (Новосибирская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">14.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">13.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">10-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Спарта</b> (Нижний Новгород)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">14.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">13.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">10-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Импульс</b> (Волгодонск)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Луч</b> (Москва)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">14.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">13.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">10-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Воронеж</b> (Воронеж)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">14.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">13.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">10-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Северянка</b> (Череповец)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тулица</b> (Тула)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">14.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">20.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">11-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Импульс</b> (Волгодонск)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">21.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">20.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">11-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Луч</b> (Москва)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Северянка</b> (Череповец)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">21.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">20.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">11-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Спарта</b> (Нижний Новгород)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Воронеж</b> (Воронеж)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">21.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">20.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">11-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Олимп</b> (Новосибирская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">21.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">20.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">11-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">21.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">20.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">11-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тулица</b> (Тула)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Приморочка </b> (Владивосток)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">21.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">27.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">12-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">28.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">27.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">12-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Олимп</b> (Новосибирская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Воронеж</b> (Воронеж)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">28.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">27.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">12-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Импульс</b> (Волгодонск)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Северянка</b> (Череповец)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">28.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">27.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">12-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Спарта</b> (Нижний Новгород)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">28.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">27.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">12-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Луч</b> (Москва)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Приморочка </b> (Владивосток)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">28.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">27.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">12-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тулица</b> (Тула)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">28.01.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">03.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">13-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Воронеж</b> (Воронеж)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">04.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">03.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">13-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тулица</b> (Тула)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">04.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">03.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">13-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Луч</b> (Москва)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">04.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">03.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">13-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Спарта</b> (Нижний Новгород)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Импульс</b> (Волгодонск)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">04.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">03.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">13-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Олимп</b> (Новосибирская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">04.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">10.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">13-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Приморочка </b> (Владивосток)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Северянка</b> (Череповец)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">11.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">17.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">14-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Импульс</b> (Волгодонск)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Приморочка </b> (Владивосток)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">18.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">17.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">14-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Олимп</b> (Новосибирская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Спарта</b> (Нижний Новгород)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">18.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">17.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">14-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Северянка</b> (Череповец)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">18.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">17.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">14-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Луч</b> (Москва)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">18.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">17.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">14-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тулица</b> (Тула)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Воронеж</b> (Воронеж)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">18.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">17.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">14-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">18.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">24.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">15-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Северянка</b> (Череповец)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">25.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">24.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">15-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Приморочка </b> (Владивосток)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">25.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">24.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">15-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Олимп</b> (Новосибирская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Импульс</b> (Волгодонск)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">25.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">24.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">15-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Спарта</b> (Нижний Новгород)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">25.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">24.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">15-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Воронеж</b> (Воронеж)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Луч</b> (Москва)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">25.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">25.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">15-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тулица</b> (Тула)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">26.02.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">03.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">16-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Северянка</b> (Череповец)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Воронеж</b> (Воронеж)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">04.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">03.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">16-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Луч</b> (Москва)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">04.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">03.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">16-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тулица</b> (Тула)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Спарта</b> (Нижний Новгород)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">04.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">03.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">16-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Импульс</b> (Волгодонск)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">04.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">03.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">16-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Олимп</b> (Новосибирская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">04.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">03.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">16-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Приморочка </b> (Владивосток)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">04.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">10.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">17-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">11.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">10.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">17-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Олимп</b> (Новосибирская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тулица</b> (Тула)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">11.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">10.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">17-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Спарта</b> (Нижний Новгород)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Луч</b> (Москва)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">11.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">10.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">17-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Северянка</b> (Череповец)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">11.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">10.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">17-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Воронеж</b> (Воронеж)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Приморочка </b> (Владивосток)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">11.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">10.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">17-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Импульс</b> (Волгодонск)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">11.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">24.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">18-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Луч</b> (Москва)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Олимп</b> (Новосибирская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">25.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">24.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">18-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Импульс</b> (Волгодонск)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">25.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">24.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">18-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тулица</b> (Тула)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">25.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">24.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">18-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Воронеж</b> (Воронеж)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">25.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">24.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">18-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Приморочка </b> (Владивосток)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">25.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">24.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">18-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Северянка</b> (Череповец)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Спарта</b> (Нижний Новгород)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">25.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">31.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">19-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Олимп</b> (Новосибирская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Северянка</b> (Череповец)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">01.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">31.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">19-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Спарта</b> (Нижний Новгород)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Приморочка </b> (Владивосток)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">01.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">31.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">19-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">01.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">31.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">19-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тулица</b> (Тула)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Импульс</b> (Волгодонск)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">01.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">31.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">19-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Воронеж</b> (Воронеж)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">01.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">31.03.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">19-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Луч</b> (Москва)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">01.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">07.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">20-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Луч</b> (Москва)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тулица</b> (Тула)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">08.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">07.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">20-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">08.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">07.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">20-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Спарта</b> (Нижний Новгород)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">08.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">07.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">20-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Приморочка </b> (Владивосток)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Олимп</b> (Новосибирская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">08.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">07.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">20-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Северянка</b> (Череповец)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">08.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">07.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">20-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Импульс</b> (Волгодонск)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Воронеж</b> (Воронеж)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">08.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">21.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">21-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Спарта</b> (Нижний Новгород)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">22.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">21.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">21-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Луч</b> (Москва)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Импульс</b> (Волгодонск)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">22.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">21.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">21-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Воронеж</b> (Воронеж)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">22.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">21.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">21-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тулица</b> (Тула)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Северянка</b> (Череповец)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">22.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">21.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">21-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Приморочка </b> (Владивосток)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">22.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">21.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">21-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Олимп</b> (Новосибирская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">22.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">28.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">22-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>ЮЗГУ-Атом</b> (Курская область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тюмень-ТюмГУ</b> (Тюмень)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">29.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">28.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">22-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Приморочка </b> (Владивосток)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Тулица</b> (Тула)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">29.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">28.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">22-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Импульс</b> (Волгодонск)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Самрау-УГНТУ</b> (Уфа)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">29.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">28.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">22-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Северянка</b> (Череповец)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Луч</b> (Москва)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">29.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#FFFFFF">
		<td class="text">28.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">22-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Воронеж</b> (Воронеж)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Спарта</b> (Нижний Новгород)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#FFFFFF">
		<td class="text">29.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		
		
		<tr bgcolor="#EBEBEB">
		<td class="text">28.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		<td class="round" rowspan="2">22-й тур</td>
		<td bgcolor="#FFFFFF"></td>
		
			<td class="command_left" rowspan="2"><b>Липецк-Индезит</b> (Липецкая область)</td>
			<td class="command_center" rowspan="2"><b>:</b></td>
			<td class="command_right" rowspan="2"><b>Олимп</b> (Новосибирская область)<br></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		<tr bgcolor="#EBEBEB">
		<td class="text">29.04.18</td>
		<td bgcolor="#FFFFFF"></td>
		
		<td bgcolor="#FFFFFF"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text" align="center"></td>
		<td bgcolor="#FFFFFF"></td>
		<td class="text">&nbsp;</td>
		</tr>
		
		</tbody></table>'); // путь к вашему файлу
//
//        $xpath = new \DOMXpath($doc);
//
//        $expression = './/table[contains(concat(" ", normalize-space(@class), " "), " calend_table ")]';
//        $data = array();
//        $i = 0;
//        foreach ($xpath->evaluate($expression) as $tr) {
//            foreach ($tr->getElementsByTagName('td') as $td) {
//                $data[$i][] = htmlentities(utf8_decode($td->nodeValue), ENT_QUOTES, 'UTF-8');
//            }
//            $i++;
//        }
//        foreach ($data as $key => $str){
//            $data[$key] =  mb_convert_encoding(implode($str), "UTF-8", "WINDOWS-1251");
//        }

        $table = $doc->getElementsByTagName('table');
        $data = array();
        $i = 0;
        foreach ($table->item(0)->getElementsByTagName('tr') as $tr) {
            foreach ($tr->getElementsByTagName('td') as $td) {
                $data[$i][] = htmlentities(utf8_decode($td->nodeValue), ENT_QUOTES, 'UTF-8');
            }
            $i++;
        }
        unset($data[0]);
        unset($data[1]);
        $temp_result = [];

        $data = array_chunk($data, 2);

        foreach ($data as $key => $rows) {
            foreach ($rows as $row){
                $number_row = 'first';
                if(count($row)< 11){
                    $number_row = 'two';
                }
                $temp_result[$key][$number_row] = $row;
            }
        }

        //dd($temp_result);
        $result = [];

        $i = 0;
        foreach ($temp_result as $item) {
            $date = $item['first'][0] . '<br>' . $item['two'][0];
            $tour = $item['first'][2];
            $team_first__temp = $item['first'][4];
            $team_two__temp = $item['first'][6];
            $first_match = $item['first'][10];
            $two_match = $item['two'][6];


            $team_first__temp = explode('(', $team_first__temp);
            $team_first['name'] = $team_first__temp[0];
            $team_first['location'] = str_replace(')', '', $team_first__temp[1]);

            $team_two__temp = explode('(', $team_two__temp);
            $team_two['name'] = $team_two__temp[0];
            $team_two['location'] = str_replace(')', '', $team_two__temp[1]);


            if($first_match){
                $first_match = explode('(', $first_match);
                $scope['first_match']['short'] = $first_match[0];
                $scope['first_match']['full'] = str_replace(')', '', $first_match[1]);
            } else {
                $first_match = explode('(', $first_match);
                $scope['first_match']['short'] = '';
                $scope['first_match']['full'] = '';
            }

            if($two_match){
                $two_match = explode('(', $two_match);
                $scope['two_match']['short'] = $two_match[0];
                $scope['two_match']['full'] = str_replace(')', '', $two_match[1]);
            } else {
                $scope['two_match']['short'] = '';
                $scope['two_match']['full'] = '';
            }




            $result[$i]['date'] = $date;
            $result[$i]['tour'] = $tour;
            $result[$i]['team_first'] = $team_first;
            $result[$i]['team_two'] = $team_two;
            $result[$i]['scope'] = $scope;

            $i++;
        }

        //dd($result);

        return view('pages.import', ['data' => $result]);

    }

    public function get_web_page($url)
    {
        $user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

        $options = array(

            CURLOPT_CUSTOMREQUEST => "GET",        //set request type post or get
            CURLOPT_POST => false,        //set to GET
            CURLOPT_USERAGENT => $user_agent, //set user agent
            //CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            //CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING => "",       // handle all encodings
            CURLOPT_AUTOREFERER => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT => 120,      // timeout on response
            CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        $header['errno'] = $err;
        $header['errmsg'] = $errmsg;
        $header['content'] = $content;
        return $header;
    }
}
