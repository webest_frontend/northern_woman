<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class News extends Controller
{
    public $filterMonthValue = [
        'january' => [
            'name' => 'январь',
            'value' => 1
        ],
        'february' => [
            'name' => 'февраль',
            'value' => 2
        ],
        'march' => [
            'name' => 'март',
            'value' => 3
        ],
        'april' => [
            'name' => 'апрель',
            'value' => 4
        ],
        'may' => [
            'name' => 'май',
            'value' => 5
        ],
        'june' => [
            'name' => 'июнь',
            'value' => 6
        ],
        'july' => [
            'name' => 'июль',
            'value' => 7
        ],
        'august' => [
            'name' => 'август',
            'value' => 7
        ],
        'september' => [
            'name' => 'сентябрь',
            'value' => 8
        ],
        'october' => [
            'name' => 'октябрь',
            'value' => 10
        ],
        'november' => [
            'name' => 'ноябрь',
            'value' => 11
        ],
        'december' => [
            'name' => 'декабрь',
            'value' => 12
        ],
    ];

    public $filterYearValue = [];

    protected $filterYear = null;
    protected $filterMonth = null;
    protected $filterMonthShow = null;
    protected $filterYearShow = null;
    protected $filterYearUrl = null;
    protected $filterMonthUrl = null;


    // list element
    public function index(Request $request)
    {

        if (\Illuminate\Support\Facades\Request::ajax()) {
            $data = [];

            if(\Illuminate\Support\Facades\Request::input('count') == 10 ){
                $listNews = \App\News::Published()->paginate(10);
            } else {
                $listNews = \App\News::Published()->paginate(11);
            }

            foreach ($listNews as $news){
                $news['date'] = $news->getDate();
            }

            header('Cache-Control: no-cache, no-store, must-revalidate');
            header('Pragma: no-cache');
            header('Expires: 0');

            $data = array_add($data, 'news', $listNews);
            $data = array_add($data, 'filter', null);
            return json_encode($data);

        } else {
            $listNews = \App\News::Published()->paginate(11);
        }

        foreach ($listNews as $news){
            $news['date'] = $news->getDate();
        }

        return view('pages.news', [
            'listNews' => $listNews,
        ]);
    }


    // show element
    public function element($url)
    {

        $news = \App\News::getByUrl($url);
        $sidebarNews = \App\News::GetSidebarNews($url);

        if(!$news->first()){
            $this::get404();
        }

        foreach ($sidebarNews as $news){
            $news['date'] = $news->getDate();
        }


        $news['date'] = $news->getDate();


        return view('pages.news-detail', [
            'news' => $news,
            'sidebarNews' => $sidebarNews,
        ]);
    }

    // archive and filter
    public function archive(Request $request)
    {
        $this->filterYearValue = \App\News::getYearFilter();

        // сборка параметров фильтрации из url с проверкой на существование фильтров
        foreach ($request->route()->parameters as $filter) {
            $is_filter = false;

            if (starts_with($filter, 'year-')) {
                $is_filter = true;
                $this->checkFilter($filter, 'year');
            }

            if (starts_with($filter, 'month-')) {
                $is_filter = true;
                $this->checkFilter($filter, 'month');
            }

            if(!$is_filter){
                $this::get404();
            }

        }

        $listNews = \App\News::filterDate($this->GetFilterDate())->orderBy('date', 'desc')->paginate(12);

        // перевод даты публикации
        foreach ($listNews as $news){
            $news['date'] = $news->getDate();
        }

        $filtersYear = [
            'list' => $this->filterYearValue,
            'setValue' => $this->filterYearShow,
            'setUrl' => $this->filterYearUrl
        ];

        $filtersMonth = [
            'list' => $this->filterMonthValue,
            'setValue' => $this->filterMonthShow,
            'setUrl' => $this->filterMonthUrl
        ];

        $filter = [
            'year' => $filtersYear,
            'month' => $filtersMonth
        ];

        // пересылаем данные при ajax загрузке
        if (\Illuminate\Support\Facades\Request::ajax()) {
            $data = [];
            $data = array_add($data, 'news', $listNews);
            $data = array_add($data, 'filter', $filter);

            header('Cache-Control: no-cache, no-store, must-revalidate');
            header('Pragma: no-cache');
            header('Expires: 0');

            return json_encode($data);
        }

        return view('pages.archive', [
            'listNews' => $listNews,
            'filter' => $filter,
        ]);
    }

    protected function checkFilter($filterValue, $typeFilter)
    {
        $value = str_replace($typeFilter . '-', '', $filterValue);

        if ($typeFilter == 'year') {
            if (array_has($this->filterYearValue, $value)) {
                $this->filterYearShow = $value;
                $this->filterYearUrl = $value;
                return $this->filterYear = $value;
            }
        }

        if ($typeFilter == 'month') {
            if (array_has($this->filterMonthValue, $value)) {
                $this->filterMonthShow = $this->filterMonthValue[$value]['name'];
                $this->filterMonthUrl = $value;
                return $this->filterMonth = $this->filterMonthValue[$value]['value'];
            }
        }

        $this::get404();
    }

    protected function GetFilterDate()
    {
        $arFilter = [];
        if ($this->filterMonth && $this->filterYear) {
            $fullDate = [];
            $fullDate[0] = $this->filterYear.'-'.$this->filterMonth.'-1';
            $fullDate[1] = $this->filterYear.'-'.$this->filterMonth.'-31';
            $arFilter = array_add($arFilter,'full', $fullDate);
            return $arFilter;
        }

        if ($this->filterMonth) {
            $arFilter = array_add($arFilter,'month', $this->filterMonth);
            return $arFilter;
        }

        if ($this->filterYear) {
            $arFilter = array_add($arFilter,'year', $this->filterYear);
            return $arFilter;
        }
        return $arFilter;
    }

    protected function get404()
    {
        abort('404', 'Страница 404 ошибки');
    }
}
