<?php

namespace App\Http\Controllers;

use App\File;
use App\History;
use App\Info;
use Illuminate\Http\Request;

class StaticPageController extends Controller
{
    public function history(){
        $history = History::active()->orderBy('date', 'desc')->get();

        $maxYear = date('Y', strtotime($history->max('date')));
        $minYear = date('Y', strtotime($history->min('date')));

        $renderYear = [];
        for ($i = $minYear; $i <= $maxYear; $i++) {
            $renderYear[$i] = (int)$i;
        }

        foreach ($history as $item){
            $item['date'] = date('Y', strtotime($item->date));
        }

        // todo : Вывести информацию о тренерах и видео

        return view('pages.history', [
            'years' => $renderYear,
            'history' => $history,
        ]);
    }

    public function contact(){
        return view('pages.contact', [
        ]);
    }

    public function media(){

        $files = File::active()->get();

        foreach ($files as $file){
            $photo = '';
            $extension = pathinfo($file->file)['extension'];
            $extensionsImg = ['JPEG'=>'JPEG', 'png'=>'png', 'jpeg'=>'jpeg', 'jpg'=>'jpg', 'gif'=>'gif', 'bmp'=>'bmp'];
            if(array_has($extensionsImg, $extension)){
                $photo = '/'.$file->file;
            } else {
                $photo = '/img/load-file-photo.png';
            }
            $file->photo = $photo;
        }

        return view('pages.media', [
            'files' => $files
        ]);
    }

    public function volejbolnyj_center(){
        return view('pages.volejbolnyj_center', [
        ]);
    }

    public function links(){
        $links = Info::getInfo('links')->first();

        return view('pages.links', [
            'links' => json_decode($links->content)
        ]);
    }

    public function gorodskaja_federacija(){
        return view('pages.gorodskaja_federacija', [
        ]);
    }

    public function search(){
        return view('pages.search', [
        ]);
    }


}
