<?php

namespace App\Http\Controllers;

use App\Player;
use App\Team;
use App\Trainer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function command($url_team)
    {
        $team = Team::GetTeam($url_team);

        if(!$team->first()){
            $this::get404();
        }

        $markItemInfo = $team->GetInfoMark();

        return view('pages.team-page', [
            "team" => $team,
            "players" => collect($markItemInfo['players']),
            "trainers" => collect($markItemInfo['trainers'])
        ]);
    }

    public function playerShow($url_player){
        $player = Player::GetPlayer($url_player);

        if(!$player->first()){
            $this::get404();
        }

        return view('pages.player', [
            'player' => $player,
        ]);
    }

    public function trainerShow($url_trainer){
        $trainer = Trainer::GetTrainer($url_trainer);

        if(!$trainer->first()){
            $this::get404();
        }

        return view('pages.trainer', [
            'trainer' => $trainer,
        ]);
    }


    protected function get404()
    {
        abort('404', 'Страница 404 ошибки');
    }
}
