<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\FundamentalSetting::class => 'App\Http\Sections\fundamentalSettings',
        \App\News::class => 'App\Http\Sections\news',
        \App\Player::class => 'App\Http\Sections\players',
        \App\Trainer::class => 'App\Http\Sections\trainers',
        \App\Team::class => 'App\Http\Sections\teams',
        \App\Gallerie::class => 'App\Http\Sections\galleries',
        \App\Gallerie::class => 'App\Http\Sections\galleries',
        \App\GallerieItem::class => 'App\Http\Sections\gallerieItems',
        \App\Rival::class => 'App\Http\Sections\Rivals',
        \App\Match::class => 'App\Http\Sections\Matches',
        \App\History::class => 'App\Http\Sections\Histories',
        \App\Partner::class => 'App\Http\Sections\Partners',
        \App\File::class => 'App\Http\Sections\Files',
        \App\Info::class => 'App\Http\Sections\Infos',
        \App\Link::class => 'App\Http\Sections\Links',
    ];
    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
        parent::boot($admin);
    }
}
