<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Date\DateFormat;
class GallerieItem extends Model
{
    public function Gallerie()
    {
        return $this->belongsTo('App\Gallerie');
    }

    public function getDate()
    {
        return DateFormat::date($this->date);
    }
}
