<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected $table = 'infos';

    public function scopeGetInfo($query, $slug){
        return $query->where('slug', $slug);
    }
}
