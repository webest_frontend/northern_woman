<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use \SleepingOwl\Admin\Traits\OrderableModel;

    public function getOrderField()
    {
        return 'sort';
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1)->orderBy('sort', 'asc');
    }
}
