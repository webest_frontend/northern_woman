<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rival extends Model
{
    protected $table='rivals';
}
