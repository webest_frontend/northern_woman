<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Team extends Model
{
    protected $table = "teams";

    protected static function boot()
    {
        parent::boot();

        Team::updating(function ($model_request) {
            $model_request->team_left = $_POST['json-team_left'];
            $model_request->team_joined = $_POST['json-team_joined'];
            $model_request->info_marks = $_POST['json-marks'];
            $model_request->coaching_staff = $_POST['json-coaching_staff'];
            return $model_request->url = str_slug($model_request->name);
        });

        Team::creating(function ($model_request) {
            $model_request->team_left = $_POST['json-team_left'];
            $model_request->team_joined = $_POST['json-team_joined'];
            $model_request->info_marks = $_POST['json-marks'];
            $model_request->coaching_staff = $_POST['json-coaching_staff'];
            return $model_request->url = str_slug($model_request->name);
        });
    }

    public function scopeGetTeam($query, $url){
        return $query->active()->where('url', '=', $url)->first();
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function getUrlAttribute($value)
    {
        return route('team-command', ['url_team' => $value]);
    }

    public function players()
    {
        return $this->hasMany('App\Player');
    }

    public function trainer()
    {
        return $this->hasMany('App\Trainer');
    }

    public function GetInfoMark(){
        $playersID = [];
        $trainersID = [];
        $marks = json_decode($this->info_marks);

        foreach ($marks as $mark){
            if($mark->type == 'player'){
                $playersID[] = (int)$mark->id;
            }
            if($mark->type == 'trainer'){
                $trainersID[] = (int)$mark->id;
            }
        }

        $infoPlayers = DB::table('players')
            ->whereIn('id', $playersID)
            ->get();

        $infoTrainers = DB::table('trainers')
            ->whereIn('id', $trainersID)
            ->get();

        return ['players' => $infoPlayers, 'trainers' => $infoTrainers];
    }
}
