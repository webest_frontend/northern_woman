<?php namespace App;
class Link extends Info
{
    protected static function boot()
    {
        parent::boot();

        Link::updating(function ($model_request) {
            return $model_request->slug = 'links';
        });

        Link::creating(function ($model_request) {
            return $model_request->slug = 'links';
        });
    }
}
