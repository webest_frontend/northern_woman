<?php

namespace App;

use Carbon\Carbon;
use Date\DateFormat;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    protected $table = 'news';

    protected static function boot()
    {
        parent::boot();

        News::updating(function ($model_request) {
            return $model_request->url = str_slug($model_request->title);
        });

        News::creating(function ($model_request) {
            return $model_request->url = str_slug($model_request->title);
        });
    }

    public function getDate()
    {
        return DateFormat::date($this->date);
    }


    public function scopePublished($query)
    {
        return $query->active()->publishedDate()->orderBy('date', 'desc');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1)->orderBy('date', 'desc');
    }

    public function scopePublishedDate($query)
    {
        return $query->active()->where('date', '<=', Carbon::now());
    }


    public function scopeGetByUrl($query, $url)
    {
        return $query->active()->where('url', '=', $url)->first();
    }

    public function scopeGetSidebarNews($query, $url)
    {
        return $query->active()->where('url', '!=', $url)->take(3)->orderBy('date', 'desc')->get();
    }


    public function scopeGetSliderNews($query)
    {
        return $query->publishedDate()->where('published_slider', 1)->orderBy('date', 'desc')->get();
    }


    static function getYearFilter()
    {
        $maxYear = News::max('date');
        $minYear = News::min('date');

        $timestampMax = strtotime($maxYear);
        $maxYearValue = date('Y', $timestampMax);

        $timestampMin = strtotime($minYear);
        $minYearValue = date('Y', $timestampMin);

        $filterYear = [];
        for ($i = $minYearValue; $i <= $maxYearValue; $i++) {
            $filterYear[$i] = $i;
        }

        return $filterYear;
    }

    public function scopeFilterDate($query, $arDate)
    {
        if (array_has($arDate, 'full')) {
             return $query->publishedDate()->where([
                ['date', '>=', Carbon::createFromFormat('Y-m-d', $arDate['full'][0])],
                ['date', '<=',  Carbon::createFromFormat('Y-m-d', $arDate['full'][1])],
            ]);
        }

        if (array_has($arDate, 'month')) {
            return $query->publishedDate()->whereMonth('date', $arDate['month']);
        }

        if (array_has($arDate, 'year')) {
            return $query->publishedDate()->whereYear('date', $arDate['year']);
        }

        return $query->publishedDate();

    }

    public function getUrlAttribute($value)
    {
        return route('news-element', ['url' => $value]);
    }


}
