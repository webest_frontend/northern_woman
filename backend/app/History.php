<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    public function scopeActive($query)
    {
        return $query->where('active', 1)->orderBy('date', 'desc');
    }
}
