<?php

return [
    'later'             => ':date в :time',
    'today'             => 'сегодня в :time',
    'yesterday'         => 'вчера в :time',
    'later_date'             => ':date',
    'today_date'             => 'сегодня',
    'yesterday_date'         => 'вчера',
    'month_declensions' => [
        'January'   => 'Января',
        'February'  => 'Февраля',
        'March'     => 'Марта',
        'April'     => 'Апреля',
        'May'       => 'Мая',
        'June'      => 'Июня',
        'July'      => 'Июля',
        'August'    => 'Августа',
        'September' => 'Сентября',
        'October'   => 'Октября',
        'November'  => 'Ноября',
        'December'  => 'Декабря'
    ]
];