function dd(value) {
    console.log(value);
}

Vue.component('db-links', Vue.extend({
    template: '#db-links',
    name: 'db-links',
    props: {
        data : []
    },
    data: function () {
        return {
            category : {},
            categories : [],
            number: 0
        }
    },
    mounted: function () {
        this.categories = this.data;
        this.number = this.categories.length;
        this.category =  {
            number : this.number,
            name : '',
            links : []
        };
    },
    methods: {
        addCategory : function () {
            if(this.category.name){
                this.categories.push(this.category);
                this.number = this.number + 1;
                this.category =  {
                    number : this.number,
                    name : '',
                    links : []
                };
            }else {
                Admin.Messages.error('', 'Введите Название категории')
            }
        },
        deleteCategory : function (index) {
            this.categories.splice(index, 1);
        },
        save : function () {
            var that = this;
            $.ajax({
                url : '/admin/links',
                data : 'json-links='+that.jsonLinks,
                dataType: 'json',
                method : 'POST'
            }).done(function (response) {
                Admin.Messages.success('Данные успешно сохранены')
            }).fail(function () {
                Admin.Messages.error('', 'Данные не удалось сохранить')
            });

        }
    },
    computed: {
        jsonLinks: function () {
            if (this.categories.length == 0) {
                return ''
            }
            return JSON.stringify(this.categories)
        }
    },

}));

Vue.component('link-cat', Vue.extend({
    template: '<div style="display: flex;"><a style="margin-right: 10px;" target="_blank" v-bind:href="link.href" v-bind:title="link.name"> {{link.name}} </a>' +
    '<button style="width: 21px; height: 21px;" v-on:click="deleteLink" class="btn btn-xs btn-danger btn-delete"  title="Удалить ссылку"> ' +
        '<i class="fa fa-times"></i> </button></div>',
    name: 'link-cat',
    props: {
        'link' : [],
        index : ''
    },
    methods: {
        deleteLink: function () {
            this.$emit('deleteLink', this.index);
        }
    }
}));


Vue.component('category-link', Vue.extend({
    template: '#category-link',
    name: 'category-link',
    props: {
        'category' : [],
        'index' : ''
    },
    data : function () {
        return {
            link : {
                'name' : '',
                'href' : ''
            },
            number : 0
        }
    },
    mounted: function () {
        this.number = this.category.links.length;
        this.category =  {
            'number' : this.number,
            'name' : '',
            'href' : ''
        };
    },
    methods: {
        addLink : function () {
            if(!this.link.name){
               Admin.Messages.error('', 'Введите заголов ссылки');
               return ;
            }
            if(!this.link.href){
                Admin.Messages.error('', 'Введите url адрес');
                return ;
            }
            this.category.links.push(this.link);
            this.number = this.number + 1;
            this.link = {
                number : this.number,
                'name' : '',
                'href' : ''
            }
        },
        deleteLink : function (index) {
            dd(index)
            this.category.links.splice(index, 1);
        },
        deleteSection : function () {
            this.$emit('deleteSection', this.index);
        }
    }
}));



Vue.component('category-name', Vue.extend({
    template: '<div style="display: flex;margin-bottom: 20px"><input style="margin-right: 10px;" type="text" v-model="name"  v-on:input="updateValue($event.target.value)" class="form-control">' +
    '<button style="width: 34px;" v-on:click="deleteItem" class="btn btn-xs btn-danger btn-delete"  title="Удалить категорию"> ' +
    '<i class="fa fa-trash"></i> </button></div>',
    name: 'category-name',
    props: {
        'name' : []
    },
    methods: {
        updateValue: function () {
            this.$emit('input', this.name)
        },
        deleteItem : function () {
            this.$emit('delete')
        }
    }
}));
