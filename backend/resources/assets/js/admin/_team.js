$(document).on('click', '.js-toggle-mark', function () {
    var mark = $(this).parent();

    $('.photo-map__mark').each(function (index, element) {
        if (element.classList.contains('show-mark-content') && mark[0] != element) {
            element.classList.remove('show-mark-content');
        }
    });
    mark.toggleClass('show-mark-content');
});

Vue.component('mark-item', Vue.extend({
    template: '#mark-item',
    name: 'mark-item',
    props: {
        item: '',
        index: '',
        players: [],
        trainers: []
    },
    data: function () {
        return {
            'dragDrop': {}
        }
    },
    methods: {
        deleteItem: function () {
            this.$emit('delete', this.index)
        },
        setPosition: function () {
            this.item.left = this.dragDrop.GetData().left ? this.dragDrop.GetData().left : this.item.left;
            this.item.top = this.dragDrop.GetData().top ? this.dragDrop.GetData().top : this.item.top;
        }
    },
    mounted: function () {
        this.dragDrop = Dragger(window);
        var element = document.getElementsByClassName('js-mark-' + this.index);
        this.dragDrop.makeDragDrop(element);
    }
}));

Vue.component('custom-select', Vue.extend({
    name: 'custom-select',
    props: {
        list: '',
        selected: ''
    },
    template: ' <select v-model="selected"  v-on:input="updateValue($event.target.value)" > ' +
    '<option  v-bind:value="option.id" v-for="option in list">' +
    '{{option.name}} ' +
    '</option> ' +
    '</select>',
    methods: {
        updateValue: function (value) {
            this.selected = value;
            this.$emit('input', this.selected)
        }
    }
}));

Vue.component('mark-list', Vue.extend({
    template: '#mark-list',
    name: 'mark-list',
    props: {
        'data': '',
        'players': [],
        'trainers': [],
        'nameJson': [],
    },
    data: function () {
        return {
            'markList': {},
            'mark': {
                "id": '',
                "type": '',
                "left": '20',
                "top": '20'
            },
            'number': 0,
            'src_photo': null
        }
    },
    computed: {
        jsonData: function () {
            if (this.markList.length == 0) {
                return ''
            }
            return JSON.stringify(this.markList)
        }
    },
    mounted: function () {
        this.markList = this.data;
        this.number = this.markList.length;
        this.mark = {
            "number": this.number,
            "id": '',
            "type": '',
            "left": '20',
            "top": '20'
        };
        this.src_photo = __SRC_PHOTO__
    },
    methods: {
        addMark: function () {
            this.markList.push(this.mark);
            this.number = this.number + 1;
            this.mark = {
                "number": this.number,
                "id": '',
                "type": '',
                "left": '20',
                "top": '20'
            }

        },
        deleteItemGlobal: function (index) {
            this.markList.splice(index, 1);
        }
    }
}));


function Dragger(GLOBAL) {

    var DOC = GLOBAL.document,
        left,
        top;

    var that = this;

    // Действия когда завершили перетаскивание:
    function stopDrag() {
        DOC.onmousemove = null;
        DOC.onselectstart = null;
    }

    // Служебная функция, позволяет кроссбраузерно получить прокрутку страницы:
    function pageOffset() {
        return {
            x: GLOBAL.pageXOffset || DOC.documentElement.scrollLeft || DOC.body.scrollLeft,
            y: GLOBAL.pageYOffset || DOC.documentElement.scrollTop || DOC.body.scrollTop
        };
    }

    // Действия во время перетаскивания:
    function process(element, dx, dy) {
        if (element.classList.contains('js-drag-event')) {
            // Корректный запрет выделения в Chrom Safari:
            DOC.onselectstart = function () {
                return false;
            };
            // Обработчик нужно ставить именно на объект document:
            DOC.onmousemove = function (event) {
                // Кроссбраузерно получаем объект события:
                var e = GLOBAL.event || event;

                // Запрет выделения в Opera IE
                if (GLOBAL.getSelection) {
                    GLOBAL.getSelection().removeAllRanges();
                } else if (DOC.selection && DOC.selection.clear) {
                    DOC.selection.clear();
                }
                // Высчитываем новое положение перетаскиваемого элемента, с
                // учётом дельты:
                left = e.clientX + pageOffset().x - dx;
                top = e.clientY + pageOffset().y - dy;

                that.leftPos = left;
                that.topPos = top;

                element.style.left = left + "px";
                element.style.top = top + "px";
            }
        }
    }

    // Основная функция-метод, вызываемая из вне:
    // param elements:NodeList - содержит элементы,
    // которые нужно сделать перетаскиваемыми.
    function dragDrop(elements) {

        var length, deltaX, deltaY, i;
        length = elements.length;

        // Действия когда нажали кнопку мыши:
        function startDrag(event) {
            // Кроссбраузерно получаем объект события и текущую цель:
            var e = GLOBAL.event || event,
                target = e.srcElement || e.target;

            // Высчитываем дельту, обязательно только в момент нажатия кнопки мыши:
            deltaX = e.clientX + pageOffset().x - target.offsetLeft;
            deltaY = e.clientY + pageOffset().y - target.offsetTop;
            // Запускаем процесс перетаскивания:
            process(target, deltaX, deltaY);
        }

        // В цикле проходим по всему списку элементов:
        for (i = 0; i < length; i += 1) {
            // Здесь всё начинается. Задаем позиционирование и раздаём обработчики:
            elements[i].style.position = "absolute";
            elements[i].onmousedown = startDrag;
        }
    }

    function getData() {
        return {
            'left': that.leftPos,
            'top': that.topPos
        }
    }

    // Отпустили кнопку - конец перетаскивания:
    DOC.onmouseup = stopDrag;
    // Возвращаем объект:
    return {
        makeDragDrop: dragDrop,
        GetData: getData
    };
}

( function (window, document) {
    'use strict';

    var file = '/img/sprite/sprite.svg',
        revision = 1;

    if (!document.createElementNS || !document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect)
        return true;

    var isLocalStorage = 'localStorage' in window && window['localStorage'] !== null,
        request,
        data,
        SVG_container = document.getElementById('SVG_container'),
        insertIT = function () {
            SVG_container.insertAdjacentHTML('afterbegin', data);
        },
        insert = function () {
            if (document.body) insertIT();
            else document.addEventListener('DOMContentLoaded', insertIT);
        };

    if (isLocalStorage && localStorage.getItem('inlineSVGrev') == revision) {
        data = localStorage.getItem('inlineSVGdata');
        if (data) {
            insert();
            return true;
        }
    }

    try {
        request = new XMLHttpRequest();
        request.open('GET', file, true);
        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                data = request.responseText;
                insert();
                if (isLocalStorage) {
                    localStorage.setItem('inlineSVGdata', data);
                    localStorage.setItem('inlineSVGrev', revision);
                }
            }
        }
        request.send();
    }
    catch (e) {
    }

}(window, document) );