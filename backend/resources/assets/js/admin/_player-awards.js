Vue.component('row-awards', Vue.extend({
    template: '#row-awards',
    props : {
        info : '',
        index : ''
    },
    methods: {
        deleteItem : function () {
            this.$emit('delete', this.index)
        }
    }
}));

Vue.component('adwardsList', Vue.extend({
    template:   '#awards-list',
    data : function () {
        return {
            reward : {
                name : '',
                year : '',
                type : ''
            },
            listAwards : __awards__
        }
    },
    methods: {
        deleteRow: function (index) {
            this.listAwards.splice(index, 1);
        },
        addRow : function () {
            this.listAwards.push(this.reward);
            this.reward =  {
                name : '',
                year : '',
                type : ''
            }
        }
    },
    computed : {
        jsonData : function () {
            if(this.listAwards.length == 0 ){
                return ''
            }
            return JSON.stringify(this.listAwards)
        }  
    }
}));


Vue.component('info-row', Vue.extend({
    template: '#info-row',
    props : {
        info : '',
        index : '',
        configItem : '',
        classRow: ''
    },
    methods: {
        deleteItem : function () {
            this.$emit('delete', this.index)
        }
    }
}));

Vue.component('info-list', Vue.extend({
    template:   '#info-list',
    props :{
        data : '',
        templateData : '',
        nameJson : '',
        config: '',
        classRow : '',
        nameSetting : ''
    },
    data : function () {
        return {
            listInfo : [],
            infoItem : []
        }
    },
    mounted : function(){
        this.listInfo = this.data;
        this.infoItem = this.templateData;
    },
    updated : function() {
        // Передаем глобальный контекст, внутри этой функции this = window
        var dragDrop = Dragger(window);
        var divs = document.getElementsByClassName("js-drag-and-drop");
        dragDrop.makeDragDrop(divs);
    },
    methods: {
        deleteRow: function (index) {
            this.listInfo.splice(index, 1);
        },
        addRow : function () {
            var copy = {};
            for (var key in this.templateData) {
                copy[key] = this.templateData[key];
            }
            this.listInfo.push(copy);
            //this.infoItem = this.templateData;
        }
    },
    computed : {
        jsonData : function () {
            if(this.listInfo.length == 0 ){
                return ''
            }
            return JSON.stringify(this.listInfo)
        }
    }
}));

