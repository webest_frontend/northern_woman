$(document).on('click', '.js-toggle--media-menu', function() {
	dd('toggle media menu')
	$('.js-media-menu').toggleClass('show-media-menu');
	$('body').toggleClass('no-scroll');
	
	$('.js-menu-liks').removeClass('show-lvl-menu');
	$('.wrap-menu-lvl').removeClass('show--this-lvl-menu');
});


$(document).on('click', '.js-show--lvl-menu', function(){
	var menu = $(this).attr('data-id-menu');

	$('#'+menu).toggleClass('show--this-lvl-menu');


	$('.js-menu-liks').toggleClass('show-lvl-menu');
});
