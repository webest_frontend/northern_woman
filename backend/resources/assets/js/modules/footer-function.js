$(document).on('click', '.js-toggle-footer-menu', function() {
	if($(window).width() > 690) return false;

	$(this).toggleClass('active');
	$(this).parent().find('.footer-nav__list').slideToggle();
})