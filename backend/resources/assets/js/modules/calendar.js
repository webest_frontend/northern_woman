
// Calendar = {
// 	init : function(){
// 		this.eventHandler();
// 	},

// 	eventHandler : function() {

// 	}
// };



calendare_slider = $('.js-calendare-slider').lightSlider({
	item:1,
	loop: false,
	slideMargin: 0,
	auto:false,
	pauseOnHover: true,
	slideMove:1,
	easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
	speed:900,
	pager: false,
	responsive : [
	{
		breakpoint:800,
		settings: {
			item:3,
			slideMove:1,
		}
	},
	{
		breakpoint:480,
		settings: {
			item:2,
			slideMove:1
		}
	}
	]
});