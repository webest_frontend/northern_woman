$(document).on('click', '.js-tooggle-nearest-games', function () {
	$('.container--nearest-games').toggleClass('js-show-games');
	//$('.nearest-games__body').toggle();
});


$(document).on('click', '.js-toggle-popup-nav', function(event) {
	var nav = $('[data-popup-nav="'+$(this).attr('data-target-nav')+'"]');
	nav.toggleClass('js-show-popup-nav');
	event.preventDefault();
});


function balanceMatchesHeight (container) {

	var maxHeight = 0;
	$(container).find('.card-match--small').each(function (index, element) {
		if(maxHeight < element.offsetHeight){
			maxHeight = element.offsetHeight
		}
	});

	$(container).find('.card-match--small').each(function (index, element) {
		element.style.height = maxHeight + 'px';
	});
}

balanceMatchesHeight('.js-balans-matches');


$(document).on('click', '.js-toggle-stadium-map', function(){
	$('.card-match__location-map').toggleClass('show-map')
});

SelectLanguage = {
	language : 'RU',

	init : function(){
		this.eventHandler();
	},

	eventHandler: function(){
		var that = this; 

		$(document).on('click', '.js-open-language', function(){
			that.showVariantLanguge();
		});


		$(document).on('click', '.js-select-language', function(){
			that.language = $(this).text();
			that.showVariantLanguge();
		});
	},

	showVariantLanguge : function(){
		$('.b-select-language').toggleClass('js-show-select-language')
		this.showSelectLanguage();
	},

	showSelectLanguage : function(){
		$('.js-language-selected').text(this.language);
	}
}

SelectLanguage.init();