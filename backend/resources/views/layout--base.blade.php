@extends('layout')


@section('layout-content')

@yield('content')

    <div class="section-all-page">
        @include('widget.photo-gallery')
        @include('widget.partners')

    </div>

    <footer class="footer">
        <div class="bg-gray">
            <div class="f-container">

                @include('widget.menu--footer')

            </div>
        </div>
        <div class="footer-info bg-d-blue">
            <div class="f-container f-container--footer-info">
                <div class="footer-info__contact">
                    <div class="footer-info__label">162600, г. Череповец, спорткомплекс  "Юбилейный", ул. Ленина 125</div><a href="#" class="footer-info__value">8 (8202) 57-58-59</a>
                </div>
                <div class="footer-info__phone">
                    <div class="footer-info__label">Телефон для справок по приобретению билетов: </div><a href="#" class="footer-info__value">8 (8202) 58-66-80</a>
                </div>
                <div class="footer-info__social bg-l-blue">
                    <ul class="list-soc">
                        <li class="list-soc__item"><a href="#">
                                <svg class="icon icon-2 small-icon">
                                    <use xlink:href="#2"></use>
                                </svg></a></li>
                        <li class="list-soc__item"><a href="#">
                                <svg class="icon icon-3 big-icon">
                                    <use xlink:href="#3"></use>
                                </svg></a></li>
                        <li class="list-soc__item"><a href="#">
                                <svg class="icon icon-4 ">
                                    <use xlink:href="#4"></use>
                                </svg></a></li>
                        <li class="list-soc__item"><a href="#">
                                <svg class="icon icon-5 ">
                                    <use xlink:href="#5"></use>
                                </svg></a></li>
                        <li class="list-soc__item"><a href="#">
                                <svg class="icon icon-6 ">
                                    <use xlink:href="#6"></use>
                                </svg></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="f-container f-container--footer-bottom">
                <div class="wrap-copy">
                    <div class="footer-bottom__copy">© 1999-2017 гг. При использовании материалов сайта ссылка на него обязательна.</div><a href="#" class="footer-bottom__privat">Пользовательское соглашение</a>
                </div>
                <div class="wrap-webest"><span>разработано</span><a href="#" class="footer-bottom__webest">
                        <svg class="icon icon-1 ">
                            <use xlink:href="#1"></use>
                        </svg></a></div>
            </div>
        </div>
    </footer>
@endsection
