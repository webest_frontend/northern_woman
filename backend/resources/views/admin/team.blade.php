@php
    $coaching_staff = $model->coaching_staff ? $model->coaching_staff : '[]';
    $playerList = $model->players;
    $trainerList = \App\Trainer::active()->get();
    $photo = $model->team_photo ? $model->team_photo : '';
    $markList =  $model->info_marks ? $model->info_marks : '[]';
    $team_left =  $model->team_left ? $model->team_left : '[]';
    $team_joined =  $model->team_joined ? $model->team_joined : '[]';



@endphp


<div class="row">
    <div class="col-md-12">
        <div class="title"
             style=" font-weight: bold;
                     margin-top: 20px;
                     margin-bottom: 20px;">
            Метки на фотографии
        </div>
    </div>
</div>
<mark-list
        :name-json="'json-marks'"
        :players='{!! $playerList !!}'
        :trainers='{!! $trainerList  !!}'
        :data='{!! $markList !!}'>
</mark-list>

<info-list
        :data='{!! $team_left !!}'
        :name-setting="'Команду покинули'"
        :config='[{"name_input" : "name", "label" : "Имя"},{"name_input" : "amplua", "label" : "Амплуа"},{"name_input" : "career", "label" : "Карьера"}]'
        :name-json="'json-team_left'"
        :class-row="'col-md-3'"
        :template-data='{"name" : "", "amplua" : "", "career" : ""}'>
</info-list>

<info-list
        :data='{!! $team_joined !!}'
        :name-setting="'Команду пополнили'"
        :config='[{"name_input" : "name", "label" : "Имя"},{"name_input" : "amplua", "label" : "Амплуа"},{"name_input" : "career", "label" : "Карьера"}]'
        :name-json="'json-team_joined'"
        :class-row="'col-md-3'"
        :template-data='{"name" : "", "amplua" : "", "career" : ""}'>
</info-list>

<info-list
        :data='{!! $coaching_staff !!}'
        :name-setting="'Тренерский штаб команды'"
        :config='[{"name_input" : "name", "label" : "ФИО тренера"},{"name_input" : "value", "label" : "Должность"}]'
        :name-json="'json-coaching_staff'"
        :class-row="'col-md-4'"
        :template-data='{"name" : "", "value" : ""}'>
</info-list>




<script>window.jQuery || document.write('<script src="/js/jquery-3.2.1.min.js"><\/script>')</script>
<script>
    var __SRC_PHOTO__ = "/{{$photo}}"
</script>
@verbatim
    <script type="text/x-template" id="mark-item">
        <div class="photo-map__mark js-drag-and-drop js-drag-event" @mouseup="setPosition"
             v-bind:class="'js-mark-'+index">
            <button class="photo-map__mark-action js-toggle-mark"
                    type="button">
                <svg class="icon icon-11 photo-map__mark-icon">
                    <use xlink:href="#11"></use>
                </svg>
            </button>
            <div class="photo-map__mark-content">

                <div class="photo-map__radio-wrap">
                    <input type="radio" v-bind:id="'player-'+index" value="player" v-model="item.type">
                    <label v-bind:for="'player-'+index">Игрок</label>
                </div>

                <div class="photo-map__radio-wrap">
                    <input type="radio" v-bind:id="'trainer-'+index" value="trainer" v-model="item.type">
                    <label v-bind:for="'trainer-'+index">Тренер</label>
                </div>

                <custom-select
                        @input="value => { item.id = value }"
                        v-if="item.type == 'player'"
                        :selected="item.id"
                        :list="players">
                </custom-select>

                <custom-select
                        @input="value => { item.id = value }"
                        v-if="item.type == 'trainer'"
                        :selected="item.id"
                        :list="trainers">
                </custom-select>


                <button v-on:click="deleteItem" type="button" class="photo-map__mark-delete btn btn--blue">Удалить
                </button>
            </div>
        </div>
    </script>
@endverbatim
<script type="text/x-template" id="mark-list">
    <div id="js-photo-map" class="photo-map" v-if="src_photo">
        <img :src="src_photo" class="photo-map__img">

        <mark-item v-on:delete="deleteItemGlobal"
                   v-bind:item="item"
                   v-bind:index="index"
                   :key="item.number"
                   v-bind:players='players'
                   v-bind:trainers='trainers'
                   v-bind:style="{ left: item.left + 'px', top: item.top + 'px' }"
                   v-for="(item, index) in markList">
        </mark-item>

        <div style="margin-top: 20px" class="f-container">
            <button type="button"  v-on:click="addMark" class="btn btn-primary">
                <i class="fa fa-plus"></i>
                Добавить метку
            </button>
        </div>

        <input type="hidden" v-model="jsonData" :name="nameJson">
    </div>
</script>

<script type="text/x-template" id="info-row">
    <div class="row">
        <div v-bind:class="classRow" v-for="(item , index) in configItem">
            <div class="form-group form-element-text ">
                <input type="text"
                       v-bind:placeholder="item.label"
                       v-model="info[item.name_input]"
                       class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <button type="button"
                    v-on:click="deleteItem"
                    class="btn btn-danger btn-delete ">
                <i class="fa fa-times"></i>
                Удалить
            </button>
        </div>
    </div>
</script>

<script type="text/x-template" id="info-list">
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="title"
                     style=" font-weight: bold;
                     margin-top: 20px;
                     margin-bottom: 20px;">
                    @{{nameSetting}}
                </div>
            </div>
        </div>

        <info-row :info="element"
                  :index="index"
                  :label="label"
                  :config-item="config"
                  :classRow = "classRow"
                  v-on:delete="deleteRow"
                  v-for="(element, index) in listInfo">
        </info-row>

        <div class="row" style="margin-top: 15px;margin-bottom: 25px;">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary js-add-row-params"
                        @click="addRow">
                    <i class="fa fa-plus"></i>
                    Добавить параметр
                </button>
            </div>
        </div>
        <input type="hidden" :name="nameJson" v-model="jsonData">
    </div>
</script>