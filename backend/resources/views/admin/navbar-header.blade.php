@if ($user)
    <div id="SVG_container"></div>
    <li style="margin-right: 25px;">
        <a href="/" target="_blank">
            <i class="fa fa-btn fa-globe"></i> Главная страница
        </a>
    </li>
    <li style="margin-right: 25px;">
        <a href="/logout">
            <i class="fa fa-btn fa-sign-out"></i> Выход
        </a>
    </li>
@endif