@php

    if(\Illuminate\Support\Facades\Request::ajax()){
        $isSave = isset($_POST['json-links']) ? true : false;
        $dbLinks = \App\Info::GetInfo('links')->first();
        if($isSave){
            if(!$dbLinks){
                $result = \Illuminate\Support\Facades\DB::table('infos')
                    ->insert(
                        ['slug' => 'links', 'content' => $_POST['json-links']]
                    );
            } else {
                 $result = \Illuminate\Support\Facades\DB::table('infos')
                    ->where('slug', 'links')
                    ->update(['content' => $_POST['json-links']]);
            }
        }
        die(\Illuminate\Support\Facades\Request::ajax());

    } else {
        $dbLinks = \App\Info::GetInfo('links')->first();
        $jsonLink = $dbLinks ? $dbLinks->content : '[]';
    }
@endphp
<db-links
        :data='{!! $jsonLink !!}'>
</db-links>

<script type="text/x-template" id="db-links">
    <div>
        <div class="wrap-links">

            <category-link
                    :category="category"
                    :key="category.number"
                    :index="index"
                    v-on:deleteSection="deleteCategory"
                    v-for="(category, index) in categories">
            </category-link>
        </div>

        <div class="form-group form-element-text ">
            <label class="control-label">
                Название категории
            </label>
            <input type="text" v-model="category.name" class="form-control">
        </div>

        <button class="btn btn-primary" v-on:click="addCategory" type="button">
            <i class="fa fa-plus"></i>
            Добавить категорию
        </button>

        <button v-on:click="save"
                class="btn btn-primary">
            <i class="fa fa-check"></i>
            Сохранить изменения
        </button>

    </div>

    </div>
</script>


<script type="text/x-template" id="category-link">
    <div style="padding-bottom: 35px;" class="links-item">
        <category-name
        @delete="deleteSection"
        @input="value => { category.name = value }"
        :name="category.name">
        </category-name>

        <div class="links-item__content" style="margin-bottom: 35px;">
            <link-cat v-for="(link, index) in category.links"
                      :index="index"
                      :key="link.number"
            @deleteLink="deleteLink"
            :link="link">
            </link-cat>
        </div>

        <div class="row">
            <div class="col-md-7">
                <div class="form-group form-element-text ">
                    <input type="text" placeholder="Заголовок ссылки" v-model="link.name" class="form-control">
                </div>
                <div class="form-group form-element-text ">
                    <input type="text" v-model="link.href" placeholder="url адрес" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <button class="btn btn-primary" v-on:click="addLink" type="button">
                    <i class="fa fa-plus"></i>
                    Добавить ссылку
                </button>
            </div>
        </div>


    </div>
</script>