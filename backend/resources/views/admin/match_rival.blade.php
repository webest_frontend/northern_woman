@php
    $players = $model->rival_composition ? json_decode($model->rival_composition) : false
@endphp

@if($players)
    <div class="form-group" style="margin-top: 45px;">
        <div class="control-label"><b>Информация о игроках</b></div>
    </div>

    <div class="b-table table-white">
        <div class="table-container">
            <table class="table--center">
                <thead>
                <tr>
                    <th class="t-a-l">ИГРОК</th>
                    <th>N ИГРОКА</th>
                    <th class="t-a-l">АМПЛУА</th>
                    <th >ЗАБИТЫХ МЯЧЕЙ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($players as $player)
                    <tr>
                        <th class="t-a-l color w-1-3">{{$player->name}}</th>
                        <th>{{$player->nomer}}</th>
                        <th class="t-a-l">{{$player->amplua}}</th>
                        <th >{{$player->zabitykh_myachey}}</th>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif