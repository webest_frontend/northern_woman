@php
    $parameters = $model->parameters ? $model->parameters : '[]';;
@endphp

<info-list
        :data='{!! $parameters !!}'
        :name-setting = "'Параметры'"
        :config='[{"name_input" : "name", "label" : "Название параметра"},{"name_input" : "value", "label" : "Значение параметра"}]'
        :name-json="'json-parameters'"
        :template-data='{"name" : "", "value" : ""}'>
</info-list>

<script>window.jQuery || document.write('<script src="/js/jquery-3.2.1.min.js"><\/script>')</script>

<script type="text/x-template" id="info-row">
    <div class="row">
        <div class="col-md-4" v-for="(item , index) in configItem">
            <div class="form-group form-element-text ">
                <input type="text"
                       v-bind:placeholder="item.label"
                       v-model="info[item.name_input]"
                       class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <button type="button"
                    v-on:click="deleteItem"
                    class="btn btn-danger btn-delete ">
                <i class="fa fa-times"></i>
                Удалить
            </button>
        </div>
    </div>
</script>

<script type="text/x-template" id="info-list">
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="title"
                     style=" font-weight: bold;
                     margin-top: 20px;
                     margin-bottom: 20px;">
                    @{{nameSetting}}
                </div>
            </div>
        </div>

        <info-row :info="element"
                  :index="index"
                  :label="label"
                  :config-item="config"
                  v-on:delete="deleteRow"
                  v-for="(element, index) in listInfo">
        </info-row>

        <div class="row" style="margin-top: 15px;margin-bottom: 25px;">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary js-add-row-params"
                        @click="addRow">
                    <i class="fa fa-plus"></i>
                    Добавить параметр
                </button>
            </div>
        </div>
        <input type="hidden" :name="nameJson" v-model="jsonData">
    </div>
</script>