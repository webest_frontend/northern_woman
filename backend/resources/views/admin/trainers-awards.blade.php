@php

    $awards = $model->awards ? $model->awards : '[]';;
    $achievements = $model->achievements ? $model->achievements : '[]';
    $career = $model->career  ? $model->career  : json_encode([]);
    //dd($awards)

    //dd(json_decode($awards))

    //if(isset($_GET['params'])){
    //    $temp_arrParams = $_GET['params'];
    //    foreach ($temp_arrParams['name'] as $key => $item){
    //        $awards[] = array(
    //            'name' => $item,
    //            'year' => $temp_arrParams['year'][$key]
    //            );
    //    }
    //}
@endphp

<adwards-list></adwards-list>

{{--<achievements-list></achievements-list>--}}
<info-list
        :data='{!! $achievements !!}'
        :name-setting = "'Достижения тренера'"
        :config='[{"name_input" : "name", "label" : "Достижение"},{"name_input" : "year", "label" : "Год"}]'
        :name-json="'json-achievements'"
        :template-data='{"name" : "", "year" : ""}'>
</info-list>

<info-list
    :data='{!! $career !!}'
    :name-setting = "'Карьера'"
    :config='[{"name_input" : "location", "label" : "Место"},{"name_input" : "year", "label" : "Год"}]'
    :name-json="'json-career'"
    :template-data='{"location" : "", "year" : ""}'>
</info-list>

<script>window.jQuery || document.write('<script src="/js/jquery-3.2.1.min.js"><\/script>')</script>

<script>
    var __awards__ = {!! $awards !!};
</script>

{{--Шаблоны заполнения наград--}}
<script type="text/x-template" id="row-awards">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group form-element-text ">
                <input type="text"
                       placeholder="Название награды"
                       v-model="info.name"
                       class="form-control  js-input-params">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-element-text ">
                <input type="text"
                       placeholder="Год"
                       v-model="info.year"
                       class="form-control  js-input-params">
            </div>
        </div>
        <div class="col-md-2">
            <select v-model="info.type" class="form-control ">
                <option disabled value="">Тип награды</option>
                <option value="kubok">Кубок</option>
                <option value="medale">Медаль</option>
            </select>
        </div>
        <div class="col-md-2">
            <button type="button"
                    v-on:click="deleteItem"
                    class="btn btn-danger btn-delete ">
                <i class="fa fa-times"></i>
                Удалить
            </button>
        </div>
    </div>
</script>

<script type="text/x-template" id="awards-list">
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="title"
                     style=" font-weight: bold;
                     margin-top: 20px;
                     margin-bottom: 20px;">
                    Награды тренера
                </div>
            </div>
        </div>

        <row-awards :info="element"
                    :index="index"
                    v-on:delete="deleteRow"
                    v-for="(element, index) in listAwards">
        </row-awards>

        <div class="row" style="margin-top: 15px;margin-bottom: 25px;">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary js-add-row-params"
                        @click="addRow">
                    <i class="fa fa-plus"></i>
                    Добавить награду
                </button>
            </div>
        </div>
        <input type="hidden" name="json-awards" v-model="jsonData">
    </div>
</script>

{{--Шаблоны заполнения достижений--}}
<script type="text/x-template" id="achievements-row">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group form-element-text ">
                <input type="text"
                       placeholder="Название награды"
                       v-model="info.name"
                       class="form-control  js-input-params">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group form-element-text ">
                <input type="text"
                       placeholder="Год"
                       v-model="info.year"
                       class="form-control  js-input-params">
            </div>
        </div>
        <div class="col-md-2">
            <button type="button"
                    v-on:click="deleteItem"
                    class="btn btn-danger btn-delete ">
                <i class="fa fa-times"></i>
                Удалить
            </button>
        </div>
    </div>
</script>

<script type="text/x-template" id="achievements-list">
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="title"
                     style=" font-weight: bold;
                     margin-top: 20px;
                     margin-bottom: 20px;">
                    Достижения игрока
                </div>
            </div>
        </div>

        <achievements-row :info="element"
                    :index="index"
                    v-on:delete="deleteRow"
                    v-for="(element, index) in listAchievements">
        </achievements-row>

        <div class="row" style="margin-top: 15px;margin-bottom: 25px;">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary js-add-row-params"
                        @click="addRow">
                    <i class="fa fa-plus"></i>
                    Добавить достижение
                </button>
            </div>
        </div>
        <input type="hidden" name="json-achievements" v-model="jsonData">
    </div>
</script>

{{--Шаблоны заполнения карьеры--}}

<script type="text/x-template" id="info-row">
    <div class="row">
        <div class="col-md-4" v-for="(item , index) in configItem">
            <div class="form-group form-element-text ">
                <input type="text"
                       v-bind:placeholder="item.label"
                       v-model="info[item.name_input]"
                       class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <button type="button"
                    v-on:click="deleteItem"
                    class="btn btn-danger btn-delete ">
                <i class="fa fa-times"></i>
                Удалить
            </button>
        </div>
    </div>
</script>

<script type="text/x-template" id="info-list">
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="title"
                     style=" font-weight: bold;
                     margin-top: 20px;
                     margin-bottom: 20px;">
                    @{{nameSetting}}
                </div>
            </div>
        </div>

        <info-row :info="element"
                  :index="index"
                  :label="label"
                  :config-item="config"
                  v-on:delete="deleteRow"
                  v-for="(element, index) in listInfo">
        </info-row>

        <div class="row" style="margin-top: 15px;margin-bottom: 25px;">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary js-add-row-params"
                        @click="addRow">
                    <i class="fa fa-plus"></i>
                    Добавить елемент
                </button>
            </div>
        </div>
        <input type="hidden" :name="nameJson" v-model="jsonData">
    </div>
</script>