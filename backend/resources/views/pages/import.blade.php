@extends('layout--base')
@section('content')
    <div class="page-sorevnovanija">
        <div class="container">
            <div class="b-breadcrumbs"><a href="/" class="breadcrumbs__item--link">Главная </a><a href="/"
                                                                                                  class="breadcrumbs__item--link">Соревнования </a><span
                        class="breadcrumbs__item">Чемпионат Росии</span></div>
            <div class="title-default title-default--white title-default--mit-link"><span>Чемпионат России. Сезон 2016/17 </span>
                <div class="wrap-title-links"><a href="#" class="title-links__item title-links__item--blue">
                        <svg class="icon icon-15 ">
                            <use xlink:href="#15"></use>
                        </svg>
                    </a>
                    <div class="title-links__item title-links__item--blue">Сезон 2016/2017</div>
                    <a href="#" class="title-links__item title-links__item--blue">
                        <svg class="icon icon-16 ">
                            <use xlink:href="#16"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="table-description">
                <div class="table-description__title">Высшая лига А. Женщины (турнирная таблица)</div>
                <div class="table-description__content">
                    <table>
                        <tbody>
                        <tr>
                            <th class="table-description__item"><span class="value">И</span><span
                                        class="delmiter">-</span><span class="description">Игры</span></th>
                            <th class="table-description__item"><span class="value">П</span><span
                                        class="delmiter">-</span><span class="description">Поражения</span></th>
                        </tr>
                        <tr>
                            <th class="table-description__item"><span class="value">В</span><span
                                        class="delmiter">-</span><span class="description">Выигрыши</span></th>
                            <th class="table-description__item"><span class="value">О</span><span
                                        class="delmiter">-</span><span class="description">Очки</span></th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="do-scroll show-media--lg-flex">
                <svg class="icon icon-28 do-scroll__icon">
                    <use xlink:href="#28"></use>
                </svg>
                <div class="do-scroll__title">ПРОКРУТИТЕ<br>ТАБЛИЦУ</div>
            </div>
            <div class="wrap-media-table">
                <div class="b-table table-white table--tournament">
                    <div class="table-container">
                        <table class="table--center">
                            <thead>
                            <tr>
                                <th class="t-a-l">КОМАНДА</th>
                                <th>N</th>
                                <th class="color-gray">1</th>
                                <th class="color-gray">2</th>
                                <th class="color-gray">3</th>
                                <th class="color-gray">4</th>
                                <th class="color-gray">5</th>
                                <th class="color-gray">6</th>
                                <th class="color-gray">7</th>
                                <th class="color-gray">8</th>
                                <th class="color-gray">9</th>
                                <th>И</th>
                                <th>В</th>
                                <th>П</th>
                                <th>О</th>
                                <th>ПАР</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th class="t-a-l"><span class="color-dark">СЕВЕРЯНКА</span><br>ЧЕРЕПОВЕЦ</th>
                                <th>1</th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty"></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th>32</th>
                                <th class="color-dark">22</th>
                                <th>32</th>
                                <th>28</th>
                                <th>88:38</th>
                            </tr>
                            <tr>
                                <th class="t-a-l"><span class="color-dark">СЕВЕРЯНКА</span><br>ЧЕРЕПОВЕЦ</th>
                                <th>2</th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty"></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th>32</th>
                                <th class="color-dark">22</th>
                                <th>32</th>
                                <th>28</th>
                                <th>88:38</th>
                            </tr>
                            <tr>
                                <th class="t-a-l"><span class="color-dark">СЕВЕРЯНКА</span><br>ЧЕРЕПОВЕЦ</th>
                                <th>3</th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty"></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th>32</th>
                                <th class="color-dark">22</th>
                                <th>32</th>
                                <th>28</th>
                                <th>88:38</th>
                            </tr>
                            <tr>
                                <th class="t-a-l"><span class="color-dark">СЕВЕРЯНКА</span><br>ЧЕРЕПОВЕЦ</th>
                                <th>4</th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty"></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th>32</th>
                                <th class="color-dark">22</th>
                                <th>32</th>
                                <th>28</th>
                                <th>88:38</th>
                            </tr>
                            <tr>
                                <th class="t-a-l"><span class="color-dark">СЕВЕРЯНКА</span><br>ЧЕРЕПОВЕЦ</th>
                                <th>5</th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty"></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th>32</th>
                                <th class="color-dark">22</th>
                                <th>32</th>
                                <th>28</th>
                                <th>88:38</th>
                            </tr>
                            <tr>
                                <th class="t-a-l"><span class="color-dark">СЕВЕРЯНКА</span><br>ЧЕРЕПОВЕЦ</th>
                                <th>6</th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty"></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th>32</th>
                                <th class="color-dark">22</th>
                                <th>32</th>
                                <th>28</th>
                                <th>88:38</th>
                            </tr>
                            <tr>
                                <th class="t-a-l"><span class="color-dark">СЕВЕРЯНКА</span><br>ЧЕРЕПОВЕЦ</th>
                                <th>7</th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty"></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th>32</th>
                                <th class="color-dark">22</th>
                                <th>32</th>
                                <th>28</th>
                                <th>88:38</th>
                            </tr>
                            <tr>
                                <th class="t-a-l"><span class="color-dark">СЕВЕРЯНКА</span><br>ЧЕРЕПОВЕЦ</th>
                                <th>8</th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty"></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th>32</th>
                                <th class="color-dark">22</th>
                                <th>32</th>
                                <th>28</th>
                                <th>88:38</th>
                            </tr>
                            <tr>
                                <th class="t-a-l"><span class="color-dark">СЕВЕРЯНКА</span><br>ЧЕРЕПОВЕЦ</th>
                                <th>9</th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty"></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th class="color-gray th-is-empty">3:0 <br>3:0<br>3:1<br>3:2<br></th>
                                <th>32</th>
                                <th class="color-dark">22</th>
                                <th>32</th>
                                <th>28</th>
                                <th>88:38</th>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="title-default title-default--white"><span>Календарь турнира</span></div>
            <div class="do-scroll show-media--lg-flex">
                <svg class="icon icon-28 do-scroll__icon">
                    <use xlink:href="#28"></use>
                </svg>
                <div class="do-scroll__title">ПРОКРУТИТЕ<br>ТАБЛИЦУ</div>
            </div>
            <div class="wrap-media-table">
                <div class="b-table table-white table--tournament table--bg-row">
                    <div class="table-container">
                        <table class="table--center">
                            <thead>
                            <tr>
                                <th class="t-a-l">ДАТА</th>
                                <th>ТУР</th>
                                <th>КОМАНДЫ</th>
                                <th class="t-a-l">РЕЗУЛЬТАТЫ</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 0)
                            @foreach($data as $row)
                                @php($class = '')
                                <tr class="{{$class}}">
                                    <th class="t-a-l color-dark">

                                        {!! $row['date'] !!}
                                    </th>
                                    <th>{{$row['tour']}}</th>
                                    <th>
                                        <div class="wrap-table__teams">
                                            <div class="table__team">
                                                <div style="background-image: url('/img/severjanka.png')"
                                                     class="table__team__logo"></div>
                                                <div class="table__team__info">
                                                    <div class="table__team__name">{{$row['team_first']['name']}}</div>
                                                    <div class="table__team__city">{{$row['team_first']['location']}}</div>
                                                </div>
                                            </div>
                                            <div class="table__team__vs">VS</div>
                                            <div class="table__team">
                                                <div style="background-image: url('/img/primor.png')"
                                                     class="table__team__logo"></div>
                                                <div class="table__team__info">
                                                    <div class="table__team__name">{{$row['team_two']['name']}}</div>
                                                    <div class="table__team__city">{{$row['team_two']['location']}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="t-a-l">
                                        <span class="color-dark">{{$row['scope']['first_match']['short']}} </span>
                                        @if($row['scope']['first_match']['full'])
                                        ({{$row['scope']['first_match']['full']}})
                                        @endif
                                        <br>
                                        <span class="color-dark">{{$row['scope']['two_match']['short']}} </span>
                                        @if($row['scope']['two_match']['full'])
                                            ({{$row['scope']['two_match']['full']}})
                                        @endif
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="b-subscribe b-subscribe--full">
                <div class="b-subscribe__title">Будьте в курсе последних новостей:</div>
                <div class="b-subscribe__form">
                    <input type="email" name="email" placeholder="E-mail" class="input">
                    <button class="b-subscribe__send btn--blue">Подписаться</button>
                </div>
            </div>
        </div>
    </div>
@endsection
