@extends('layout--base')

@section('content')
    <div class="page-gamer">
        <div class="container">
            <div class="b-breadcrumbs">
                <a href="/" class="breadcrumbs__item--link">Главная </a>
                <a href="/" class="breadcrumbs__item--link">Команды </a>
                <span class="breadcrumbs__item">Северянка</span>
            </div>
            <div class="player-info">

                <a href="#" class="link-back">
                    <svg class="icon icon-15 ">
                        <use xlink:href="#15"></use>
                    </svg>
                    <span>Вернуться к команде</span>
                </a>

                <div class="player-info__header">
                    <div style="background-image: url('/{{$trainer->photo}}')" class="player-info__photo"></div>
                    <div class="player-info__content trener">
                        <div class="player-info__name"><span>{{$trainer->name}}</span></div>
                        <div class="player-info__team">СЕВЕРЯНКА-2</div>
                        <div class="player-info__status">
                            <div class="player-info__status-type">{{$trainer->role_in_team}}</div>
                            <div class="player-info__status-league">{{$trainer->type_team}}</div>
                        </div>
                    </div>
                    <div class="player-info__table">
                        <div class="player-info__table-row">
                            <div class="player-info__table-cell">
                                @php
                                    $year_birth_date = new \Carbon\Carbon($trainer->year_birth);
                                @endphp
                                <div class="player-info__table-title">ДАТА РОЖДЕНИЯ</div>
                                <div class="player-info__table-content">{{$year_birth_date->format('d.m.Y')}}
                                    ({{\App\Trainer::GetAge($trainer->year_birth)}} лет)
                                </div>
                            </div>
                            <div class="player-info__table-cell">
                                <div class="player-info__table-title">ГРАЖДАНСТВО</div>
                                <div class="player-info__table-content">{{$trainer->сitizenship}}</div>
                            </div>
                        </div>
                        <div class="player-info__table-row">
                            <div class="player-info__table-cell">
                                <div class="player-info__table-title">ЗВАНИЕ И СПОРТИВНОЕ ЗВАНИЕ</div>
                                <div class="player-info__table-content">{{$trainer->sporting_title}}</div>
                            </div>
                            <div class="player-info__table-cell">
                                <div class="player-info__table-title">РОСТ</div>
                                <div class="player-info__table-content">{{$trainer->height}} см</div>
                            </div>
                        </div>
                        <div class="player-info__table-row">
                            <div class="player-info__table-cell">
                                <div class="player-info__table-title"> РОДНОЙ ГОРОД</div>
                                <div class="player-info__table-content">{{$trainer->hometown}}</div>
                            </div>
                        </div>
                    </div>
                </div>

                @if($trainer->awards != '')
                    <div class="title-default title-default--white">
                        <span>Достижения тренера</span>
                    </div>
                    <div class="competition">
                        @foreach( json_decode($trainer->awards) as $award)
                            @if($award->type == 'kubok')
                                <div class="competition__item competition__item--kubok">
                                    <svg class="icon icon-23 competition__icon kubok">
                                        <use xlink:href="#23"></use>
                                    </svg>
                                    <div class="competition__date">{{$award->year}}</div>
                                    <div class="competition__title">{{$award->name}}</div>
                                </div>
                            @endif
                            @if($award->type == 'medale')
                                <div class="competition__item competition__item--medal">
                                    <svg class="icon icon-24 competition__icon medal">
                                        <use xlink:href="#24"></use>
                                    </svg>
                                    <div class="competition__date">{{$award->year}}</div>
                                    <div class="competition__title">{{$award->name}}</div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @endif

                @if($trainer->achievements != '')
                    <div class="progress progress--achievements progress--slider">
                        <div class="progress__controls">
                            <button data-slider="progress_achievements"
                                    class="progress__control progress__control-prev js-prev-banner">
                                <svg class="icon icon-17 ">
                                    <use xlink:href="#17"></use>
                                </svg>
                            </button>
                            <button data-slider="progress_achievements"
                                    class="progress__control progress__control-next js-next-banner">
                                <svg class="icon icon-18 ">
                                    <use xlink:href="#18"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="wrap-progress__slider">
                            <div data-slider="progress_achievements"
                                 class="progress__slider js-progress__slider--achievements">

                                @foreach( json_decode($trainer->achievements) as $achievement)
                                    <div class="progress__item">
                                        <div class="progress__circle"></div>
                                        <div class="progress__date">{{$achievement->year}}</div>
                                        <div class="progress__title">{{$achievement->name}}</div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>

        @if($trainer->parameters != '')
            <div class="bg-gray">
                <div class="container">
                    <div class="list-information list-information--player-info">
                        @foreach(json_decode($trainer->parameters) as $param)
                            <div class="list-information__item">
                                <div class="list-information__title">{{$param->name}}</div>
                                <div class="list-information__content">{{$param->value}}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <div class="container">

            @if($trainer->career != '')
                <div class="title-default title-default--white"><span>Карьера </span></div>
                <div class="progress progress--career">
                    <div class="progress__controls">
                        <button data-slider="progress_career"
                                class="progress__control progress__control-prev js-prev-banner">
                            <svg class="icon icon-17 ">
                                <use xlink:href="#17"></use>
                            </svg>
                        </button>
                        <button data-slider="progress_career"
                                class="progress__control progress__control-next js-next-banner">
                            <svg class="icon icon-18 ">
                                <use xlink:href="#18"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="wrap-progress__slider">
                        <div data-slider="progress_career" class="progress__slider js-progress__slider--career">
                            @foreach( json_decode($trainer->career) as $career)
                                <div class="progress__item">
                                    <div class="progress__circle"></div>
                                    <div class="progress__date">{{$career->year}}</div>
                                    <div class="progress__title">{{$career->location}}</div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif

            <div class="b-text-border--blue">
                {!! $trainer->description !!}
            </div>

            @include('element.subscribe-form--full')

        </div>
    </div>
@endsection