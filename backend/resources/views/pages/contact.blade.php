@extends('layout--clear-footer')


@section('content')
    <div class="page--contact">
        <div class="container">
            <div class="b-breadcrumbs"><a href="/" class="breadcrumbs__item--link">Главная</a><span class="breadcrumbs__item">Контакты</span></div>
            <div class="title-default title-default--white"><span>Контакты</span></div>
            <div class="wrap-info-item--flex">
                <div class="info-item">
                    <div class="info-item__title">Офис волейбольного клуба "Северянка"</div>
                    <div class="info-item__content">
                        <p>162600, г. Череповец, Спорткомплекс "Юбилейный",  ул. Ленина 125, каб. 2-20</p>
                        <p>Телефон/факс:</p><a href="#" class="phone-link"> 8 (8202) 57-58-59</a>
                        <p>Телефон для справок по приобретению билетов:</p><a href="#" class="phone-link"> 8 (8202) 58-66-80</a>
                    </div>
                </div>
                <div class="info-item">
                    <div class="info-item__title">Игровой зал: СК "Юбилейный"</div>
                    <div class="info-item__content">
                        <table>
                            <tr>
                                <th>Адрес зала: </th>
                                <th>162600, г. Череповец, ул. Ленина 125</th>
                            </tr>
                            <tr>
                                <th>Количество мест: </th>
                                <th>1300</th>
                            </tr>
                            <tr>
                                <th>Покрытие: </th>
                                <th>тарафлекс/паркет</th>
                            </tr>
                            <tr>
                                <th>Размеры: </th>
                                <th>45х25 м</th>
                            </tr>
                            <tr>
                                <th>Высота до металлоконструкций:</th>
                                <th>14,5 м </th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="contact-map"><img src="/img/contact-map.png" alt=""></div>
            <div class="wrap-info-item--flex">
                <div class="info-item">
                    <div class="info-item__title">Волейбольный клуб "Северянка" (до 2014 г. - "Северсталь")</div>
                    <div class="info-item__content">
                        <p>САНО (Спортивная Автономная Некоммерческая Организация) "Волейбольный Клуб "Северсталь""</p>
                        <p>Волейбольная команда "Северянка". Цвета игровой формы: синий и бело-зелёный.</p><a href="#"> Информация об уставной деятельности клуба</a>
                    </div>
                </div>
                <div class="info-item">
                    <div class="info-item__content">
                        <div class="coaching-list coaching-list--contact">
                            <div class="coaching-list__item">
                                <div class="coaching-list__name">Пилипенко Захар Владимирович</div>
                                <div class="coaching-list__position">ГЛАВНЫЙ ТРЕНЕР</div><a href="#" class="coaching-list__email">rodionovanu@mail.ru</a>
                            </div>
                            <div class="coaching-list__item">
                                <div class="coaching-list__name">Самбурский Роман Владимирович</div>
                                <div class="coaching-list__position">СТАРШИЙ ТРЕНЕР</div><a href="#" class="coaching-list__email">sambursky@mail.ru</a>
                            </div>
                            <div class="coaching-list__item">
                                <div class="coaching-list__name">Самбурский Роман Владимирович</div>
                                <div class="coaching-list__position">СТАРШИЙ ТРЕНЕР</div><a href="#" class="coaching-list__email">vuv63@mail.ru</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection