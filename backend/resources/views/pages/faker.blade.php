@extends('layout--base')
@php
    use Faker\Factory as Faker;


    $faker = Faker::create('ru_RU');

dd($faker->realText($maxNbChars = 10));

@endphp
@section('content')
    <div class="container">
        <img src="{{$faker->imageUrl(200, 200, 'sports', true, 'Faker')}}" alt="">
    <div class="wrap-content-lazy-load">
        <div class="grid-container">
            {{--@foreach($arNews as $news)--}}
                {{--<div class="news-item">--}}
                    {{--<a href="#" style="background-image: url('{{$news['preview-photo']}}')"--}}
                       {{--class="news-item__photo"></a>--}}
                    {{--<div class="wrap-news-header">--}}
                        {{--<div class="news-item__date">{{ $news['date']->formatLocalized('%d %B,  %Y')}}</div>--}}
                        {{--<div class="news-item__location">{{$news['city']}}</div>--}}
                    {{--</div>--}}
                    {{--<a href="#" class="news-item__title">{{$news['title']}}</a>--}}
                    {{--<div class="news-item__description">--}}
                        {{--{{$news['preview-text']}}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--@endforeach--}}
        </div>
        <button class="load-more"><span>Загрузить еще</span>
            <svg class="icon icon-14 icon-load-more">
                <use xlink:href="#14"></use>
            </svg>
        </button>
    </div>
    </div>
@endsection
