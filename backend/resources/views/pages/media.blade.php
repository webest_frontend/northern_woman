@extends('layout--clear-footer')


@section('content')
    <div class="page--files">
        <div class="container">
            <div class="b-breadcrumbs">
                <a href="/" class="breadcrumbs__item--link">Главная</a>
                <span class="breadcrumbs__item">Для скачивания</span>
            </div>
            <div class="title-default title-default--white"><span>Для скачивания</span></div>
            <div class="wrap-load-files">
                @foreach($files as $file)
                    <div class="load-files__item">
                        <a href="{{$file->file}}" download style="background-image: url('{{$file->photo}}')"
                           class="load-files__photo"></a>
                        <a href="{{$file->file}}" download class="load-files__link">
                            <svg class="icon icon-26 load-files__icon">
                                <use xlink:href="#26"></use>
                            </svg>
                            <span>{{$file->name}}</span>
                        </a>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection