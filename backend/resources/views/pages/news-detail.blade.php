@extends('layout--base')


@section('content')

    <div class="page-news-current">
        <div class="container">
            <div class="b-breadcrumbs">
                <a href="/" class="breadcrumbs__item--link">Главная</a>
                <a href="{{route('news')}}" class="breadcrumbs__item--link">Новости</a>
                <span class="breadcrumbs__item">{{$news->title}}</span>
            </div>
        </div>
        <div class="f-container f-container--current-news">
            <div class="current-news">
                <div class="current-news__header">
                    <div class="current-news__date">
                        {{$news->date}}
                    </div>
                    <div class="current-news__location">
                        {{$news->city}}
                    </div>
                </div>
                <div class="current-news__title">
                    {{$news->title}}
                </div>
                <div class="current-news__content">
                    <p>{!!$news->content !!}</p>
                </div>
                <div class="current-news__footer">
                    <div class="current-news__social">
                        <div class="current-news__social-title">ПОДЕЛИТЬСЯ</div>
                        <div class="share__item share__item--vk">

                            <bitton class="share__btn">
                                <svg class="icon icon-2 share__icon">
                                    <use xlink:href="#2"></use>
                                </svg>
                                <div class="share__title">Вконтакте</div>
                            </bitton>

                            <div class="share__counter">{{$news->share__vk}}</div>
                        </div>
                        <div class="share__item share__item--facebook">

                            <bitton class="share__btn">
                                <svg class="icon icon-3 share__icon">
                                    <use xlink:href="#3"></use>
                                </svg>
                                <div class="share__title">Facebook</div>
                            </bitton>

                            <div class="share__counter">{{$news->share__facebook}}</div>
                        </div>
                        <div class="share__item share__item--twetter">

                            <bitton class="share__btn">
                                <svg class="icon icon-4 share__icon">
                                    <use xlink:href="#4"></use>
                                </svg>
                                <div class="share__title">Twetter</div>
                            </bitton>

                            <div class="share__counter">{{$news->share__twetter}}</div>
                        </div>
                    </div>

                    <div class="b-subscribe hide-media--lg">
                        <div class="b-subscribe__title">Будьте в курсе последних новостей:</div>
                        <div class="b-subscribe__form">
                            <input type="email" name="email" placeholder="E-mail" class="input">
                            <button class="b-subscribe__send btn--blue">Подписаться</button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="sidebar-news">
                <div class="sidebar-news__title">ДРУГИЕ НОВОСТИ</div>

                @foreach($sidebarNews as $news)
                    <div class="news-item news-item--sidebar">
                        <a href="{{route('news-element', ['url' => $news->url])}}"
                           style="background-image: url('{{$news->preview_photo}}')"
                           title="{{$news->title}}"
                           class="news-item__photo"></a>
                        <div class="wrap-news-header">
                            <div class="news-item__date">{{$news->date}}</div>
                            <div class="news-item__location">{{$news->city}}</div>
                        </div>
                        <a href="{{route('news-element', ['url' => $news->url])}}" title="{{$news->title}}" class="news-item__title">{{$news->title}}</a>
                    </div>
                @endforeach

            </div>
            <div style="display: none" class="b-subscribe show-media--lg-flex">

                <div class="b-subscribe__title">Будьте в курсе последних новостей:</div>
                <div class="b-subscribe__form">
                    <input type="email" name="email" placeholder="E-mail" class="input">
                    <button class="b-subscribe__send btn--blue">Подписаться</button>
                </div>

            </div>
        </div>
    </div>

@endsection