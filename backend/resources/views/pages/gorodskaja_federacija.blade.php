@extends('layout--clear-footer')

@section('content')
    <div class="page--gorodskaja-federacija page--volejbolnyj-centr">
        <div class="container">
            <div class="b-breadcrumbs"><a href="/" class="breadcrumbs__item--link">Главная</a><span class="breadcrumbs__item">Городская федерация</span></div>
            <div class="title-default title-default--white"><span>Городская федерация</span></div>
            <div class="wrap-info-item--flex">
                <div class="info-item">
                    <div class="info-item__title">Череповецкая городская федерация волейбола</div>
                    <div class="info-item__content">
                        <p>162600, г. Череповец, ул. Ленина 125а, каб. 2-20</p>
                        <p>Телефон/факс:</p><a href="#" class="phone-link info-item--mb"> (8202) 57-58-59</a>
                        <p>Официальный публикатор материалов: </p>
                        <p>раздел Федерация волейбола Череповца сайта <a href="#">www.cherlvolley.ru</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection