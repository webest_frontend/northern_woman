@extends('layout--clear-footer')


@section('content')
    <div class="page--search">
        <div class="container">
            <div class="b-breadcrumbs"><a href="/" class="breadcrumbs__item--link">Главная</a><span class="breadcrumbs__item">Результаты поска</span></div>
            <form class="search-form--page">
                <input type="text" placeholder="Поисковая строка" name="query" class="search-form__input">
                <button class="search-form__btn">
                    <svg class="icon icon-10 ">
                        <use xlink:href="#10"></use>
                    </svg>
                </button>
            </form>
            <div class="title-default title-default--white"><span>Результаты поска</span></div>
            <div class="wrap-search-result">
                <div class="search-result__item"><a href="#" class="search-result__title"> <span class="overlap">"Северянка" </span>начинает игры на турнире в Белоруссии</a>
                    <div class="search-result__content">Сегодня в 17:00 МСК на "Чижовка-Арене" города Минска открывается международный турнир, посвящённый 950-летию <span class="overlap">столицы </span>республики. В стартовом матче соревнования "Северянка" встречается со сборной Белоруссии.</div>
                </div>
                <div class="search-result__item"><a href="#" class="search-result__title"> <span class="overlap">"Северянка" </span>начинает игры на турнире в Белоруссии</a>
                    <div class="search-result__content">Сегодня в 17:00 МСК на "Чижовка-Арене" города Минска открывается международный турнир, посвящённый 950-летию <span class="overlap">столицы </span>республики. В стартовом матче соревнования "Северянка" встречается со сборной Белоруссии.</div>
                </div>
                <div class="search-result__item"><a href="#" class="search-result__title"> <span class="overlap">"Северянка" </span>начинает игры на турнире в Белоруссии</a>
                    <div class="search-result__content">Сегодня в 17:00 МСК на "Чижовка-Арене" города Минска открывается международный турнир, посвящённый 950-летию <span class="overlap">столицы </span>республики. В стартовом матче соревнования "Северянка" встречается со сборной Белоруссии.</div>
                </div>
                <div class="search-result__item"><a href="#" class="search-result__title"> <span class="overlap">"Северянка" </span>начинает игры на турнире в Белоруссии</a>
                    <div class="search-result__content">Сегодня в 17:00 МСК на "Чижовка-Арене" города Минска открывается международный турнир, посвящённый 950-летию <span class="overlap">столицы </span>республики. В стартовом матче соревнования "Северянка" встречается со сборной Белоруссии.</div>
                </div>
            </div>
        </div>
    </div>
@endsection