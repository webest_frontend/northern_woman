@extends('layout--base')


@section('content')
    <div class="container container--team-page">
        <div class="b-breadcrumbs">
            <a href="/" class="breadcrumbs__item--link">Главная </a>
            <a href="/" class="breadcrumbs__item--link">Матчи </a>
            <span class="breadcrumbs__item">Северянка </span>
        </div>

        <div class="title-default title-default--white title-default--mit-link">
            <span>"Cеверянка". Сезон 2016/17 </span>
            <div class="wrap-title-links">
                <a href="#" class="title-links__item title-links__item--blue">
                    <svg class="icon icon-15 ">
                        <use xlink:href="#15"></use>
                    </svg>
                </a>
                <div class="title-links__item title-links__item--blue">Сезон 2016/2017</div>
                <a href="#" class="title-links__item title-links__item--blue">
                    <svg class="icon icon-16 ">
                        <use xlink:href="#16"></use>
                    </svg>
                </a>
            </div>
        </div>

        <div class="do-scroll show-media--xl-flex">
            <svg class="icon icon-28 do-scroll__icon">
                <use xlink:href="#28"></use>
            </svg>
            <div class="do-scroll__title">ПРОКРУТИТЕ<br>ФОТОГРАФИЮ</div>
        </div>

        <div class="wrap-photo-map-media">
            <div class="photo-map">
                <img src="/{{$team->team_photo}}" alt="" class="photo-map__img">

                @foreach(json_decode($team->info_marks) as $mark)

                    <div style="left:{{$mark->left}}px; top:{{$mark->top}}px" class="photo-map__mark">
                        <button class="photo-map__mark-action js-toggle-mark">
                            <svg class="icon icon-11 photo-map__mark-icon">
                                <use xlink:href="#11"></use>
                            </svg>
                        </button>

                        @if($mark->type == 'player')
                            @php
                                $player = $players->where('id',$mark->id)->first();
                            @endphp
                            <div class="photo-map__mark-content">
                                <div class="photo-map__mark-title">{{$player->name}} #{{$player->number_team}}</div>
                                <div class="photo-map__mark-text">{{$player->amplua}}</div>

                                <a href="{{route('player', ['url_player' => $player->url])}}"
                                   class="link-mit-arrow photo-map__mark-link">
                                    <span>Подробнее</span>
                                    <span class="link-mit-arrow__icon">
                                <svg class="icon icon-16 ">
                                  <use xlink:href="#16"></use>
                                </svg>
                            </span>
                                </a>
                            </div>
                        @endif

                        @if($mark->type == 'trainer')
                            @php
                                $trainer = $trainers->where('id',$mark->id)->first();
                            @endphp
                            <div class="photo-map__mark-content">
                                <div class="photo-map__mark-title">{{$trainer->name}}</div>
                                <div class="photo-map__mark-text">{{$trainer->role_in_team}}</div>

                                <a href="{{route('trainer', ['url_trainer' => $trainer->url])}}"
                                   class="link-mit-arrow photo-map__mark-link">
                                    <span>Подробнее</span>
                                    <span class="link-mit-arrow__icon">
                                <svg class="icon icon-16 ">
                                  <use xlink:href="#16"></use>
                                </svg>
                            </span>
                                </a>
                            </div>
                        @endif

                    </div>

                @endforeach
            </div>
        </div>
        <div class="title-default title-default--white">
            <span>Состав команды "Северянка" сезона 2016-2017. Высшая лига "А"</span>
        </div>
        <div class="do-scroll show-media--lg-flex">
            <svg class="icon icon-28 do-scroll__icon">
                <use xlink:href="#28"></use>
            </svg>
            <div class="do-scroll__title">ПРОКРУТИТЕ<br>ТАБЛИЦУ</div>
        </div>
        <div class="wrap-media-table">
            <div class="b-table table-white">
                <div class="table-container">
                    <table class="table--center">
                        <thead>
                        <tr>
                            <th class="t-a-l">ИГРОК</th>
                            <th>N ИГРОКА</th>
                            <th>ГОД РОЖДЕНИЯ</th>
                            <th>РОСТ, СМ</th>
                            <th>АМПЛУА</th>
                            <th>ВЫСОТА СЪЁМА В АТАКЕ, СМ</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $sumAge = 0;
                            $sumHeight = 0;
                        @endphp
                        @foreach($players as $player)
                            <tr>
                                <th class="t-a-l color">{{mb_strtoupper($player->name)}}</th>
                                <th>{{$player->number_team}}</th>
                                <th>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $player->year_birth)->format('Y')}}</th>
                                <th>{{$player->height}}</th>
                                <th>{{mb_strtoupper($player->role_in_team)}}</th>
                                <th>---</th>
                            </tr>
                            @php
                                $sumAge = $sumAge + \App\Player::GetAge($player->year_birth);
                                $sumHeight = $sumHeight + $player->height
                            @endphp
                        @endforeach
                        @php
                            //dd($sumAge)
                        @endphp
                        </tbody>
                    </table>
                    <div class="table__footer-text table__last-content">*СРЕДНИЙ ВОЗРАСТ КОМАНДЫ
                        <span class="blue">{{$sumAge/count($players)}} ГОДА. </span>
                        СРЕДНИЙ РОСТ БЕЗ УЧЁТА ЛИБЕРО
                        <span class="blue">{{$sumHeight/count($players)}} СМ.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="title-default title-default--white title-default--mit-link"><span>Изменения в состава сезона 2015/2016</span>
            <div class="b-tab__controls hide-media--lg">
                @if($team->team_left !='')
                    <button data-id-tab="team-exit" data-targer-tab=".js-tab-changes-team"
                            class="tab-controls__item active js-select-tab">Команду покинули
                    </button>
                @endif

                @if($team->team_joined !='')
                    <button data-id-tab="team-joined" data-targer-tab=".js-tab-changes-team"
                            class="tab-controls__item js-select-tab">Команду пополнили
                    </button>
                @endif
            </div>
        </div>

        <div class="do-scroll show-media--lg-flex">
            <svg class="icon icon-28 do-scroll__icon">
                <use xlink:href="#28"></use>
            </svg>
            <div class="do-scroll__title">ПРОКРУТИТЕ<br>ТАБЛИЦУ</div>
        </div>

        <div class="wrap-media-table">
            <div class="b-tab__controls tab__controls--media show-media--lg-flex">
                @if($team->team_left !='')
                    <button data-id-tab="team-exit" data-targer-tab=".js-tab-changes-team"
                            class="tab-controls__item active js-select-tab">Команду покинули
                    </button>
                @endif

                @if($team->team_joined !='')
                    <button data-id-tab="team-joined" data-targer-tab=".js-tab-changes-team"
                            class="tab-controls__item js-select-tab">Команду пополнили
                    </button>
                @endif
            </div>
            <div class="b-tab__container">
                @if($team->team_left !='')
                    <div id="team-exit" class="b-tab__item js-tab-changes-team">
                        <div class="b-table table-white">
                            <div class="table-container">
                                <table class="table--center">
                                    <thead>
                                    <tr>
                                        <th class="t-a-l">ИГРОК</th>
                                        <th>АМПЛУА</th>
                                        <th class="t-a-l">КАРЬЕРА</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(json_decode($team->team_left) as $element)
                                        <tr>
                                            <th class="t-a-l color">{{mb_strtoupper($element->name)}}</th>
                                            <th class="w-1-3">{{mb_strtoupper($element->amplua)}}</th>
                                            <th class="color t-a-l">{{mb_strtoupper($element->career)}}</th>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
                @if($team->team_joined !='')
                    <div id="team-joined" class="b-tab__item js-tab-changes-team">
                        <div class="b-table table-white">
                            <div class="table-container">
                                <table class="table--center">
                                    <thead>
                                    <tr>
                                        <th class="t-a-l">ИГРОК</th>
                                        <th>АМПЛУА</th>
                                        <th class="t-a-l">КАРЬЕРА</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(json_decode($team->team_joined) as $element)
                                        <tr>
                                            <th class="t-a-l color">{{mb_strtoupper($element->name)}}</th>
                                            <th class="w-1-3">{{mb_strtoupper($element->amplua)}}</th>
                                            <th class="color t-a-l">{{mb_strtoupper($element->career)}}</th>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        @if(json_decode($team->coaching_staff) != '')
            <div class="title-default title-default--white"><span>Тренерский штаб команды</span></div>
            <div class="coaching-list">
                @foreach(json_decode($team->coaching_staff) as $coaching)
                    <div class="coaching-list__item">
                        <div class="coaching-list__name">{{$coaching->name}}</div>
                        <div class="coaching-list__position">{{$coaching->value}}</div>
                    </div>
                @endforeach
            </div>
        @endif
        @include('element.subscribe-form--full')

    </div>
@endsection