@extends('layout--clear-footer')


@section('content')
    <div class="page--volejbolnyj-centr">
        <div class="container">
            <div class="b-breadcrumbs"><a href="/" class="breadcrumbs__item--link">Главная</a><span class="breadcrumbs__item">Волейбольный центр СДЮСШОР</span></div>
            <div class="title-default title-default--white"><span>Волейбольный центр СДЮСШОР</span></div>
            <div class="wrap-info-item--flex">
                <div class="info-item">
                    <div class="info-item__title">О школе</div>
                    <div class="info-item__content">
                        <p class="info-item--mb">Муниципальное бюджетное образовательное учреждение дополнительного образования детей "Специализированная детско-юношеская спортивная школа олимпийского резерва по волейболу" (МБОУ ДОД "СДЮСШОР по волейболу").</p>
                        <p class="info-item--blue-color">Контингент занимающихся - девочки.</p>
                        <p class="info-item--blue-color">Занятия проводятся бесплатно.</p>
                    </div>
                </div>
                <div class="info-item">
                    <div class="info-item__title">Контаты</div>
                    <div class="info-item__content">
                        <p>Россия, 162614, Вологодская область, г. Череповец, "Волейбольный центр", ул.Маяковского д. 11, корпус 1.</p>
                        <p>Телефон:</p><a href="#" class="phone-link"> 8 (8202) 55-60-94</a><a href="#" class="phone-link"> 8 (8202) 50-28-21</a>
                        <p>Факс:</p><a href="#" class="phone-link info-item--mb"> 8 (8202) 55-60-94</a>
                        <p>E-mail: <a href="#">cher.volleyball@yandex.ru</a></p>
                        <p>Cайт: <a href="#">https://sites.google.com/site/chervolleyball/school</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection