@extends('layout--base')


@section('content')
    <script>
        var __NewsList__ = {!! $listNews->toJson() !!};
        var __Filter__ = {!!  json_encode($filter) !!};
        var __SETTING__ = {
            'custom_template_first' : false,
            'count_ajax_element' : 12
        }
    </script>
    <div class="page-arhiv" id="vue-news">
        <div class="container">

            <div class="b-breadcrumbs">
                <a href="/" class="breadcrumbs__item--link">Главная</a>
                <span class="breadcrumbs__item">Архив</span>
            </div>

            <div class="title-default title-default--white title-default--mit-link"><span>Архив</span>
                <div class="wrap-title-links">
                    <div class="arhive-filter">

                    <filter-select
                        name="Год"
                        v-bind:select-value= "filter.year.setValue ? filter.year.setValue : 'выбрать'"
                        v-bind:filter-data="filter.year.list"
                        v-bind:select-url="year"
                        type="year"
                        v-on:setting-filter="setFilterGlobal"
                    >
                    </filter-select>

                    <filter-select
                        name="Месяц"
                        v-bind:select-value= "filter.month.setValue ? filter.month.setValue : 'выбрать'"
                        v-bind:filter-data="filter.month.list"
                        v-bind:select-url="month"
                        type="month"
                        v-on:setting-filter="setFilterGlobal"
                    >
                            
                    </filter-select>

                    </div>
                </div>
            </div>
            <div class="holder--page " v-bind:class="page_load ? 'preloader--hide' : ''">
                <div class="preloader">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
            <div class="wrap-loading-page" v-bind:class="page_load ? 'page-load' : ''">
                <div class="grid-container">
                    <div class="news-item" v-for="news in newsList" >
                        <a  v-bind:href="news.url"
                            v-bind:title="news.title"
                            v-bind:style="{'background-image': 'url( '+ news.preview_photo + ')'}"
                            class="news-item__photo"></a>
                        <div class="wrap-news-header">
                            <div class="news-item__date">@{{news.date}}</div>
                            <div class="news-item__location">@{{news.city}}</div>
                        </div>
                        <a v-bind:href="news.url"
                           v-bind:title="news.title"
                           class="news-item__title">@{{news.title}}</a>
                        <div class="news-item__description">
                            @{{news.preview_text}}
                        </div>
                    </div>
                </div>
                <div class="holder" v-if="lazy_loading">
                    <div class="preloader">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
                <button class="load-more" v-on:click="lazyLoad"
                        v-if="PageInfo.current_page != PageInfo.last_page && newsList.length > 0 && !lazy_loading">
                    <span>Загрузить ещё</span>
                    <svg class="icon icon-14 icon-load-more">
                        <use xlink:href="#14"></use>
                    </svg>
                </button>

                <div class="b-pagination" v-if="listPagination.length > 2">

                    <ul class="pagination__list">

                        <li class="pagination__item hide-media--sm" v-if="PageInfo.prev_page_url">
                            <a v-bind:href="PageInfo.prev_page_url"
                               v-on:click.prevent="loadLink(PageInfo.prev_page_url)">
                                <svg class="icon icon-17 ">
                                    <use xlink:href="#17"></use>
                                </svg>
                            </a>
                        </li>

                        <li v-if="link" v-bind:class="index == PageInfo.current_page ? 'pagination__item--active' : ''"
                            class=" pagination__item" v-for="(link, index) in listPagination">
                            <a v-bind:href="link" v-on:click.prevent="loadLink(link)">@{{ index }}</a>
                        </li>

                        <li class="pagination__item hide-media--sm" v-if="PageInfo.next_page_url">
                            <a v-bind:href="PageInfo.next_page_url"
                               v-on:click.prevent="loadLink(PageInfo.next_page_url)">
                                <svg class="icon icon-18 ">
                                    <use xlink:href="#18"></use>
                                </svg>
                            </a>
                        </li>

                    </ul>

                </div>
            </div>
            <div class="no-result" style="text-align: center;font-size: 18pt;font-weight: bold;" v-if="newsList.length == 0">
                Новости отсутсвуют
            </div>
            @include('element.subscribe-form--full')

        </div>
    </div>
@endsection