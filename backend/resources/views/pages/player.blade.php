@extends('layout--base')


@section('content')
    <div class="page-gamer">
        <div class="container">
            <div class="b-breadcrumbs"><a href="/" class="breadcrumbs__item--link">Главная </a>
                <a href="/" class="breadcrumbs__item--link">Команды </a>
                <span class="breadcrumbs__item">Северянка</span>
            </div>
            <div class="player-info">
                <a href="#" class="link-back">
                    <svg class="icon icon-15 ">
                        <use xlink:href="#15"></use>
                    </svg>
                    <span>Вернуться к команде</span>
                </a>
                <div class="player-info__header">
                    <div style="background-image: url('/{{$player->photo}}')" class="player-info__photo"></div>
                    <div class="player-info__content">
                        <div class="player-info__name"><span>{{$player->name}}</span>
                            <div class="number"><sup>№</sup><span>{{$player->number_team}}</span></div>
                        </div>
                        <div class="player-info__nickname">({{$player->amplua}})</div>
                        <div class="player-info__team">{{$player->team->name}}
                            <div style="display:none" class="number"><sup>№</sup><span>{{$player->number_team}}</span>
                            </div>
                        </div>
                        <div class="player-info__status">
                            <div class="player-info__status-type">{{$player->role_in_team}}</div>
                            @if($player->is_captain)
                                <div class="player-info__status-rank">
                                    <svg class="icon icon-5 ">
                                        <use xlink:href="#5"></use>
                                    </svg>
                                    Капитан команды
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="player-info__table">
                        <div class="player-info__table-row">
                            <div class="player-info__table-cell">
                                <div class="player-info__table-title">ДАТА РОЖДЕНИЯ</div>
                                @php
                                    $year_birth_date = new \Carbon\Carbon($player->year_birth);
                                @endphp
                                <div class="player-info__table-content">{{$year_birth_date->format('d.m.Y')}}
                                    ({{\App\Player::GetAge($player->year_birth)}} лет)
                                </div>
                            </div>
                            <div class="player-info__table-cell">
                                <div class="player-info__table-title">РОСТ</div>
                                <div class="player-info__table-content">{{$player->height}} см</div>
                            </div>
                        </div>
                        <div class="player-info__table-row">
                            <div class="player-info__table-cell">
                                <div class="player-info__table-title">СПОРТИВНОЕ ЗВАНИЕ</div>
                                <div class="player-info__table-content">{{$player->sporting_title}}</div>
                            </div>
                            <div class="player-info__table-cell">
                                <div class="player-info__table-title">МАССА</div>
                                <div class="player-info__table-content">{{$player->mass}} кг</div>
                            </div>
                        </div>
                        <div class="player-info__table-row">
                            <div class="player-info__table-cell">
                                <div class="player-info__table-title"> РОДНОЙ ГОРОД</div>
                                <div class="player-info__table-content">{{$player->hometown}}</div>
                            </div>
                            <div class="player-info__table-cell">
                                <div class="player-info__table-title">ВЫСОТА СЪЕМА В АТАКЕ</div>
                                <div class="player-info__table-content">--- см</div>
                            </div>
                        </div>
                        <div class="player-info__table-row">
                            <div class="player-info__table-cell">
                                <div class="player-info__table-title"> ГРАЖДАНСТВО</div>
                                <div class="player-info__table-content">{{$player->сitizenship}}</div>
                            </div>
                            <div class="player-info__table-cell">
                                <div class="player-info__table-title">ВЫСОТА БЛОКИРОВАНИЯ</div>
                                <div class="player-info__table-content">--- см</div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($player->awards != '')
                    <div class="title-default title-default--white"><span>Достижения игрока</span></div>
                    <div class="competition">
                        @foreach( json_decode($player->awards) as $award)
                            @if($award->type == 'kubok')
                                <div class="competition__item competition__item--kubok">
                                    <svg class="icon icon-23 competition__icon kubok">
                                        <use xlink:href="#23"></use>
                                    </svg>
                                    <div class="competition__date">{{$award->year}}</div>
                                    <div class="competition__title">{{$award->name}}</div>
                                </div>
                            @endif
                            @if($award->type == 'medale')
                                <div class="competition__item competition__item--medal">
                                    <svg class="icon icon-24 competition__icon medal">
                                        <use xlink:href="#24"></use>
                                    </svg>
                                    <div class="competition__date">{{$award->year}}</div>
                                    <div class="competition__title">{{$award->name}}</div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @endif
                @if($player->achievements != '')
                    <div class="progress progress--achievements progress--slider">
                        <div class="progress__controls">
                            <button data-slider="progress_achievements"
                                    class="progress__control progress__control-prev js-prev-banner">
                                <svg class="icon icon-17 ">
                                    <use xlink:href="#17"></use>
                                </svg>
                            </button>
                            <button data-slider="progress_achievements"
                                    class="progress__control progress__control-next js-next-banner">
                                <svg class="icon icon-18 ">
                                    <use xlink:href="#18"></use>
                                </svg>
                            </button>
                        </div>
                        <div class="wrap-progress__slider">
                            <div data-slider="progress_achievements"
                                 class="progress__slider js-progress__slider--achievements">
                                @foreach( json_decode($player->achievements) as $achievement)
                                    <div class="progress__item">
                                        <div class="progress__circle"></div>
                                        <div class="progress__date">{{$achievement->year}}</div>
                                        <div class="progress__title">{{$achievement->name}}</div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        @php
            $arrParams = unserialize($player->parameters) ? unserialize($player->parameters): [];
        @endphp

        @if(count($arrParams) > 0)
            <div class="bg-gray">
                <div class="container">
                    <div class="list-information list-information--player-info">
                        @foreach($arrParams as $param)
                            <div class="list-information__item">
                                <div class="list-information__title">{{$param['name']}}</div>
                                <div class="list-information__content">{{$param['value']}}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        <div class="container">
            @if($player->career != '')
                <div class="title-default title-default--white"><span>Карьера игрока</span></div>
                <div class="progress progress--career">
                    <div class="progress__controls">
                        <button data-slider="progress_career"
                                class="progress__control progress__control-prev js-prev-banner">
                            <svg class="icon icon-17 ">
                                <use xlink:href="#17"></use>
                            </svg>
                        </button>
                        <button data-slider="progress_career"
                                class="progress__control progress__control-next js-next-banner">
                            <svg class="icon icon-18 ">
                                <use xlink:href="#18"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="wrap-progress__slider">
                        <div data-slider="progress_career" class="progress__slider js-progress__slider--career">
                            @foreach( json_decode($player->career) as $career)
                                <div class="progress__item">
                                    <div class="progress__circle"></div>
                                    <div class="progress__date">{{$career->year}}</div>
                                    <div class="progress__title">{{$career->location}}</div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            <div class="b-text-border--blue">
                {!! $player->description !!}
            </div>

            @include('element.subscribe-form--full')

        </div>
    </div>
@endsection