@extends('layout--base')


@section('content')
    <div class="page--histiry">
        <div class="container">
            <div class="b-breadcrumbs"><a href="/" class="breadcrumbs__item--link">Главная</a><span class="breadcrumbs__item">История клуба</span></div>
            <div class="title-default title-default--white"><span>История клуба</span></div>
            <div class="wrap-trener-items">
                <div class="trener-item">
                    <div style="background-image: url('/img/trener.png')" class="trener-item__photo"></div>
                    <div class="trener-item__info">
                        <div class="trener-item__name">ШЕВЦОВ ГЕОРГИЙ ЕГОРОВИЧ</div>
                        <div class="trener-item__status">УЧРЕДИТЕЛЬ</div>
                        <div class="trener-item__description">Президент клуба, депутат Законодательного Собрания Вологодской области</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="time-history">
            <div class="time-history__content">
                <div class="wrap-time-history-content js-wrap-time-history-content">
                    <div class="wrap-time-history js-hide-media-history hide-media-history">

                        @foreach($years as $year)
                            <div class="time-history__col">
                                <div class="time-history__year">{{$year}}</div>

                                @foreach($history as $item)
                                    @if($item->date == $year)
                                        <div class="time-history__event">{{$item->event}}</div>
                                    @endif
                                @endforeach

                            </div>
                        @endforeach

                    </div>
                </div>
                <button class="load-more js-toggle-media-history"><span>Показать еще</span>
                    <svg class="icon icon-14 icon-load-more">
                        <use xlink:href="#14"></use>
                    </svg>
                </button>
            </div>
            <div class="time-history__control">
                <div class="container">
                    <div class="time-history__line js-time-history__line">
                        <button class="time-history__btn-control js-history-control"></button>
                        @foreach($years as $year)
                            <div class="time-history__line-item">{{$year}}</div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="title-default title-default--white"><span>Администрация клуба</span></div>
            <div class="coaching-list coaching-list--border-item">
                <div class="coaching-list__item">
                    <div class="coaching-list__name">Пилипенко Захар Владимирович</div>
                    <div class="coaching-list__position">ГЛАВНЫЙ ТРЕНЕР</div>
                </div>
                <div class="coaching-list__item">
                    <div class="coaching-list__name">Самбурский Роман Владимирович</div>
                    <div class="coaching-list__position">СТАРШИЙ ТРЕНЕР</div>
                </div>
                <div class="coaching-list__item">
                    <div class="coaching-list__name">Пилипенко Захар Владимирович</div>
                    <div class="coaching-list__position">ГЛАВНЫЙ ТРЕНЕР</div>
                </div>
                <div class="coaching-list__item">
                    <div class="coaching-list__name">Самбурский Роман Владимирович</div>
                    <div class="coaching-list__position">СТАРШИЙ ТРЕНЕР</div>
                </div>
                <div class="coaching-list__item">
                    <div class="coaching-list__name">Пилипенко Захар Владимирович</div>
                    <div class="coaching-list__position">ГЛАВНЫЙ ТРЕНЕР</div>
                </div>
                <div class="coaching-list__item">
                    <div class="coaching-list__name">Самбурский Роман Владимирович</div>
                    <div class="coaching-list__position">СТАРШИЙ ТРЕНЕР</div>
                </div>
                <div class="coaching-list__item">
                    <div class="coaching-list__name">Пилипенко Захар Владимирович</div>
                    <div class="coaching-list__position">ГЛАВНЫЙ ТРЕНЕР</div>
                </div>
                <div class="coaching-list__item">
                    <div class="coaching-list__name">Самбурский Роман Владимирович</div>
                    <div class="coaching-list__position">СТАРШИЙ ТРЕНЕР</div>
                </div>
                <div class="coaching-list__item">
                    <div class="coaching-list__name">Самбурский Роман Владимирович</div>
                    <div class="coaching-list__position">СТАРШИЙ ТРЕНЕР</div>
                </div>
            </div>
        </div>
        <div class="bg-gray">
            <div class="container">
                <div class="section--video">
                    <div class="wrap-frame">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/UWD3vgeSV6o" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="b-subscribe b-subscribe--full">
                <div class="b-subscribe__title">Будьте в курсе последних новостей:</div>
                <div class="b-subscribe__form">
                    <input type="email" name="email" placeholder="E-mail" class="input">
                    <button class="b-subscribe__send btn--blue">Подписаться</button>
                </div>
            </div>
        </div>
    </div>
@endsection