@extends('layout--clear-footer')

@section('content')
    <div class="page--links page--volejbolnyj-centr">
        <div class="container">
            <div class="b-breadcrumbs">
                <a href="/" class="breadcrumbs__item--link">Главная</a>
                <span class="breadcrumbs__item">Ссылки</span>
            </div>
            <div class="title-default title-default--white">
                <span>Ссылки</span>
            </div>
            @if(count($links) > 0)
                <div class="wrap-links">

                    @foreach($links as $section)
                        <div class="links-item">
                            <div class="links-item__title">{{$section->name}}</div>
                            <div class="links-item__content">
                                @foreach($section->links as $link)
                                    <a href="{{$link->href}}" title="{{$link->name}}"
                                       target="_blank">{{$link->name}}</a>
                                @endforeach
                            </div>
                        </div>
                    @endforeach

                </div>
            @endif
        </div>
    </div>
@endsection