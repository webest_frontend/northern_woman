@extends('layout--base')


@section('content')
    <div class="page-home">
        <div class="wrap-b-nearest-games">
            <div class="b-nearest-games bg-gray">
                <div class="f-container container--nearest-games">
                    <div class="nearest-games__title bg-gray">БЛИЖАЙШИЕ ИГРЫ</div>
                    <div class="nearest-games__body">
                        <div class="grid-container">
                            <div class="card-match card-match--small grid-4">
                                <div class="card-match__location">Выезд</div>
                                <div class="card-match__type">Регулярный чемпионат</div>
                                <div class="card-match__info">
                                    <div class="card-match__team">
                                        <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                        <div class="card-match__team-name">Приморочка</div>
                                        <div class="card-match__team-location">Приморский край</div>
                                    </div>
                                    <div class="card-match__date">
                                        <div class="card-match__date-day">СЕГОДНЯ</div>
                                        <div class="card-match__date-time">20:00</div>
                                    </div>
                                    <div class="card-match__team">
                                        <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                        <div class="card-match__team-name">Северянка</div>
                                        <div class="card-match__team-location">Череповец</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-match card-match--small grid-4">
                                <div class="card-match__location">Выезд</div>
                                <div class="card-match__type">Регулярный чемпионат</div>
                                <div class="card-match__info">
                                    <div class="card-match__team">
                                        <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                        <div class="card-match__team-name">Приморочка</div>
                                        <div class="card-match__team-location">Приморский край</div>
                                    </div>
                                    <div class="card-match__date">
                                        <div class="card-match__date-day">СЕГОДНЯ</div>
                                        <div class="card-match__date-time">20:00</div>
                                    </div>
                                    <div class="card-match__team">
                                        <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                        <div class="card-match__team-name">Северянка</div>
                                        <div class="card-match__team-location">Череповец</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-match card-match--small grid-4">
                                <div class="card-match__location">Выезд</div>
                                <div class="card-match__type">Регулярный чемпионат</div>
                                <div class="card-match__info">
                                    <div class="card-match__team">
                                        <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                        <div class="card-match__team-name">Приморочка</div>
                                        <div class="card-match__team-location">Приморский край</div>
                                    </div>
                                    <div class="card-match__date">
                                        <div class="card-match__date-day">СЕГОДНЯ</div>
                                        <div class="card-match__date-time">20:00</div>
                                    </div>
                                    <div class="card-match__team">
                                        <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                        <div class="card-match__team-name">Северянка</div>
                                        <div class="card-match__team-location">Череповец</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nearest-games__header">
                        <button class="nearest-games__show js-tooggle-nearest-games circle__arrow">
                            <svg class="icon icon-19 ">
                                <use xlink:href="#19"></use>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        @include('element.news-slider')

        <div class="container container--matches-tabs">
            <div class="b-tabs--matches js-balans-matches">
                <div class="matches-tabs">
                    <div class="matches-tabs__header">
                        <div class="matches-tabs__title"><span>Матчи</span></div>
                        <div class="matches-tabs__controls">
                            <button data-id-tab="matches-tabs-1" data-targer-tab=".js-tab-matches-home" class="matches-tabs__control active js-select-tab">СЕВЕРЯНКА</button>
                            <button data-id-tab="matches-tabs-2" data-targer-tab=".js-tab-matches-home" class="matches-tabs__control js-select-tab">СЕВЕРЯНКА-2</button>
                            <button data-id-tab="matches-tabs-3" data-targer-tab=".js-tab-matches-home" class="matches-tabs__control js-select-tab">СЕВЕРЯНКА-3</button>
                        </div>
                        <div class="matches-tabs__link-calendar"><a href="#" class="link-mit-arrow"><span>Календарь матчей</span><span class="link-mit-arrow__icon">
                    <svg class="icon icon-16 ">
                      <use xlink:href="#16"></use>
                    </svg></span></a></div>
                    </div>
                    <div id="matches-tabs-1" class="wrap-matches-tabs__container js-tab-matches-home">
                        <div class="matches-tabs__container">
                            <div class="matches-tabs__cards">
                                <div class="wrap-card-match">
                                    <div class="card-match card-match--small grid-6 card-match--link">
                                        <div class="card-match__location">Выезд</div>
                                        <div class="card-match__type">Регулярный чемпионат</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">СЕГОДНЯ</div>
                                                <div class="card-match__date-time">20:00</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-match card-match--small grid-6 card-match--link">
                                        <div class="card-match__location">Выезд</div>
                                        <div class="card-match__type">Регулярный чемпионат</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">СЕГОДНЯ</div>
                                                <div class="card-match__date-time">20:00</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-match card-match--small grid-6 card-match--link">
                                        <div class="card-match__location">Выезд</div>
                                        <div class="card-match__type">Регулярный чемпионат</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">СЕГОДНЯ</div>
                                                <div class="card-match__date-time">20:00</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-match card-match--small grid-6 card-match--link">
                                        <div class="card-match__location card-match__location--home"> Дома</div>
                                        <div class="card-match__type">Регулярный чемпионат</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">СЕГОДНЯ</div>
                                                <div class="card-match__date-time">20:00</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="matches-tabs__more"><a href="#" class="link-mit-arrow"><span>Показать больше</span><span class="link-mit-arrow__icon">
                        <svg class="icon icon-16 ">
                          <use xlink:href="#16"></use>
                        </svg></span></a></div>
                                <div class="b-subscribe">
                                    <div class="b-subscribe__title">Будьте в курсе последних новостей:</div>
                                    <div class="b-subscribe__form">
                                        <input type="email" name="email" placeholder="E-mail" class="input">
                                        <button class="b-subscribe__send btn--blue">Подписаться</button>
                                    </div>
                                </div>
                            </div>
                            <div class="matches-tabs__result">
                                <div class="matches-tabs__last-result">
                                    <div class="card-match__location">Последний результат</div>
                                    <div class="card-match card-match--small card-match--result">
                                        <div class="card-match__location">Последний результат</div>
                                        <div class="card-match__type">Чемпионат России 2016-2017. Женщины. Высшая лига "А"</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">4 АВГУСТА</div>
                                                <div class="card-match__date-time">3:0</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="matches-tabs__turnir-table">
                                    <div class="table-gray b-table">
                                        <div class="table-label">Турнирная таблица</div>
                                        <div class="table-title">Чемпионат России 2016-2017. Женщины. Высшая лига "А"</div>
                                        <div class="table-container">
                                            <table class="table-color-first-row--blue">
                                                <thead>
                                                <tr>
                                                    <th class="t-a-l">N</th>
                                                    <th class="t-a-l">КОМАНДА</th>
                                                    <th>И</th>
                                                    <th>В</th>
                                                    <th>П</th>
                                                    <th>О</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th class="t-a-l">1</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">2</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">3</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">4</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">5</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">6</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">7</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">8</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">9</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div><a href="#" class="table-link--footer"><span>Вся таблица</span>
                                            <svg class="icon icon-16 ">
                                                <use xlink:href="#16"></use>
                                            </svg></a>
                                    </div>
                                </div>
                            </div>
                            <div class="b-subscribe b-subscribe--media">
                                <div class="b-subscribe__title">Будьте в курсе последних новостей:</div>
                                <div class="b-subscribe__form">
                                    <input type="email" name="email" placeholder="E-mail" class="input">
                                    <button class="b-subscribe__send btn--blue">Подписаться</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="matches-tabs-2" class="wrap-matches-tabs__container js-tab-matches-home">
                        <div class="matches-tabs__container">
                            <div class="matches-tabs__cards">
                                <div class="wrap-card-match">
                                    <div class="card-match card-match--small grid-6 card-match--link">
                                        <div class="card-match__location">Выезд</div>
                                        <div class="card-match__type">Регулярный чемпионат</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">СЕГОДНЯ</div>
                                                <div class="card-match__date-time">20:00</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-match card-match--small grid-6 card-match--link">
                                        <div class="card-match__location">Выезд</div>
                                        <div class="card-match__type">Регулярный чемпионат</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">СЕГОДНЯ</div>
                                                <div class="card-match__date-time">20:00</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-match card-match--small grid-6 card-match--link">
                                        <div class="card-match__location">Выезд</div>
                                        <div class="card-match__type">Регулярный чемпионат</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">СЕГОДНЯ</div>
                                                <div class="card-match__date-time">20:00</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-match card-match--small grid-6 card-match--link">
                                        <div class="card-match__location card-match__location--home"> Дома</div>
                                        <div class="card-match__type">Регулярный чемпионат</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">СЕГОДНЯ</div>
                                                <div class="card-match__date-time">20:00</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="matches-tabs__more"><a href="#" class="link-mit-arrow"><span>Показать больше</span><span class="link-mit-arrow__icon">
                        <svg class="icon icon-16 ">
                          <use xlink:href="#16"></use>
                        </svg></span></a></div>
                                <div class="b-subscribe">
                                    <div class="b-subscribe__title">Будьте в курсе последних новостей:</div>
                                    <div class="b-subscribe__form">
                                        <input type="email" name="email" placeholder="E-mail" class="input">
                                        <button class="b-subscribe__send btn--blue">Подписаться</button>
                                    </div>
                                </div>
                            </div>
                            <div class="matches-tabs__result">
                                <div class="matches-tabs__last-result">
                                    <div class="card-match__location">Последний результат</div>
                                    <div class="card-match card-match--small card-match--result">
                                        <div class="card-match__location">Последний результат</div>
                                        <div class="card-match__type">Чемпионат России 2016-2017. Женщины. Высшая лига "А"</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">4 АВГУСТА</div>
                                                <div class="card-match__date-time">3:0</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="matches-tabs__turnir-table">
                                    <div class="table-gray b-table">
                                        <div class="table-label">Турнирная таблица</div>
                                        <div class="table-title">Чемпионат России 2016-2017. Женщины. Высшая лига "А"</div>
                                        <div class="table-container">
                                            <table class="table-color-first-row--blue">
                                                <thead>
                                                <tr>
                                                    <th class="t-a-l">N</th>
                                                    <th class="t-a-l">КОМАНДА</th>
                                                    <th>И</th>
                                                    <th>В</th>
                                                    <th>П</th>
                                                    <th>О</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th class="t-a-l">1</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">2</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">3</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">4</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">5</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">6</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">7</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">8</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">9</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div><a href="#" class="table-link--footer"><span>Вся таблица</span>
                                            <svg class="icon icon-16 ">
                                                <use xlink:href="#16"></use>
                                            </svg></a>
                                    </div>
                                </div>
                            </div>
                            <div class="b-subscribe b-subscribe--media">
                                <div class="b-subscribe__title">Будьте в курсе последних новостей:</div>
                                <div class="b-subscribe__form">
                                    <input type="email" name="email" placeholder="E-mail" class="input">
                                    <button class="b-subscribe__send btn--blue">Подписаться</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="matches-tabs-3" class="wrap-matches-tabs__container js-tab-matches-home">
                        <div class="matches-tabs__container">
                            <div class="matches-tabs__cards">
                                <div class="wrap-card-match">
                                    <div class="card-match card-match--small grid-6 card-match--link">
                                        <div class="card-match__location">Выезд</div>
                                        <div class="card-match__type">Регулярный чемпионат</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">СЕГОДНЯ</div>
                                                <div class="card-match__date-time">20:00</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-match card-match--small grid-6 card-match--link">
                                        <div class="card-match__location">Выезд</div>
                                        <div class="card-match__type">Регулярный чемпионат</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">СЕГОДНЯ</div>
                                                <div class="card-match__date-time">20:00</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-match card-match--small grid-6 card-match--link">
                                        <div class="card-match__location">Выезд</div>
                                        <div class="card-match__type">Регулярный чемпионат</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">СЕГОДНЯ</div>
                                                <div class="card-match__date-time">20:00</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-match card-match--small grid-6 card-match--link">
                                        <div class="card-match__location card-match__location--home"> Дома</div>
                                        <div class="card-match__type">Регулярный чемпионат</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">СЕГОДНЯ</div>
                                                <div class="card-match__date-time">20:00</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="matches-tabs__more"><a href="#" class="link-mit-arrow"><span>Показать больше</span><span class="link-mit-arrow__icon">
                        <svg class="icon icon-16 ">
                          <use xlink:href="#16"></use>
                        </svg></span></a></div>
                                <div class="b-subscribe">
                                    <div class="b-subscribe__title">Будьте в курсе последних новостей:</div>
                                    <div class="b-subscribe__form">
                                        <input type="email" name="email" placeholder="E-mail" class="input">
                                        <button class="b-subscribe__send btn--blue">Подписаться</button>
                                    </div>
                                </div>
                            </div>
                            <div class="matches-tabs__result">
                                <div class="matches-tabs__last-result">
                                    <div class="card-match__location">Последний результат</div>
                                    <div class="card-match card-match--small card-match--result">
                                        <div class="card-match__location">Последний результат</div>
                                        <div class="card-match__type">Чемпионат России 2016-2017. Женщины. Высшая лига "А"</div>
                                        <div class="card-match__info">
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/primor.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Приморочка</div>
                                                <div class="card-match__team-location">Приморский край</div>
                                            </div>
                                            <div class="card-match__date">
                                                <div class="card-match__date-day">4 АВГУСТА</div>
                                                <div class="card-match__date-time">3:0</div>
                                            </div>
                                            <div class="card-match__team">
                                                <div style="background-image: url('/img/severjanka.png')" class="card-match__team-logo"></div>
                                                <div class="card-match__team-name">Северянка</div>
                                                <div class="card-match__team-location">Череповец</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="matches-tabs__turnir-table">
                                    <div class="table-gray b-table">
                                        <div class="table-label">Турнирная таблица</div>
                                        <div class="table-title">Чемпионат России 2016-2017. Женщины. Высшая лига "А"</div>
                                        <div class="table-container">
                                            <table class="table-color-first-row--blue">
                                                <thead>
                                                <tr>
                                                    <th class="t-a-l">N</th>
                                                    <th class="t-a-l">КОМАНДА</th>
                                                    <th>И</th>
                                                    <th>В</th>
                                                    <th>П</th>
                                                    <th>О</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th class="t-a-l">1</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">2</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">3</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">4</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">5</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">6</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">7</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">8</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                <tr>
                                                    <th class="t-a-l">9</th>
                                                    <th class="t-a-l color">ЛИПЕЦК-ИНДЕЗИТ</th>
                                                    <th>32</th>
                                                    <th>20</th>
                                                    <th>4</th>
                                                    <th class="color">80</th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div><a href="#" class="table-link--footer"><span>Вся таблица</span>
                                            <svg class="icon icon-16 ">
                                                <use xlink:href="#16"></use>
                                            </svg></a>
                                    </div>
                                </div>
                            </div>
                            <div class="b-subscribe b-subscribe--media">
                                <div class="b-subscribe__title">Будьте в курсе последних новостей:</div>
                                <div class="b-subscribe__form">
                                    <input type="email" name="email" placeholder="E-mail" class="input">
                                    <button class="b-subscribe__send btn--blue">Подписаться</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-gray">
            <div class="f-container f-container--default contents-mit-tiile--home">
                <div class="content-mit-tiile">
                    <div class="title-default title-default--gray"><span>Лучшие бомбардиры</span></div>
                    <div class="media-table">
                        <div class="b-table table-white">
                            <div class="table-container">
                                <table class="table--center">
                                    <thead>
                                    <tr>
                                        <th>МЕСТО</th>
                                        <th class="t-a-l t-a-c--media">ИГРОК</th>
                                        <th class="hide-media--md">N ИГРОКА</th>
                                        <th><span class="hide-media--md">ЗАБИТЫХ МЯЧЕЙ</span><span style="text-align: center; font-weight: bold" class="show-media--md">МЯЧЕЙ</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>1</th>
                                        <th class="t-a-l color">НЕМЦЕВА АЛЕВТИНА</th>
                                        <th class="hide-media--md">32</th>
                                        <th class="color">20</th>
                                    </tr>
                                    <tr>
                                        <th>2</th>
                                        <th class="t-a-l color">НЕМЦЕВА АЛЕВТИНА</th>
                                        <th class="hide-media--md">32</th>
                                        <th class="color">20</th>
                                    </tr>
                                    <tr>
                                        <th>3</th>
                                        <th class="t-a-l color">НЕМЦЕВА АЛЕВТИНА</th>
                                        <th class="hide-media--md">32</th>
                                        <th class="color">20</th>
                                    </tr>
                                    <tr>
                                        <th>4</th>
                                        <th class="t-a-l color">НЕМЦЕВА АЛЕВТИНА</th>
                                        <th class="hide-media--md">32</th>
                                        <th class="color">20</th>
                                    </tr>
                                    <tr>
                                        <th>5</th>
                                        <th class="t-a-l color">НЕМЦЕВА АЛЕВТИНА</th>
                                        <th class="hide-media--md">32</th>
                                        <th class="color">20</th>
                                    </tr>
                                    <tr>
                                        <th>6</th>
                                        <th class="t-a-l color">НЕМЦЕВА АЛЕВТИНА</th>
                                        <th class="hide-media--md">32</th>
                                        <th class="color">20</th>
                                    </tr>
                                    <tr>
                                        <th>7</th>
                                        <th class="t-a-l color">НЕМЦЕВА АЛЕВТИНА</th>
                                        <th class="hide-media--md">32</th>
                                        <th class="color">20</th>
                                    </tr>
                                    <tr>
                                        <th>8</th>
                                        <th class="t-a-l color">НЕМЦЕВА АЛЕВТИНА</th>
                                        <th class="hide-media--md">32</th>
                                        <th class="color">20</th>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-mit-tiile">
                    <div class="title-default title-default--gray"><span>Лучший нападающий</span></div>
                    <div class="player-card player-card--small player-card--link">
                        <div class="player-card__header-bgi"></div>
                        <div class="player-card__content">
                            <div class="player-card__header">
                                <div style="background-image: url(/img/Lysaya.JPG)" class="player-card__photo"></div>
                                <div class="player-card__info">
                                    <div class="player-card__name">КСЕНИЯ КРАВЧЕНКО</div>
                                    <div class="player-card__team">СЕВЕРЯНКА</div>
                                    <div class="player-card__status">Доигровщик</div>
                                </div>
                                <div class="player-card__number"><sup>№</sup><span>30</span></div>
                            </div>
                            <div class="player-card__static">
                                <div class="title-default title-default--small title-default--white"><span>СТАТИСТИКА ИГРОКА</span></div>
                                <div class="player-card__static-wrap">
                                    <div class="player-card__static-value">
                                        <div class="player-card__static-number">18</div>
                                        <div class="player-card__static-title">ЗАБИТЫХ МЯЧЕЙ </div>
                                    </div>
                                    <div class="player-card__static-value">
                                        <div class="player-card__static-number">24</div>
                                        <div class="player-card__static-title">КОЛИЧЕСТВО ИГР</div>
                                    </div>
                                    <div class="player-card__static-value">
                                        <div class="player-card__static-number">10</div>
                                        <div class="player-card__static-title">СЫГРАННЫХ СЕТОВ</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-mit-tiile">
                    <div class="title-default title-default--gray"><span>Лучший блокирующий</span></div>
                    <div class="player-card player-card--small player-card--link">
                        <div class="player-card__header-bgi"></div>
                        <div class="player-card__content">
                            <div class="player-card__header">
                                <div style="background-image: url(/img/Lysaya.JPG)" class="player-card__photo"></div>
                                <div class="player-card__info">
                                    <div class="player-card__name">КСЕНИЯ КРАВЧЕНКО</div>
                                    <div class="player-card__team">СЕВЕРЯНКА</div>
                                    <div class="player-card__status">Доигровщик</div>
                                </div>
                                <div class="player-card__number"><sup>№</sup><span>30</span></div>
                            </div>
                            <div class="player-card__static">
                                <div class="title-default title-default--small title-default--white"><span>СТАТИСТИКА ИГРОКА</span></div>
                                <div class="player-card__static-wrap">
                                    <div class="player-card__static-value">
                                        <div class="player-card__static-number">18</div>
                                        <div class="player-card__static-title">ЗАБИТЫХ МЯЧЕЙ </div>
                                    </div>
                                    <div class="player-card__static-value">
                                        <div class="player-card__static-number">24</div>
                                        <div class="player-card__static-title">КОЛИЧЕСТВО ИГР</div>
                                    </div>
                                    <div class="player-card__static-value">
                                        <div class="player-card__static-number">10</div>
                                        <div class="player-card__static-title">СЫГРАННЫХ СЕТОВ</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-mit-tiile">
                    <div class="title-default title-default--gray"><span>Лучший подающий</span></div>
                    <div class="player-card player-card--small player-card--link">
                        <div class="player-card__header-bgi"></div>
                        <div class="player-card__content">
                            <div class="player-card__header">
                                <div style="background-image: url(/img/Lysaya.JPG)" class="player-card__photo"></div>
                                <div class="player-card__info">
                                    <div class="player-card__name">КСЕНИЯ КРАВЧЕНКО</div>
                                    <div class="player-card__team">СЕВЕРЯНКА</div>
                                    <div class="player-card__status">Доигровщик</div>
                                </div>
                                <div class="player-card__number"><sup>№</sup><span>30</span></div>
                            </div>
                            <div class="player-card__static">
                                <div class="title-default title-default--small title-default--white"><span>СТАТИСТИКА ИГРОКА</span></div>
                                <div class="player-card__static-wrap">
                                    <div class="player-card__static-value">
                                        <div class="player-card__static-number">18</div>
                                        <div class="player-card__static-title">ЗАБИТЫХ МЯЧЕЙ </div>
                                    </div>
                                    <div class="player-card__static-value">
                                        <div class="player-card__static-number">24</div>
                                        <div class="player-card__static-title">КОЛИЧЕСТВО ИГР</div>
                                    </div>
                                    <div class="player-card__static-value">
                                        <div class="player-card__static-number">10</div>
                                        <div class="player-card__static-title">СЫГРАННЫХ СЕТОВ</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('widget.video-gallery')
    </div>
@endsection