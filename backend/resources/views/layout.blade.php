<!DOCTYPE html>
<html lang="ru" class="no-js">
<head>
    <meta charset="UTF-8"/>
    <title>UI элементов сайта</title>
    <meta name="viewport" content="width=device-width"/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="format-detection" content="telephone=no"/>
    <link type="image/x-icon" rel="shortcut icon" href="favicon.ico"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="57x57" href="apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="apple-touch-icon-76x76.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="apple-touch-icon-152x152.png"/>
    <link rel="stylesheet" media="all" href="/css/main_global.css"/>
</head>
<body>
<div id="SVG_container"></div>
<header class="header">
    <div class="bg-d-blue">
        <div class="f-container container--header">
            <div class="header__map-data"><a href="#">
                    <svg class="icon icon-9 ">
                        <use xlink:href="#9"></use>
                    </svg><span class="media-hidden">г. Череповец, спорткомплекс  "Юбилейный", ул. Ленина 125</span><span class="media-show">"Юбилейный", ул. Ленина 125</span></a></div>
            <div class="header__soc-list"><span>Северянка в соцсетях:</span>
                <ul class="list-soc">
                    <li class="list-soc__item"><a href="#">
                            <svg class="icon icon-2 small-icon">
                                <use xlink:href="#2"></use>
                            </svg></a></li>
                    <li class="list-soc__item"><a href="#">
                            <svg class="icon icon-3 big-icon">
                                <use xlink:href="#3"></use>
                            </svg></a></li>
                    <li class="list-soc__item"><a href="#">
                            <svg class="icon icon-4 ">
                                <use xlink:href="#4"></use>
                            </svg></a></li>
                    <li class="list-soc__item"><a href="#">
                            <svg class="icon icon-5 ">
                                <use xlink:href="#5"></use>
                            </svg></a></li>
                    <li class="list-soc__item"><a href="#">
                            <svg class="icon icon-6 ">
                                <use xlink:href="#6"></use>
                            </svg></a></li>
                </ul>
            </div>
            <div class="header__sponsor bg-l-blue"><span>Спонсор волейбольного клуба</span><img src="/img/logo-fosagro.png" alt="Фосагро"/></div>
            <div class="b-select-language">
                <button class="select-language__selected bg-d-blue js-open-language"><span class="js-language-selected">RU</span>
                    <svg class="icon icon-14 togle-arrow">
                        <use xlink:href="#14"></use>
                    </svg>
                </button>
                <div class="language__list">
                    <button class="language__item js-select-language">EN</button>
                    <button class="language__item js-select-language">FR</button>
                    <button class="language__item js-select-language">GE </button>
                    <button class="language__item js-select-language">IT</button>
                    <button class="language__item js-select-language">PL</button>
                </div>
            </div>
        </div>
    </div>

    @include('widget.menu--header')
</header>

@include('widget.menu--header-media')


@yield('layout-content')

<div class="modal"></div>

<script>window.jQuery || document.write('<script src="/js/jquery-3.2.1.min.js"><\/script>')</script>
<script src="/js/all.js"></script>
<script src="/js/vue.js"></script>
<script src="/js/main.js"></script>
</body>
</html>