@extends('auth')
@php
    Auth::logout();
@endphp

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Вы вышли из системы</div>
                </div>
            </div>
        </div>
    </div>
@endsection
