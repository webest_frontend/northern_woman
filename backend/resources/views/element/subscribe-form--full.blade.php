<div class="b-subscribe b-subscribe--full">
    <div class="b-subscribe__title">Будьте в курсе последних новостей:</div>
    <div class="b-subscribe__form">
        <input type="email" name="email" placeholder="E-mail" class="input">
        <button class="b-subscribe__send btn--blue">Подписаться</button>
    </div>
</div>