@php
    $listNews = \App\News::GetSliderNews();
    foreach ($listNews as $news){
        $news['date'] = $news->getDate();
    }

    $firstNews =$listNews ->shift();

@endphp


<div class="wrap-home-slider">
    <div class="home-slider js-home-slider__container">

        <div class="home-slider__item home-slider__item--first active">
            <div style="background-image: url('{{$firstNews->preview_photo}}')"
                 class="home-slider__content">
                <div class="wrap-home-slide-information">
                    <div class="home-slider__content-header">
                        <div class="home-slider__date">{{$firstNews->date}}</div>
                        <div class="home-slider__city">{{$firstNews->city}}</div>
                    </div>
                    <div class="home-slider__title">
                        {{$firstNews->title}}
                    </div>
                    <a href="{{route('news-element', ['url' => $firstNews->url])}}"
                       title="{{$firstNews->title}}"
                       class="home-slider__link btn btn--blue btn--radius">Подробнее</a>
                </div>
            </div>
            <button class="home-slider__control js-home-slider-controll--next">
                <svg class="icon icon-21 ">
                    <use xlink:href="#21"></use>
                </svg>
            </button>
        </div>

        @foreach($listNews as $news)
            <div class="home-slider__item">
                <div style="background-image: url('{{$news->preview_photo}}')" class="home-slider__content">
                    <div class="wrap-home-slide-information">

                        <div class="home-slider__content-header">
                            <div class="home-slider__date">{{$news->date}}</div>
                            <div class="home-slider__city">{{$news->city}}</div>
                        </div>

                        <div class="home-slider__title">
                            {{$news->title}}
                        </div>

                        <a href="{{route('news-element', ['url' => $news->url])}}"
                           title="{{$news->title}}"
                           class="home-slider__link btn btn--blue btn--radius">
                            Подробнее
                        </a>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
    <div class="home-slider__controls js-home-slider__controls">
        <button class="home-slider__control home-slider__control--prev js-home-slider-controll--prev">
            <svg class="icon icon-21 ">
                <use xlink:href="#21"></use>
            </svg>
        </button>
        <button class="home-slider__control home-slider__control--next js-home-slider-controll--next">
            <svg class="icon icon-22 ">
                <use xlink:href="#22"></use>
            </svg>
        </button>
    </div>
</div>

<div class="wrap-home-slider--media">
    <div class=" home-slider--media js-slider--home-media">

        <div class="home-slider__item--media">
            <div style="background-image: url('{{$firstNews->preview_photo}}')" class="home-slider__content">
                <div class="wrap-home-slide-information">
                    <div class="home-slider__content-header">
                        <div class="home-slider__date">{{$firstNews->date}}</div>
                        <div class="home-slider__city">{{$firstNews->city}}</div>
                    </div>
                    <a href="{{route('news-element', ['url' => $firstNews->url])}}" title="{{$firstNews->title}}"
                       class="home-slider__title">{{$firstNews->title}}</a>
                    <a href="{{route('news-element', ['url' => $firstNews->url])}}" title="{{$firstNews->title}}"
                       class="home-slider__link btn btn--blue btn--radius">Подробнее</a>
                </div>
            </div>
        </div>

        @foreach($listNews as $news)

            <div class="home-slider__item--media">
                <div style="background-image: url('{{$news->preview_photo}}')" class="home-slider__content">
                    <div class="wrap-home-slide-information">
                        <div class="home-slider__content-header">
                            <div class="home-slider__date">{{$news->date}}</div>
                            <div class="home-slider__city">{{$news->city}}</div>
                        </div>
                        <a href="{{route('news-element', ['url' => $news->url])}}" title="{{$news->title}}"
                           class="home-slider__title">{{$news->title}}</a>
                        <a href="{{route('news-element', ['url' => $news->url])}}" title="{{$news->title}}"
                           class="home-slider__link btn btn--blue btn--radius">Подробнее</a>
                    </div>
                </div>
            </div>

        @endforeach
    </div>
</div>