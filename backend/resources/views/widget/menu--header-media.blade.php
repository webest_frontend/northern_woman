<div class="media-menu js-media-menu">
    <button class="media-menu__toggle js-toggle--media-menu">
        <span style="background-image: url(/img/menu-btn.png)"></span>МЕНЮ</button>
    <div class="media-menu__content">
        <div class="wrap-menu-liks js-menu-liks">
            <a href="#" class="media-menu__item">МАТЧИ</a>

            <button data-id-menu="menu_club" class="media-menu__item js-show--lvl-menu">
                КЛУБ
                <svg class="icon icon-16 ">
                    <use xlink:href="#16"></use>
                </svg>
            </button>

            <button data-id-menu="menu_teams" class="media-menu__item js-show--lvl-menu">
                КОМАНДЫ
                <svg class="icon icon-16 ">
                    <use xlink:href="#16"></use>
                </svg>
            </button>

            <button data-id-menu="menu_competition" class="media-menu__item js-show--lvl-menu">
                СОРЕВНОВАНИЯ
                <svg class="icon icon-16 ">
                    <use xlink:href="#16"></use>
                </svg>
            </button>
            <a href="#" class="media-menu__item">СТАТИСТИКА</a>

            <a href="{{route('archive')}}" class="media-menu__item">АРХИВ</a>

            <a href="{{route('links')}}" class="media-menu__item">ССЫЛКИ</a>

        </div>

        <div id="menu_club" class="wrap-menu-lvl">
            <div class="media-menu__item">
                <button data-id-menu="menu_club" class="media-menu__lvl-back js-show--lvl-menu">
                    <svg class="icon icon-15 ">
                        <use xlink:href="#15"></use>
                    </svg>КЛУБ
                </button>
            </div>

            <a href="{{route('news')}}" class="media-menu__item">НОВОСТИ КЛУБА</a>
            <a href="{{route('archive')}}" class="media-menu__item">АРХИВ НОВОСТЕЙ</a>
            <a href="{{route('history')}}" class="media-menu__item">ИСТОРИЯ</a>
            <a href="{{route('contact')}}" class="media-menu__item">КОНТАКТЫ</a>
            <a href="{{route('media')}}" class="media-menu__item">ДЛЯ СКАЧИВАНИЯ</a>
            <a href="{{route('volejbolnyj_center')}}" class="media-menu__item">ВОЛЕЙБОЛЬНЫЙ ЦЕНТР</a>
            <a href="{{route('gorodskaja_federacija')}}" class="media-menu__item">ГОРОДСКАЯ ФЕДЕРАЦИЯ</a>

        </div>

        <div id="menu_teams" class="wrap-menu-lvl">

            <div class="media-menu__item">
                <button data-id-menu="menu_teams" class="media-menu__lvl-back js-show--lvl-menu">
                    <svg class="icon icon-15 ">
                        <use xlink:href="#15"></use>
                    </svg>КОМАНДЫ
                </button>
            </div>

            <a href="#" class="media-menu__item">СЕВЕРЯНКА</a>
            <a href="#" class="media-menu__item">СЕВЕРЯНКА-1</a>
            <a href="#" class="media-menu__item">СЕВЕРЯНКА-2</a>
        </div>

        <div id="menu_competition" class="wrap-menu-lvl">
            <div class="media-menu__item">
                <button data-id-menu="menu_competition" class="media-menu__lvl-back js-show--lvl-menu">
                    <svg class="icon icon-15 ">
                        <use xlink:href="#15"></use>
                    </svg>СОРЕВНОВАНИЯ
                </button>

            </div>

            <a href="#" class="media-menu__item">ЧЕМПИОНАТ РОССИИ</a>
            <a href="#" class="media-menu__item">КУБОК РОСИИ</a>

        </div>
    </div>
    <div class="media-menu__search">
        <div class="header-search">
            <form class="b-search-form">
                <input type="text" name="query" placeholder="ПОИСК" class="input"/>
                <button class="send-search">
                    <svg class="icon icon-10 ">
                        <use xlink:href="#10"></use>
                    </svg>
                </button>
            </form>
        </div>
    </div>
</div>