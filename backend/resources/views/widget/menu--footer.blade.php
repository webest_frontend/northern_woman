<div class="footer-nav">
    <div class="footer-nav__col">

        <butoon class="footer-nav__title js-toggle-footer-menu">
            МАТЧИ
            <svg class="icon icon-14 toogle-icon">
                <use xlink:href="#14"></use>
            </svg>
        </butoon>

        <div class="footer-nav__list">
            <a href="#" class="footer-nav__item">МАТЧИ "СЕВЕРЯНКА"</a>
            <a href="#" class="footer-nav__item">МАТЧИ "СЕВЕРЯНКА-2"</a>
            <a href="#" class="footer-nav__item">МАТЧИ "СЕВЕРЯНКА-3"</a>
        </div>

    </div>
    <div class="footer-nav__col">

        <butoon class="footer-nav__title js-toggle-footer-menu">
            КЛУБ
            <svg class="icon icon-14 toogle-icon">
                <use xlink:href="#14"></use>
            </svg>
        </butoon>

        <div class="footer-nav__list">
            <a href="{{route('news')}}" class="footer-nav__item">НОВОСТИ КЛУБА</a>
            <a href="{{route('history')}}" class="footer-nav__item">ИСТОРИЯ</a>
            <a href="{{route('contact')}}" class="footer-nav__item">КОНТАКТЫ</a>
            <a href="{{route('media')}}" class="footer-nav__item">ДЛЯ СКАЧИВАНИЯ</a>
            <a href="{{route('volejbolnyj_center')}}" class="footer-nav__item">ВОЛЕЙБОЛЬНЫЙ ЦЕНТР</a>
            <a href="{{route('gorodskaja_federacija')}}" class="footer-nav__item">ГОРОДСКАЯ ФЕДЕРАЦИЯ</a>
        </div>
    </div>

    <div class="footer-nav__col">

        <butoon class="footer-nav__title js-toggle-footer-menu">
            КОМАНДЫ
            <svg class="icon icon-14 toogle-icon">
                <use xlink:href="#14"></use>
            </svg>
        </butoon>

        <div class="footer-nav__list">
            <a href="#" class="footer-nav__item">"СЕВЕРЯНКА"</a>
            <a href="#" class="footer-nav__item">"СЕВЕРЯНКА-2"</a>
            <a href="#" class="footer-nav__item">"СЕВЕРЯНКА-3"</a>
        </div>
    </div>

    <div class="footer-nav__col">

        <butoon class="footer-nav__title js-toggle-footer-menu">
            СОРЕВНОВАНИЯ
            <svg class="icon icon-14 toogle-icon">
                <use xlink:href="#14"></use>
            </svg>
        </butoon>

        <div class="footer-nav__list">
            <a href="#" class="footer-nav__item">ЧЕМПИОНАТ РОССИИ</a>
            <a href="#" class="footer-nav__item">КУБОК РОССИИ</a>
        </div>
    </div>

    <div class="footer-nav__col footer-nav__col--not-list">
        <a href="#" class="footer-nav__title footer-nav__title--link">СТАТИСТИКА</a>
        <a href="{{route('archive')}}" class="footer-nav__title footer-nav__title--link">АРХИВ</a>
        <a href="#" class="footer-nav__title footer-nav__title--link">О ВОЛЕЙБОЛЕ</a>
        <a href="{{route('links')}}" class="footer-nav__title footer-nav__title--link">ССЫЛКИ</a>
    </div>
</div>