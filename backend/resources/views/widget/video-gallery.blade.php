@php
    $Gallerie = \App\Gallerie::Galleries('youtube')->first();
    $arItems = $Gallerie->items->sortByDesc('created_at')->chunk(5);

@endphp
<div class="container container--youtube-slider">

    <div class="title-default title-default--white title-default--mit-link">
        <span>Последние видео</span>
        <a href="#" class="link-mit-icon hide-media--md">
            <svg class="icon icon-6 icon-youtube">
                <use xlink:href="#6"></use>
            </svg>
            <span>Смотреть на Youtube</span>
        </a>
    </div>

    <div class="b-slider slider--youtube js-slider--youtube">

        @foreach($arItems as $items)
            @php
                $bigItem = $items->shift();
            @endphp
            <div class="b-slider__item b-slider__item--youtube">

                <div class="container-video">
                    <div class="video--big">
                        <iframe width="100%" height="100%" src="{{$bigItem->src}}"
                                frameborder="0" allowfullscreen></iframe>
                    </div>


                    <div class="wrap-small-video">
                        @foreach($items as $item)
                            <div class="video--small">
                                <iframe width="100%" height="100%" src="{{$item->src}}"
                                        frameborder="0" allowfullscreen></iframe>
                            </div>
                        @endforeach
                    </div>

                </div>

            </div>
        @endforeach
    </div>

    <a href="#" class="link-mit-icon show-media--md">
        <svg class="icon icon-6 icon-youtube">
            <use xlink:href="#6"></use>
        </svg>
        <span>Смотреть на Youtube</span>
    </a>

</div>