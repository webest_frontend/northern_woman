<div class="f-container wrap-header-nav"><a href="/" class="logo"><img src="/img/logo.png" alt="Логотип северянка"/></a>
    <nav class="header__nav">
        <ul class="header__nav-list">
            <li class="header__nav-item">
                <a href="#">МАТЧИ</a>
            </li>
            <li class="header__nav-item">
                <a href="#" data-target-nav="club" class="js-toggle-popup-nav">КЛУБ</a>
            </li>
            <li class="header__nav-item">
                <a href="#" data-target-nav="team" class="js-toggle-popup-nav">КОМАНДЫ</a>
            </li>
            <li class="header__nav-item">
                <a href="#" data-target-nav="competition" class="js-toggle-popup-nav">СОРЕВНОВАНИЯ</a>
            </li>
            <li class="header__nav-item">
                <a href="#">СТАТИСТИКА</a>
            </li>
            <li class="header__nav-item">
                <a href="{{route('news')}}">НОВОСТИ</a>
            </li>
            <li class="header__nav-item">
                <a href="{{route('links')}}">ССЫЛКИ</a>
            </li>
        </ul>
    </nav>
    <div class="header-search hidden-media">
        <form class="b-search-form">
            <input type="text" name="query" placeholder="поиск" class="input"/>
            <button class="send-search">
                <svg class="icon icon-10 ">
                    <use xlink:href="#10"></use>
                </svg>
            </button>
        </form>
    </div>
    <button style="background-image: url(/img/menu-btn.png)" class="btn-media-menu js-toggle--media-menu"></button>
</div>
<div data-popup-nav="club" class="popup-nav">
    <div class="f-container f-container--popup-nav f-container--nav-one">
        <button data-target-nav="club" class="close-btn close-btn--popup-nav js-toggle-popup-nav">
            <svg class="icon icon-12 ">
                <use xlink:href="#12"></use>
            </svg>
        </button>
        <div class="popup-nav__col">
            <div class="popup-nav__content">
                <div class="one-row-nav">
                    <div class="logo"><img src="/img/29.svg" alt="Северянка"/></div>
                    <div class="popup-nav__title">КЛУБ</div>
                    <ul class="one-row-nav__list">
                        <li class="one-row-nav__item"><a href="{{route('news')}}">НОВОСТИ КЛУБА</a></li>
                        <li class="one-row-nav__item"><a href="{{route('archive')}}">АРХИВ НОВОСТЕЙ</a></li>
                        <li class="one-row-nav__item"><a href="{{route('history')}}">ИСТОРИЯ</a></li>
                        <li class="one-row-nav__item"><a href="{{route('contact')}}">КОНТАКТЫ</a></li>
                        <li class="one-row-nav__item"><a href="{{route('media')}}">ДЛЯ СКАЧИВАНИЯ</a></li>
                        <li class="one-row-nav__item"><a href="{{route('volejbolnyj_center')}}">ВОЛЕЙБОЛЬНЫЙ ЦЕНТР</a></li>
                        <li class="one-row-nav__item"><a href="{{route('gorodskaja_federacija')}}">ГОРОДСКАЯ ФЕДЕРАЦИЯ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div data-popup-nav="team" class="popup-nav">
    <div class="f-container f-container--popup-nav">
        <button data-target-nav="team" class="close-btn close-btn--popup-nav js-toggle-popup-nav">
            <svg class="icon icon-12 ">
                <use xlink:href="#12"></use>
            </svg>
        </button><a href="#" class="popup-nav__col popup-nav__col--team">
            <div class="popup-nav__content">
                <div class="team-nav">
                    <div class="logo"><img src="/img/29.svg" alt="Северянка"/></div>
                    <div class="popup-nav__title">СЕВЕРЯНКА-1</div>
                </div>
            </div></a><a href="#" class="popup-nav__col popup-nav__col--team">
            <div class="popup-nav__content">
                <div class="team-nav">
                    <div class="logo"><img src="/img/29.svg" alt="Северянка"/></div>
                    <div class="popup-nav__title">СЕВЕРЯНКА</div>
                </div>
            </div></a><a href="#" class="popup-nav__col popup-nav__col--team">
            <div class="popup-nav__content">
                <div class="team-nav">
                    <div class="logo"><img src="/img/29.svg" alt="Северянка"/></div>
                    <div class="popup-nav__title">СЕВЕРЯНКА-2</div>
                </div>
            </div></a>
    </div>
</div>
<div data-popup-nav="competition" class="popup-nav">
    <div class="f-container f-container--popup-nav f-container--nav-one">
        <button data-target-nav="competition" class="close-btn close-btn--popup-nav js-toggle-popup-nav">
            <svg class="icon icon-12 ">
                <use xlink:href="#12"></use>
            </svg>
        </button>
        <div class="popup-nav__col">
            <div class="popup-nav__content">
                <div class="one-row-nav">
                    <div class="logo"><img src="/img/29.svg" alt="Северянка"/></div>
                    <div class="popup-nav__title">СОРЕВНОВАНИЯ</div>
                    <ul class="one-row-nav__list">
                        <li class="one-row-nav__item"><a href="#">ЧЕМПИОНАТ РОССИИ</a></li>
                        <li class="one-row-nav__item"><a href="#">КУБОК РОСИИ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>