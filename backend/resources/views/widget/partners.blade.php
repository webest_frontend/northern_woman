@php
    $partners = \App\Partner::active()->get();

@endphp

@if(count($partners) > 0)
    <div class="container container--default">
        <div class="b-slider js-b-slider-content b-slider--partners js-slider--partners">

            @foreach($partners as $partner)
                <div class="b-slider__item b-slider__item--partners">
                    @if($partner->link)
                        <a href="{{$partner->link}}" class="card-partner" title="{{$partner->name}}">
                            <div class="card-partner__logo">
                                <img src="{{$partner->logo}}" alt="{{$partner->name}}"/>
                            </div>
                            <span class="card-partner__type">ПАРТНЕР </span>
                            <span class="card-partner__name">{{$partner->name}}</span>
                        </a>
                    @else
                        <div href="{{$partner->link}}" class="card-partner">
                            <div class="card-partner__logo">
                                <img src="{{$partner->logo}}" alt="{{$partner->name}}"/>
                            </div>
                            <span class="card-partner__type">ПАРТНЕР </span>
                            <span class="card-partner__name">{{$partner->name}}</span>
                        </div>
                    @endif

                </div>
            @endforeach
        </div>
    </div>
@endif