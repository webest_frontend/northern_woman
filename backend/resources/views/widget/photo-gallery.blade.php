@php
    $Gallerie = \App\Gallerie::Galleries('photo')->first();
    $items = $Gallerie->items->sortByDesc('created_at')->take(4);
@endphp
<div class="bg-gray">
    <div class="container container--default">
        <div class="title-default title-default--gray title-default--mit-link"><span>Фотоотчеты</span>
            <a href="#"  class="link-mit-icon hide-media--md">
                <svg class="icon icon-2 icon-vk">
                    <use xlink:href="#2"></use>
                </svg>
                <span>Смотреть ВКонтакте</span>
            </a>
        </div>
        <div class="b-fotogalery">

            @foreach($items as $item)
                <div class="fotogalery-card">
                    <div style="background-image: url('/{{$item->image}}')" class="fotogalery-card__photo">
                        <a href="{{$item->src}}" target="_blank"
                           class="fotogalery-card__hover"><span>СМОТРЕТЬ В VK</span>
                            <svg class="icon icon-8 ">
                                <use xlink:href="#8"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="fotogalery-card__footer">
                        <div class="fotogalery-card__date">{{$item->getDate()}}</div>
                        <a href="{{$item->src}}" target="_blank" class="fotogalery-card__share">
                            <svg class="icon icon-8 ">
                                <use xlink:href="#8"></use>
                            </svg>
                        </a>
                        <a href="{{$item->src}}" target="_blank" class="fotogalery-card__name">{{$item->name}}</a>
                    </div>
                </div>
            @endforeach

        </div>

        <a href="#" class="link-mit-icon show-media--md-flex">
            <svg class="icon icon-2 icon-vk">
                <use xlink:href="#2"></use>
            </svg>
            <span style="color: #8E98AD;">Смотреть ВКонтакте</span>
        </a>
    </div>
</div>