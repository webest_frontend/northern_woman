<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGallerieItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallerie_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('galleries_id');
            $table->string('name')->nullable();
            $table->text('src')->nullable();
            $table->text('image')->nullable();
            $table->timestamp('date')->nullable();
            $table->integer('sort')->nullable();
            $table->boolean('active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallerie_items');
    }
}
