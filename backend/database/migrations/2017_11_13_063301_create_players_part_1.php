<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersPart1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('photo')->nullable();
            $table->timestamp('year_birth');
            $table->integer('height');
            $table->integer('mass');
            $table->string('сitizenship');
            $table->string('hometown');
            $table->string('amplua')->nullable();
            $table->string('sporting_title');
            $table->string('role_in_team');
            $table->mediumText('description')->nullable();
            $table->text('parameters')->nullable();
            $table->boolean('active');
            $table->boolean('is_captain')->nullable();
            $table->integer('number_team');
            $table->string('url');
            $table->text('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
