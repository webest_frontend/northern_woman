<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTrainer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('trainers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('photo')->nullable();
            $table->timestamp('year_birth');
            $table->integer('height');
            $table->string('сitizenship');
            $table->string('hometown');
            $table->string('sporting_title');
            $table->string('role_in_team');
            $table->string('type_team');
            $table->mediumText('description')->nullable();
            $table->text('parameters')->nullable();
            $table->text('awards')->nullable();
            $table->text('achievements')->nullable();
            $table->text('career')->nullable();
            $table->boolean('active');
            $table->string('url');
            $table->text('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainers');
    }
}
