<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('date');
            $table->time('time');
            $table->string('url');
            $table->integer('team_severjanka');
            $table->integer('team_rival');
            $table->string('type_match');
            $table->string('type_location');
            $table->boolean('active');
            $table->boolean('is_more_info');
            $table->text('severjanka_composition')->nullable();
            $table->text('rival_composition')->nullable();
            $table->text('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
