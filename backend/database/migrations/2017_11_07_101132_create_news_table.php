<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('date');
            $table->string('title');
            $table->string('city')->nullable();
            $table->string('preview_photo');
            $table->text('preview_text')->nullable();
            $table->mediumText('content');
            $table->text('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->string('url');
            $table->integer('sort');
            $table->integer('share__vk')->nullable();
            $table->integer('share__facebook')->nullable();
            $table->integer('share__twetter')->nullable();
            $table->boolean('active');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
