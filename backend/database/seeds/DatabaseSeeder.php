<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(NewsTableSeeder::class);
        //$this->call(PlayersTableSeeder::class);
        //$this->call(TrainersTableSeeder::class);
        $this->call(GalleriesVideoItemSeeder::class);
    }
}
