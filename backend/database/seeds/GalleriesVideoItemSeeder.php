<?php
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class GalleriesVideoItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');

        for($i=0; $i<13;$i++){

            $name = $faker->name;

            DB::table('gallerie_items')->insert([
                'name' => $name,
                'gallerie_id' => 2,
                'date' => $faker->date($format = 'Y-m-d', $max = 'now').' '. $faker->time($format = 'H:i:s', $max = 'now'),
                'src' => 'https://www.youtube.com/embed/AeLEgV2Lt8g',
                'active' => 1,
            ]);
        }
    }
}
