<?php
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');

        for($i=0; $i<13;$i++){

            $name = $faker->name;

            DB::table('players')->insert([
                'name' => $name,
                'url' => str_slug($name),
                'hometown' => $faker->city,
                'year_birth' => $faker->date($format = 'Y-m-d', $max = 'now').' '. $faker->time($format = 'H:i:s', $max = 'now'),
                'photo' =>  $faker->imageUrl(200, 200, 'sports', true, 'Faker'),
                'amplua' => $faker->realText($maxNbChars = 15),
                'height' => $faker->numberBetween($min = 160, $max = 200),
                'mass' => $faker->numberBetween($min = 50, $max = 80),
                'number_team' => $faker->numberBetween($min = 0, $max = 20),
                'сitizenship'=>$faker->country,
                'sporting_title' => $faker->realText($maxNbChars = 10),
                'role_in_team' =>$faker->realText($maxNbChars = 10),
                'active' => 1,
                'is_captain' => 0,
                'description' => $faker->realText($maxNbChars = 1000, $indexSize = 2)
            ]);
        }

    }
}

//$table->string('name');
//$table->text('photo')->nullable();
//$table->timestamp('year_birth');
//$table->integer('height');
//$table->integer('mass');
//$table->string('сitizenship');
//$table->string('hometown');
//$table->string('amplua')->nullable();
//$table->string('sporting_title');
//$table->string('role_in_team');
//$table->mediumText('description')->nullable();
//$table->text('parameters')->nullable();
//$table->boolean('active');
//$table->boolean('is_captain')->nullable();
//$table->integer('number_team');
//$table->string('url');

