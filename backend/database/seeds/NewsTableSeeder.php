<?php
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');

        for($i=0; $i<50;$i++){

            $title = $faker->realText($maxNbChars = 50);

            DB::table('news')->insert([
                'title' => $title,
                'url' => str_slug($title),
                'city' => $faker->city,
                'date' => $faker->date($format = 'Y-m-d', $max = 'now').' '. $faker->time($format = 'H:i:s', $max = 'now'),
                'preview_photo' =>  $faker->imageUrl(815, 450, '', true, 'Faker'),
                'preview_text' => $faker->realText($maxNbChars = 300),
                'share__vk' => 0,
                'share__facebook' => 0,
                'share__twetter' => 0,
                'active' => 1,
                'sort' => 0,
                'content' => $faker->realText($maxNbChars = 8000, $indexSize = 2)
            ]);
        }

    }
}


