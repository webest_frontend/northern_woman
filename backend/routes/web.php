<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/news', 'News@index')->name('news');

Route::get('/news/{url}', 'News@element')->name('news-element');

Route::get('/archive/{filter_first?}/{filter_two?}', 'News@archive')->name('archive');

Route::get('/team/{url_team}', 'TeamController@command')->name('team-command');

Route::get('/player/{url_player}', 'TeamController@playerShow')->name('player');

Route::get('/trainer/{url_trainer}', 'TeamController@trainerShow')->name('trainer');

// static page
Route::get('/history', 'StaticPageController@history')->name('history');

Route::get('/contact', 'StaticPageController@contact')->name('contact');

Route::get('/media', 'StaticPageController@media')->name('media');

Route::get('/volejbolnyj-center', 'StaticPageController@volejbolnyj_center')->name('volejbolnyj_center');

Route::get('/links', 'StaticPageController@links')->name('links');

Route::get('/gorodskaja-federacija', 'StaticPageController@gorodskaja_federacija')->name('gorodskaja_federacija');

Route::get('/search', 'StaticPageController@search')->name('search');

Route::get('/faker', function () {
    return view('pages.faker');
});

Route::get('/import/championship', 'ImportController@championship_import')->name('championship_import');

Auth::routes();

Route::get('logout', function () {
    return view('auth.logout');
});
